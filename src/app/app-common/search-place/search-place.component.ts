import {Component, EventEmitter, Inject, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Place} from '../../model/place';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {SearchService} from '../../service/search.service';
import * as searchReducer from '../../state/reducers/search.reducer';
import {Unsubscribe} from 'redux';

@Component({
  selector: 'app-search-place',
  templateUrl: './search-place.component.html',
  styleUrls: ['./search-place.component.css']
})
export class SearchPlaceComponent implements OnInit, OnDestroy {
  searchControl = new FormControl();
  private _filteredPlaces: Place[];

  @Output() valueChange = new EventEmitter<Place>();
  private places: Place[] = [];
  private readonly maxResult = 5;
  private unsubscribe: Unsubscribe;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private searchService: SearchService,
  ) {
    this.unsubscribe = this.store.subscribe(() => {
      this.updateStore();
    });
    this.updateStore();
    this.searchService.getPlaces().subscribe(places => {
      this.places = places;
      this._filteredPlaces = places;
      this.searchControl.valueChanges.subscribe(places => {
        if (places === '') {
          this.selected(null);
        }
        this._filteredPlaces = places ? this.filterPlaces(places) : this.places;
      });
    });
  }

  get filteredPlaces(): Place[] {
    return this._filteredPlaces ? this._filteredPlaces.slice(0, this.maxResult) : this._filteredPlaces;
  }

  private updateStore() {
    let state = this.store.getState();
    let place = searchReducer.getSearchQuery(state).place;
    if (place) {
      this.searchControl.setValue(place.toString());
    }
    else {
      this.searchControl.setValue('');
    }
  }

  private filterPlaces(filterValue: string = ''): Place[] {
    return this.places.filter(place => place.isSuitable(('' + filterValue).toLowerCase()));
  }

  ngOnInit() {
  }

  selected(place: Place) {
    this.valueChange.emit(place);
  }

  displayFn(place: Place) {
    return place ? place.toString() : undefined;
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
