import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import {DateRange} from '../datepicker-popup/datepicker-popup.component';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as searchReducer from '../../state/reducers/search.reducer';
import {Place} from '../../model/place';
import {SearchService} from '../../service/search.service';
import {MessageService, MessageType} from '../../service/message.service';
import {Hotel} from '../../model/hotel';
import {Unsubscribe} from 'redux';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RoomComponent} from '../../app-hotel/room/room.component';
import {SearchGuestsComponent} from '../search-guests/search-guests.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-search-hotel',
  templateUrl: './hotel-search.component.html',
  styleUrls: ['./hotel-search.component.scss']
})
export class HotelSearchComponent implements OnInit, OnDestroy {
  hotelSearchQuery: HotelSearchQuery;
  private unsubscribe: Unsubscribe;

  constructor(
    private router: Router,
    private searchService: SearchService,
    private messageService: MessageService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private guestsDialog: MatDialog,
  ) {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => {
      this.updateState();
    });
  }

  private updateState() {
    this.hotelSearchQuery = searchReducer.getSearchQuery(this.store.getState());
  }

  ngOnInit() {
  }

  search(): void {
    if(this.hotelSearchQuery.place) {
      if(this.hotelSearchQuery.roomCount > this.hotelSearchQuery.adultCount) {
        this.hotelSearchQuery.roomCount = 1;
      }
      this.searchService.search(this.hotelSearchQuery).subscribe((hotels: Hotel[]) => {
        if(!hotels || !hotels.length) {
          this.messageService.showTransMessage("message.noItemsFound", MessageType.WARNING);
        }
        this.router.navigateByUrl('/hotels');
      });
    }
  }

  onDateSelected(dateRange: DateRange) {
    this.hotelSearchQuery.checkIn = dateRange.fromDate;
    this.hotelSearchQuery.checkOut = dateRange.toDate;
  }

  placeSelected(place: Place) {
    this.hotelSearchQuery.place = place;
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  openGuestsModal() {
    let modalRef = this.guestsDialog.open(SearchGuestsComponent, {
      maxWidth: 620
    });
    let modal = modalRef.componentInstance;
    modal.hotelSearchQuery = this.hotelSearchQuery;
  }
}
