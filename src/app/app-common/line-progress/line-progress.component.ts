import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-line-progress',
  templateUrl: './line-progress.component.html',
  styleUrls: ['./line-progress.component.scss']
})
export class LineProgressComponent implements OnInit {
  @Input() value: number;
  @Input() progress: number;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
