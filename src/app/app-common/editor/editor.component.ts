import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-editor',
  template: '<label *ngIf="placeholder">{{placeholder}}</label>' +
    '<ckeditor' +
    '  [(ngModel)]="content"' +
    '  [config]="{uiColor: \'#f8f8ff\'}"' +
    '  (change)="onChange($event)"' +
    // '  (ready)="onEvent($event)"' +
    // '  (focus)="onEvent($event)"' +
    // '  (blur)="onEvent($event)"' +
    '  debounce="500">' +
    '</ckeditor>',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  @Input() content: string;
  @Input() placeholder: string;
  @Output() contentChange = new EventEmitter<string>();

  onChange(event) {
    this.contentChange.emit(event);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
