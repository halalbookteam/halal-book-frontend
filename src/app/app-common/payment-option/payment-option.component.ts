import {Component, Input, OnInit} from '@angular/core';
import {PaymentOption} from '../../model/room-alternative-options';
import * as moment from 'moment';

@Component({
  selector: 'app-payment-option',
  templateUrl: './payment-option.component.html',
  styleUrls: ['./payment-option.component.scss']
})
export class PaymentOptionComponent implements OnInit {
  @Input() paymentOption: PaymentOption;
  @Input() checkIn: Date = new Date();

  constructor() { }

  ngOnInit() {
  }

  cancelExpireTime(days) {
    return moment(this.checkIn).subtract(days, 'days').toDate();
  }
}
