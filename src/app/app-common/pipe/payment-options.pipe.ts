import { Pipe, PipeTransform } from '@angular/core';
import {PaymentOption} from '../../model/room-alternative-options';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'paymentOptions',
  pure: false
})
export class PaymentOptionsPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {
  }

  transform(paymentOption: PaymentOption, args?: any): any {
    if(paymentOption.isNonRefundable()) {
      return this.translateService.get("enum.PaymentOptionType.NON_REFUNDABLE");
    }
    return this.translateService.get("enum.PaymentOptionType.REFUNDABLE");
  }

}
