import {Inject, Injectable, Pipe, PipeTransform} from '@angular/core';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as languageReducer from '../../state/reducers/language.reducer';
import {ExchangeService} from '../../service/exchange.service';
import {map} from 'rxjs/operators';
import {Price} from '../../model/price';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
@Pipe({
  name: 'price',
  pure: false
})
export class PricePipe implements PipeTransform {
  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private exchangeService: ExchangeService,
  ) {
  }

  transform(price: Price, exchange: boolean = true) {
    if(!price) {
      return;
    }
    var convertPrice = newPrice => {
      let currency = languageReducer.getCurrencyByCode(this.store.getState(), newPrice.currency);
      return newPrice.amount.toFixed(currency.fractionDigits) + ' ' + currency.shortName;
    };

    if(!exchange) {
      return of(convertPrice(price));
    }

    return this.exchangeService.exchange(price).pipe(
      map(convertPrice));
  }

}
