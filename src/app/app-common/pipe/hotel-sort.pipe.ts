import {Pipe, PipeTransform} from '@angular/core';
import {Hotel, HotelSortType} from '../../model/hotel';

@Pipe({
  name: 'hotelSort',
  pure: true
})
export class HotelSortPipe implements PipeTransform {

  transform(hotels: Hotel[], sortType: HotelSortType, isAscending: boolean): Hotel[] {
    return hotels.sort((h1, h2) => {
      const h1Value = this.getValue(h1, sortType, isAscending);
      const h2Value = this.getValue(h2, sortType, isAscending);
      const difference = h1Value - h2Value;
      return isAscending ? difference : (-1 * difference);
    });
  }

  private getValue(hotel: Hotel, sortType: HotelSortType, isAscending: boolean) {
    switch (+sortType) {
      case HotelSortType.PRICE: {
        return hotel.recommendedRooms ? hotel.recommendedRooms.totalPrice.amount : (isAscending ? 99999999 : -99999999);
      }
      case HotelSortType.STAR: {
        return hotel.star;
      }
      case HotelSortType.REVIEW: {
        return hotel.evaluation ? hotel.evaluation.score : 0;
      }
    }
  }

}
