import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'numberArray',
  pure: true
})
export class NumberArrayPipe implements PipeTransform {

  transform(count: number, start: number = 1, increment: number = 1): any {
    let res = [];
    for (let i = start; i <= count; i = i + increment) {
      res.push(i);
    }
    return res;
  }
}
