import {Pipe, PipeTransform} from '@angular/core';
import {LanguageService} from '../../service/language.service';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'formatDate',
  pure: false
})
export class FormatDatePipe implements PipeTransform {
  constructor(private languageService: LanguageService) {
  }

  transform(date: Date | Date[], format: string = "shortDate", seperator: string = ' - '): string {
    if(!Array.isArray(date)) {
      date = [date];
    }
    return date.reduce((dateStr: string, date: Date) => {
      if (!date) {
        return dateStr;
      }
      if(dateStr !== '') {
        dateStr = dateStr + seperator;
      }
      return dateStr + formatDate(date, format, this.languageService.getLanguageCode())
    }, '')
  }
}
