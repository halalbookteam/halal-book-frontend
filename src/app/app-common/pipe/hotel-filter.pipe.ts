import {Pipe, PipeTransform} from '@angular/core';
import {Hotel} from '../../model/hotel';
import {FilterGroup, LogicType} from '../../model/filter';

@Pipe({
  name: 'hotelFilter',
  pure: true
})

export class HotelFilterPipe implements PipeTransform {
  private count: number = 0;
  transform(hotels: Hotel[], groups: FilterGroup[]): Hotel[] {
    const checkedGroups = groups.filter(group => group.filters.some(filter => filter.checked));
    const filteredHotels = this.filter(hotels, checkedGroups);

    const uncheckedGroups = groups.filter(group => !checkedGroups.some(ch => ch.name === group.name));
    this.calculateMatchCounts(uncheckedGroups, filteredHotels);

    for (const group of checkedGroups) {
      const otherGroups = checkedGroups.filter(g => g.name !== group.name);
      const hotelsFilteredByOtherGroups = this.filter(hotels, otherGroups);
      let hotelsFilteredByThisGroup = this.filter(hotelsFilteredByOtherGroups, [group]);
      if(group.logicType === LogicType.AND) {
        this.calculateMatchCounts([group], hotelsFilteredByThisGroup);
      }
      else {
        let remainingHotels = hotelsFilteredByOtherGroups.filter(hotel => !hotelsFilteredByThisGroup.some(h => h.id === hotel.id));
        this.calculateMatchCounts([group], remainingHotels);
      }
    }

    return filteredHotels;
  }

  private filter(hotels: Hotel[], checkedGroups: FilterGroup[]) {
    if(!checkedGroups.length) {
      return hotels;
    }
    return hotels.filter(hotel => checkedGroups.every(group => {
      const matchFunc = filter => hotel.isMatch(group, filter);
      let checkedFilters = group.filters.filter(filter => filter.checked);
      return group.logicType === LogicType.OR ? checkedFilters.some(matchFunc) : checkedFilters.every(matchFunc);
    }));
  }

  private calculateMatchCounts(groups: FilterGroup[], hotels: Hotel[]) {
    groups.forEach(group => {
      group.filters.forEach(filter => {
        filter.count = filter.checked ? 0 : hotels.filter(hotel => hotel.isMatch(group, filter)).length;
      });
    });
  }

}
