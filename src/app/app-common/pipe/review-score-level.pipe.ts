import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
@Pipe({
  name: 'reviewScoreLevel',
  pure: false
})
export class ReviewScoreLevelPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {
  }

  transform(score: number, min?: number): Observable<string> {
    let floor = Math.floor(score);
    if(min && floor < min) {
      return this.translateService.get("review.score");
    }
    return this.translateService.get("enum.ReviewScoreLevel." + floor);
  }
}
