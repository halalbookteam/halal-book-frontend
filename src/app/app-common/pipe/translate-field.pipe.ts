import { Pipe, PipeTransform } from '@angular/core';
import {BaseTranslatedModel} from '../../model/translation';
import {LanguageService} from '../../service/language.service';

@Pipe({
  name: 'translateField',
  pure: false
})
export class TranslateFieldPipe implements PipeTransform {
  constructor(private languageService: LanguageService) {
  }

  transform(model: BaseTranslatedModel, fieldName: string, languageId?: number): any {
    if(model) {
      languageId = languageId || this.languageService.getLanguageId();
      return model.get(fieldName, languageId);
    }
    return model;
  }
}
