import {Inject, Pipe, PipeTransform} from '@angular/core';
import {PricePipe} from './price.pipe';
import {TranslateService} from '@ngx-translate/core';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {combineLatest, from, Observable} from 'rxjs';
import {map, mergeScan, scan} from 'rxjs/operators';
import {MealOptionType} from '../../model/room-alternative-options';
import {ExchangeService} from '../../service/exchange.service';

@Pipe({
  name: 'mealOption',
  pure: false
})
export class MealOptionPipe implements PipeTransform {
  pricePipe: PricePipe;

  constructor(@Inject(AppStore) private store: Redux.Store<AppState>,
              private translateService: TranslateService,
              private exchangeService: ExchangeService,
              ) {
    this.pricePipe = new PricePipe(store, exchangeService);
  }

  transform(mealOption: any, languageCode?: string): any {
    if (mealOption.type === MealOptionType.NOTHING_INCLUDED) {
      return this.translateService.get('enum.MealOptionType.NOTHING_INCLUDED');
    }
    if (mealOption.type === MealOptionType.ALL_INCLUDED) {
      return this.translateService.get('enum.MealOptionType.ALL_INCLUDED');
    }
    let mealListAsObs = this.mealListAsObs(from(mealOption.meals));
    if (mealOption.hasOptionalPrice()) {
      let price = this.pricePipe.transform(mealOption.price, false);
      return this.concatStrObs(mealListAsObs, price);
    }
    return this.concatStrObs(mealListAsObs, this.translateService.get('alternative.included'));
  }

  private mealListAsObs(observable: Observable<any>) {
    return observable.pipe(mergeScan((acc, meal) => this.translateService.get('enum.MealType.' + meal), ''))
      .pipe(scan((acc, meal) => acc + (acc ? ', ' : '') + meal));
  }

  private concatStrObs(obs1: Observable<string>, obs2: Observable<string>, seperator: string = ' ') {
    return combineLatest(obs1, obs2).pipe(map(([first, second]) => first + seperator + second));
  }
}
