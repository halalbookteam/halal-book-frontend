import {Pipe, PipeTransform} from '@angular/core';
import {Review, ReviewSortType} from '../../model/review';

@Pipe({
  name: 'reviewSort',
  pure: true
})
export class ReviewSortPipe implements PipeTransform {

  transform(reviews: Review[], sortType: ReviewSortType, isAscending: boolean): Review[] {
    return reviews.sort((h1, h2) => {
      const h1Value = this.getValue(h1, sortType);
      const h2Value = this.getValue(h2, sortType);
      const difference = h1Value - h2Value;
      return isAscending ? difference : (-1 * difference);
    });
  }

  private getValue(review: Review, sortType: ReviewSortType) {
    switch (+sortType) {
      case ReviewSortType.SCORE: {
        return review.score;
      }
      case ReviewSortType.DATE: {
        return review.time.getDate();
      }
      default: {
        break;
      }
    }
  }

}
