import { Pipe, PipeTransform } from '@angular/core';
import {Review} from '../../model/review';
import {of} from 'rxjs';
import {ReviewScoreLevelPipe} from './review-score-level.pipe';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'reviewTitlePipe'
})
export class ReviewTitlePipePipe implements PipeTransform {
  private reviewScoreLevelPipe: ReviewScoreLevelPipe;

  constructor(private translateService: TranslateService) {
    this.reviewScoreLevelPipe = new ReviewScoreLevelPipe(this.translateService);
  }

  transform(review: Review): any {
    if(review.title) {
      return of(review.title);
    }
    return this.reviewScoreLevelPipe.transform(review.score);
  }

}
