import {Pipe, PipeTransform} from '@angular/core';
import {Address} from '../../model/address';
import {LanguageService} from '../../service/language.service';

@Pipe({
  name: 'address',
  pure: false
})
export class AddressPipe implements PipeTransform {
  constructor(private languageService: LanguageService) {
  }

  transform(address: Address, formatType: string = 'short'): any {
    let languageId = this.languageService.getLanguageId();
    if(formatType === 'short') {
      return this.createShortAddress(address, languageId);
    }
    if(formatType === 'long') {
      let street = address.get('street', languageId);
      return (street ? street : "") + this.createShortAddress(address, languageId);
    }
    return address;
  }

  private createShortAddress(address: Address, languageId: number) {
    let str = '';
    let district = address.get('district', languageId);
    str += district ? (district + ', ') : '';
    let city = address.get('city', languageId);
    str += city ? (city + ', ') : '';
    let county = address.get('county', languageId);
    str += county ? (county + ', ') : '';
    str += address.get('country', languageId);
    return str;
  }
}
