import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'plural',
  pure: true
})
export class PluralPipe implements PipeTransform {
  transform(count: number, singular: string, plural?: string, none?: string): any {
    if(!singular || count < 0) {
      return count;
    }
    if(count === 1) {
      return count + " " + singular;
    }
    return (count === 0 && none ? none : count) + " " + (plural ? plural : (singular + "s"));
  }
}
