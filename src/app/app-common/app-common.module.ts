import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DatepickerPopupComponent} from './datepicker-popup/datepicker-popup.component';
import {TypeaheadComponent} from './typeahead-basic/typeahead.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HotelSearchComponent} from './hotel-search/hotel-search.component';
import {AgmCoreModule} from '@agm/core';
import {MapComponent} from './map/map.component';
import {SearchPlaceComponent} from './search-place/search-place.component';
import {FormatDatePipe} from './pipe/format-date.pipe';
import {HotelFilterPipe} from './pipe/hotel-filter.pipe';
import {HotelSortPipe} from './pipe/hotel-sort.pipe';
import {NumberArrayPipe} from './pipe/number-array.pipe';
import {CarouselWithThumbailsComponent} from './carousel-with-thumbails/carousel-with-thumbails.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {AppFileUploadModule} from '../app-file-upload/app-file-upload.module';
import {PluralPipe} from './pipe/plural.pipe';
import {LoginModalComponent} from './login-modal/login-modal.component';
import {FormatBytesPipe} from '../app-file-upload/pipe/format-bytes.pipe';
import {PriceComponent} from './price/price.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {PaymentOptionComponent} from './payment-option/payment-option.component';
import {AutoFocusDirective} from './directive/auto-focus.directive';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import {EditorComponent} from './editor/editor.component';
import {CKEditorModule} from 'ng2-ckeditor';
import {SafeHtml} from './pipe/safe-html';
import {CalendarDayComponent} from './calendar-day/calendar-day.component';
import {LineProgressComponent} from './line-progress/line-progress.component';
import {ScoreComponent} from './score/score.component';
import {AppMaterialModule} from '../app-material/app-material.module';
import {ReviewSortPipe} from './pipe/review-sort.pipe';
import {TranslateModule} from '@ngx-translate/core';
import {PricePipe} from './pipe/price.pipe';
import {MealOptionPipe} from './pipe/meal-option.pipe';
import {ReviewScoreLevelPipe} from './pipe/review-score-level.pipe';
import {ReviewTitlePipePipe} from './pipe/review-title-pipe.pipe';
import {PaymentOptionsPipe} from './pipe/payment-options.pipe';
import {TranslateFieldPipe} from './pipe/translate-field.pipe';
import {AddressPipe} from './pipe/address.pipe';
import {ActivateAccountComponent} from './activate-account/activate-account.component';
import {SearchGuestsComponent} from './search-guests/search-guests.component';
import {PersonCountComponent} from './person-count/person-count.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDD1ruw25oF898cJJYAwOiD3D8TuxqPU9M',
      libraries: ['places'],
      // language can be changed dynamically
      language: 'tr'
    }),
    AppMaterialModule,
    AppFileUploadModule,
    CKEditorModule,
    TranslateModule,
  ],
  exports: [
    DatepickerPopupComponent,
    TypeaheadComponent,
    HotelSearchComponent,
    SearchPlaceComponent,
    CarouselWithThumbailsComponent,
    MapComponent,
    FormatDatePipe,
    HotelFilterPipe,
    HotelSortPipe,
    NumberArrayPipe,
    CarouselWithThumbailsComponent,
    PluralPipe,
    AppMaterialModule,
    FormatBytesPipe,
    AppFileUploadModule,
    PriceComponent,
    PaymentOptionComponent,
    AutoFocusDirective,
    ConfirmationDialogComponent,
    EditorComponent,
    SafeHtml,
    CalendarDayComponent,
    LineProgressComponent,
    ScoreComponent,
    ReviewSortPipe,
    TranslateModule,
    PricePipe,
    MealOptionPipe,
    ReviewScoreLevelPipe,
    ReviewTitlePipePipe,
    PaymentOptionsPipe,
    TranslateFieldPipe,
    AddressPipe,
    ActivateAccountComponent,
    SearchGuestsComponent,
    PersonCountComponent,
  ],
  declarations: [
    DatepickerPopupComponent,
    TypeaheadComponent,
    HotelSearchComponent,
    MapComponent,
    SearchPlaceComponent,
    FormatDatePipe,
    HotelFilterPipe,
    ReviewSortPipe,
    HotelSortPipe,
    NumberArrayPipe,
    CarouselWithThumbailsComponent,
    PluralPipe,
    LoginFormComponent,
    LoginModalComponent,
    PriceComponent,
    PaymentOptionComponent,
    AutoFocusDirective,
    ConfirmationDialogComponent,
    EditorComponent,
    SafeHtml,
    CalendarDayComponent,
    LineProgressComponent,
    ScoreComponent,
    PricePipe,
    MealOptionPipe,
    ReviewScoreLevelPipe,
    ReviewScoreLevelPipe,
    ReviewTitlePipePipe,
    ReviewTitlePipePipe,
    PaymentOptionsPipe,
    PaymentOptionsPipe,
    TranslateFieldPipe,
    AddressPipe,
    ActivateAccountComponent,
    SearchGuestsComponent,
    PersonCountComponent,
  ],
  entryComponents: [
    CarouselWithThumbailsComponent,
    LoginModalComponent,
    ConfirmationDialogComponent,
    SearchGuestsComponent,
  ],
})
export class AppCommonModule {
}
