import {Component, Input, OnInit} from '@angular/core';
import {AuthService, GoogleLoginProvider} from 'angularx-social-login';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl} from '@angular/forms';
import {AuthenticationService} from '../../service/authentication.service';
import {TranslateService} from '@ngx-translate/core';
import {MessageService, MessageType} from '../../service/message.service';
import {FORBIDDEN, PRECONDITION_REQUIRED} from 'http-status-codes';
import {Router} from '@angular/router';

export enum LoginFormType {
  login = "login",
  register = "register",
}

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Input() formType: LoginFormType = LoginFormType.login;
  formMessage: string;
  emailControl = new FormControl('');
  passwordControl = new FormControl('');

  constructor(
    private authenticationService: AuthenticationService,
    private socialAuthService: AuthService,
    public activeModal: NgbActiveModal,
    public messageService: MessageService,
    private translateService: TranslateService,
    private router: Router,
  ) {
  }

  ngOnInit() {
  }

  submit() {
    this.formMessage = undefined;
    if(this.formValid()) {
      if(this.formType === LoginFormType.login) {
        this.login();
      }
      else if(this.formType === LoginFormType.register) {
        this.register();
      }
    }
  }

  private formValid() {
    return this.emailControl.valid && this.passwordControl.valid;
  }

  private login() {
    this.authenticationService.login(this.emailControl.value, this.passwordControl.value).subscribe(
      () => {
        this.activeModal.close('success close');
      },
      err => {
        if(err.status === FORBIDDEN) {
          this.translateService.get("message.loginCredFailed").subscribe(message => this.formMessage = message);
        }
        if(err.status === PRECONDITION_REQUIRED) {
          this.translateService.get("message.activateAccount").subscribe(message => this.formMessage = message);
        }
      }
    );
  }

  private register() {
    this.authenticationService.register(this.emailControl.value, this.passwordControl.value).subscribe(user => {
      this.translateService.get('title.activateAccount').subscribe(title => {
        this.messageService.showTransMessage('message.activateAccount', MessageType.SUCCESS, title);
        this.activeModal.close('success close');
      });
    });
  }

  // facebook() {
  //   const socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //   this.socialAuthService.signIn(socialPlatformProvider).then(
  //     (userData) => { // on success
  //       console.log(userData);
  //       this.authenticationService.loginOrRegisterWithFacebook(userData.authToken)
  //         .subscribe(() => {
  //           this.activeModal.close('success close');
  //         }, () => {
  //           console.log('some fail');
  //         });
  //     }
  //   );
  // }

  google() {
    const socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => { // on success
        this.authenticationService.loginOrRegisterWithGoogle(userData.idToken)
          .subscribe(() => {
            this.activeModal.close('success close');
          }, () => {
            console.log('some fail');
          });
      }
    );
  }

  isLogin() {
    return this.formType === LoginFormType.login;
  }

  forgotPassword() {
    this.router.navigateByUrl("/forgotPassword").then(() => {
      this.activeModal.close('success close');
    })
  }
}
