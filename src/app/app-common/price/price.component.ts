import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Price} from '../../model/price';
import {FormControl, Validators} from '@angular/forms';
import {Currency} from '../../model/language';
import {LanguageService} from '../../service/language.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnInit {
  @Input() placeholder: string;
  @Input() price: Price;
  @Input() private currency: string;
  @Input() required = false;
  @Input() showCurrencies = false;
  @Output() valueChange = new EventEmitter();
  amountControl = new FormControl('', [Validators.required, Validators.min(0.00000001)]);
  currencies: Currency[] = [];
  private _currencyShortName: string;

  constructor(
    private languageService: LanguageService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.currencies = this.languageService.getCurrencies();

    this.price = this.price || new Price();
    this.price.currency = this.price.currency || this.currency;

    if(!this.showCurrencies && this.currency) {
      this._currencyShortName = this.currencies.find(curr => curr.code === this.currency).shortName;
    }

    if(this.required) {
      this.amountControl.setValue(this.price.amount);
    }
    if(!this.placeholder) {
      this.translateService.get("common.price").subscribe(text => this.placeholder = text);
    }
  }

  get currencyShortName(): string {
    return this._currencyShortName;
  }

  changeCurrentValue() {
    if(this.required) {
      this.price.amount = this.amountControl.value;
    }
    this.valueChange.emit(this.price);
  }

  isPriceValid() {
    return !this.required || this.amountControl.valid;
  }
}
