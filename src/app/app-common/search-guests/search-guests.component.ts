import {Component, Input, OnInit} from '@angular/core';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import * as _ from 'lodash';

@Component({
  selector: 'app-search-guests',
  templateUrl: './search-guests.component.html',
  styleUrls: ['./search-guests.component.scss']
})
export class SearchGuestsComponent implements OnInit {
  @Input() hotelSearchQuery: HotelSearchQuery;
  childAges: number[];

  constructor() { }

  ngOnInit() {
    this.childAges = _.cloneDeep(this.hotelSearchQuery.childAges);
  }

  open() {
  }

  childCountChanged() {
    let difference = this.hotelSearchQuery.childCount - this.hotelSearchQuery.childAges.length;
    if(difference < 0) {
      this.hotelSearchQuery.childAges = this.hotelSearchQuery.childAges.slice(0, this.hotelSearchQuery.childCount);
    }
    else {
      for(let i = 0; i < difference; i++) {
        this.hotelSearchQuery.childAges.push(12);
      }
    }
    this.childAges = _.cloneDeep(this.hotelSearchQuery.childAges);
  }

  childAgeChanged(index: number, age: number) {
    this.hotelSearchQuery.childAges[index] = age;
  }
}
