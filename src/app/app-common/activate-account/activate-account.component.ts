import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../service/authentication.service';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.css']
})
export class ActivateAccountComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private authenticationService: AuthenticationService,
              private router: Router
              ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.authenticationService.activateAccount(params.ticket).subscribe(() => {
        this.router.navigateByUrl("");
      });
    })
  }

}
