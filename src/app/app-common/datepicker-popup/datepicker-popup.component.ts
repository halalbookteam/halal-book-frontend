import {Component, EventEmitter, Injectable, Input, Output, ViewChild} from '@angular/core';
import {NgbCalendar, NgbDatepickerI18n, NgbDateStruct, NgbInputDatepicker} from '@ng-bootstrap/ng-bootstrap';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import {LanguageService} from '../../service/language.service';
import * as moment from 'moment';

export interface DateRange {
  fromDate: Date;
  toDate: Date;
}

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private languageService: LanguageService) {
    super();
    moment.locale(this.languageService.getLanguageCode());
    moment.weekdays(true);
  }

  getWeekdayShortName(weekday: number): string {
    return moment.weekdays(weekday).substring(0, 2);
  }

  getMonthShortName(month: number): string {
    return moment.months(month);
  }

  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-datepicker-popup',
  templateUrl: './datepicker-popup.component.html',
  styleUrls: ['./datepicker-popup.component.scss'],
  providers: [{provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class DatepickerPopupComponent {
  @Input() label;
  @ViewChild('dp') private datePicker: NgbInputDatepicker;
  @Output() dateSelected = new EventEmitter<DateRange>();

  hoveredDate: NgbDateStruct;
  private _fromDate: NgbDateStruct;
  private _toDate: NgbDateStruct;
  navigation = 'arrow';
  outsideDays = 'hidden';
  today: NgbDate;

  @Input()
  set fromDate(date: Date) {
    this._fromDate = this.convertToNgbStruct(date);
  }

  @Input()
  set toDate(date: Date) {
    this._toDate = this.convertToNgbStruct(date);
  }

  get fromDate() {
    return this.createDate(this._fromDate);
  }

  get toDate() {
    return this.createDate(this._toDate);
  }

  constructor(private calendar: NgbCalendar) {
    this.today = calendar.getToday();
  }

  onDateSelection(date: NgbDateStruct) {
    if (!this._fromDate && !this._toDate) {
      this._fromDate = date;
    } else if (this._fromDate && !this._toDate && after(date, this._fromDate)) {
      this._toDate = date;
    } else {
      this._toDate = null;
      this._fromDate = date;
    }
    if (this._fromDate && this._toDate) {
      this.datePicker.close();
    }
    this.fireDateSelectedEvent();
  }

  isHovered = date => this._fromDate && !this._toDate && this.hoveredDate && after(date, this._fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this._fromDate) && before(date, this._toDate);
  isFrom = date => equals(date, this._fromDate);
  isTo = date => equals(date, this._toDate);

  closeFix(event, dpDiv) {
    if (this.datePicker.isOpen()) {
      if (!event.target.offsetParent) {
        this.datePicker.close();
      } else if (event.target.offsetParent.nodeName !== 'NGB-DATEPICKER') {
        if (this._fromDate && !this._toDate) {
          this._toDate = this.calendar.getNext(this.convertToNgbDate(this._fromDate), 'd', 1);
          this.fireDateSelectedEvent();
        }
        this.datePicker.close();
      }
    } else if (event.target.offsetParent && (event.target.offsetParent.className.indexOf('date-picker') >= 0
      || event.target.offsetParent.className.indexOf('show-prices') >= 0)) {
      dpDiv.scrollIntoView();
      this.datePicker.open();
    }
  }

  private createDate(date: NgbDateStruct) {
    if (!date) {
      return null;
    }
    return new Date(date.year, date.month - 1, date.day);
  }

  private convertToNgbDate(date: NgbDateStruct) {
    return new NgbDate(date.year, date.month, date.day);
  }

  isDateDisabled(date: NgbDateStruct) {
    return DatepickerPopupComponent.createToday().after(new NgbDate(date.year, date.month, date.day));
  }

  isPassed(date) {
    return DatepickerPopupComponent.createToday().after(new NgbDate(date.year, date.month, date.day));
  }

  static createToday() {
    const today = new Date();
    return new NgbDate(today.getFullYear(), today.getMonth() + 1, today.getDate());
  }

  private convertToNgbStruct(date: Date): NgbDateStruct {
    if (!date) {
      return null;
    }
    return {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear()
    };
  }

  private fireDateSelectedEvent() {
    this.dateSelected.emit({
      fromDate: this.fromDate,
      toDate: this.toDate
    });
  }
}

