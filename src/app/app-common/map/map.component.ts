import {Component, Input, OnInit} from '@angular/core';
import {LatLngBounds, LatLngBoundsLiteral} from '@agm/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @Input() lon: number;
  @Input() lat: number;
  @Input() smallSize: boolean;
  @Input() zoom: number = 12;
  @Input() bounds: LatLngBoundsLiteral | LatLngBounds;

  constructor() { }

  ngOnInit() {
    if(!this.lon) {
      this.setCurrentPosition();
    }
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lon = position.coords.longitude;
        this.lat = position.coords.latitude;
        this.zoom = 12;
      });
    }
  }
}
