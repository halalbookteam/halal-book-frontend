import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {NgbCarousel} from '@ng-bootstrap/ng-bootstrap';
import {Photo} from '../../model/photo';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-carousel-with-thumbails',
  templateUrl: './carousel-with-thumbails.component.html',
  styleUrls: ['./carousel-with-thumbails.component.css']
})
export class CarouselWithThumbailsComponent {

  @ViewChild('carousel')
  carouselNgbCarousel: NgbCarousel;

  @Input() photos: Array<Photo>;

  @Output() imageChanged = new EventEmitter<number>();
  @Output() modalTriggered = new EventEmitter<number>();
  @Output() closeModal = new EventEmitter();

  constructor() {
  }

  getSelectedIndex(): number {
    if (!this.carouselNgbCarousel) {
      return 0;
    }
    return this.carouselNgbCarousel.slides.toArray().map(value => value.id).indexOf(this.carouselNgbCarousel.activeId);
  }

  imageChangedAction() {
    const index = this.getSelectedIndex();
    this.imageChanged.emit(index);
  }

  select(index: number) {
      const slideId = this.carouselNgbCarousel.slides.toArray()[index].id;
      if (slideId !== this.carouselNgbCarousel.activeId) {
        this.carouselNgbCarousel.select(slideId);
        this.imageChangedAction();
      }
  }

  imageClickedAction() {
    const index = this.getSelectedIndex();
    this.modalTriggered.emit(index);
  }

  close() {
    this.closeModal.emit();
  }
}
