import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-person-count',
  templateUrl: './person-count.component.html',
  styleUrls: ['./person-count.component.scss']
})
export class PersonCountComponent implements OnInit {
  @Input() adultCount: number;
  @Input() childCount: number;

  constructor() { }

  ngOnInit() {
  }

}
