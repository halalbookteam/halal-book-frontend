import {Component, OnInit} from '@angular/core';
import {LoginFormType} from '../login-form/login-form.component';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {
  FormType = LoginFormType;
  formType: LoginFormType = LoginFormType.login;
  constructor() {
  }

  ngOnInit() {

  }
}
