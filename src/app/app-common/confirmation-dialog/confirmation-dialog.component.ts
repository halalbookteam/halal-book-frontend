import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export enum DialogOption {
  YES,
  CANCEL,
}

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;

  DialogOption = DialogOption;
  constructor(private translateService: TranslateService) {
  }

  ngOnInit() {
    if(!this.content) {
      this.translateService.get('message.sure').subscribe(text => this.content = text);
    }
  }

}
