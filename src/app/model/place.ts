export class Place {
  id: number;
  hotelName: string;
  district: string;
  city: string;
  county: string;
  country: string;

  constructor(place?: Place) {
    Object.assign(this, place);
  }

  toString() {
    let str = "";
    if(this.hotelName) {
      str = this.hotelName + ", ";
    }
    return str + this.toStringWithoutHotelName();
  }

  toStringWithoutHotelName() {
    let str = "";
    if(this.district) {
      str = this.district + ", ";
    }
    if(this.city) {
      str += this.city + ", ";
    }
    if(this.county) {
      str += this.county + ", ";
    }
    if(this.country) {
      str += this.country;
    }
    return str;
  }

  isSuitable(filterValue: string): boolean {
    let parts: string[] = this.toString().replace(",", "").split(" ");
    for(let part of parts) {
      if(part.toLowerCase().startsWith(filterValue)) {
        return true;
      }
    }
    return false;
  }
}
