import {Price} from './price';

export enum MealOptionType {
  NOTHING_INCLUDED = 'NOTHING_INCLUDED',
  ALL_INCLUDED = 'ALL_INCLUDED',
  OPTIONAL = 'OPTIONAL',
}

export enum MealType {
  BREAKFAST = 'BREAKFAST',
  LUNCH = 'LUNCH',
  DINNER = 'DINNER',
}

export class MealOption {
  type: MealOptionType = MealOptionType.NOTHING_INCLUDED;
  meals: MealType[];
  price: Price;

  constructor(mealOption?: MealOption) {
    Object.assign(this, mealOption);
    this.price = new Price(this.price);
  }

  hasOptionalPrice() {
    return this.type === MealOptionType.OPTIONAL && this.price && !!this.price.amount;
  }
}

export class PaymentOption {
  freeCancellationDays: number;
  penaltyCancellationDays: number;
  penaltyPercentage: number;
  noPrepayment: boolean;

  constructor(option?: PaymentOption) {
    Object.assign(this, option);
  }

  isNonRefundable() {
    return !this.freeCancellationDays && !this.penaltyCancellationDays;
  }
}
