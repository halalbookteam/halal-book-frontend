import {Price} from './price';
import {RoomAlternative} from './room';

export class RoomReservation {
  alternative: RoomAlternative;
  adultCount: number;
  childCount: number;
  alternativeId: number;

  constructor(roomReservation?: RoomReservation) {
    Object.assign(this, roomReservation);
  }
}

export class RecommendedRoomSet {
  roomReservations: RoomReservation[];
  private alternativeIdCountMap: {[id: number]: number};
  private alternativeIdAlternativeMap: {[id: number]: RoomAlternative};
  totalPrice: Price;

  constructor(recommendedRoomSet?: RecommendedRoomSet) {
    Object.assign(this, recommendedRoomSet);
    this.alternativeIdCountMap = {};
    this.alternativeIdAlternativeMap = {};
    if(this.totalPrice) {
      this.totalPrice = new Price(this.totalPrice);
    }
    if(this.roomReservations) {
      this.roomReservations = this.roomReservations.map(roomReservation => new RoomReservation(roomReservation))
    }
  }

  mapAlternatives() {
    this.roomReservations.forEach(roomReservation => {
      let alternativeId = roomReservation.alternativeId;
      let count = this.alternativeIdCountMap[alternativeId];
      if(!count) {
        count = 0;
        this.alternativeIdAlternativeMap[alternativeId] = roomReservation.alternative;
      }
      this.alternativeIdCountMap[alternativeId] = count + 1;
    });
  }

  get alternatives() {
    return Object.keys(this.alternativeIdAlternativeMap).map(alternativeId => this.alternativeIdAlternativeMap[alternativeId]);
  }

  getAlternativeCount(alternativeId: number) {
    return this.alternativeIdCountMap[alternativeId];
  }
}
