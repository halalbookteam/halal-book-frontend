import {Room} from './room';
import {Price} from './price';
import {Hotel} from './hotel';
import {Person} from './person';
import {CreditCard} from './credit-card';
import * as moment from 'moment';
import {Permission, User} from './user';
import {Review} from './review';
import {RoomReservation} from './recommended-room-set';

export enum ReservationStep {
  DETAILS,
  PAYMENT,
  COMPLETE
}

export enum ReservationStatus {
  APPLIED = 'APPLIED',
  CANCELLED = 'CANCELLED',
  CONFIRMED = 'CONFIRMED',
  CLOSED = 'CLOSED',
}

export class Reservation {
  id: number;
  roomReservations: RoomReservation[] = [];
  hotel: Hotel;
  hotelId: number;
  person: Person = new Person();
  user: User;
  specialRequests: string;
  source: CreditCard;
  reservationNo: string;
  checkIn: Date;
  checkOut: Date;
  price: Price;
  status: ReservationStatus = ReservationStatus.APPLIED;
  creationTime: Date;
  review: Review;

  constructor(reservation?: Reservation) {
    if (reservation) {
      Object.assign(this, reservation);
      if (this.hotel) {
        this.load();
      }
      if (reservation.user) {
        this.user = new User(reservation.user);
      }
      this.person = new Person(reservation.person);
      this.checkIn = new Date(this.checkIn);
      this.checkOut = new Date(this.checkOut);
      this.price = new Price(this.price);
    }
    if (this.id) {
      this.creationTime = new Date(this.creationTime);
      if (this.source) {
        this.source = new CreditCard(this.source);
      }
    }
  }

  hasReservedRoom(): boolean {
    return this.roomReservations.length > 0;
  }

  calculateTotalPrice(): Price {
    return this.roomReservations.reduce((totalPrice: Price, roomReservation) => {
      let alternative = roomReservation.alternative;
      totalPrice = totalPrice.add(alternative.price);
      return totalPrice;
    }, new Price());
  }

  getReservedRoomList() {
    return this.hotel.rooms.filter(room => room.alternatives.some(alternative => !!this.getAlternativeCount(alternative.id)));
  }

  getRoomCount(room: Room) {
    return this.roomReservations.filter(roomReservation => roomReservation.alternative.roomId === room.id).length;
  }

  getAlternativeCount(alternativeId: number) {
    return this.getRoomReservations(alternativeId).length;
  }

  getRoomReservations(alternativeId: number) {
    return this.roomReservations.filter(roomReservation => roomReservation.alternativeId === alternativeId);
  }

  getAlternativesByRoom(room: Room) {
    let map = this.roomReservations
      .map(roomReservation => roomReservation.alternative)
      .filter(alternative => alternative.roomId === room.id)
      .reduce((map, alternative) => {
        map[alternative.id] = alternative;
        return map;
      }, []);
    return Object.keys(map).map(alternativeId => map[alternativeId]);
  }

  get nightCount() {
    if (!this.checkIn || !this.checkOut) {
      return null;
    }
    return Reservation.getNightCount(this.checkIn, this.checkOut);
  }

  get creatorType() {
    return this.user && this.user.isAgency() ? 'Agency' : 'User';
  }

  get creator() {
    return this.user ? this.user.toString() : this.person.toString();
  }

  static getNightCount(checkIn: Date, checkOut: Date) {
    return moment(checkOut).diff(checkIn, 'days');
  }

  canCancelBy(user: User) {
    if ([ReservationStatus.APPLIED, ReservationStatus.CONFIRMED].indexOf(this.status) !== -1
      && (this.isCreator(user) || user.hasPermission(Permission.ALL_RESERVATIONS_CRUD))) {
      return true;
    }
    return this.status === ReservationStatus.CONFIRMED && user.hasPermission(Permission.OWNER_HOTELS_CRUD);

  }

  isCreator(user: User) {
    return this.user && user && this.user.id === user.id;
  }

  load() {
    this.hotel = new Hotel(this.hotel);
    this.roomReservations.forEach(roomReservation => {
      roomReservation.alternative = this.hotel.getAlternativeById(roomReservation.alternativeId);
    });
  }

  canChange() {
    return !this.isCancelled() && moment(this.checkOut).isAfter();
  }

  canReview() {
    return this.status === ReservationStatus.CLOSED && moment().diff(moment(this.checkOut), 'days') < 30;
  }

  isCancelled() {
    return this.status === ReservationStatus.CANCELLED;
  }


  findIndexes(alternativeId: number) {
    let indexes = [];
    this.roomReservations.forEach((roomReservation, index) => {
      if (roomReservation.alternative.id === alternativeId) {
        indexes.push(index);
      }
    });
    return indexes;
  }
}
