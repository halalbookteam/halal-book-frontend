import {Address} from './address';
import {Room} from './room';
import {HotelEvaluation} from './hotelEvaluation';
import {Facility} from './facility';
import {Photo} from './photo';
import {Filter, FilterGroup, FilterType} from './filter';
import {BaseTranslatedModel, Translation} from './translation';
import {RecommendedRoomSet} from './recommended-room-set';
import {Currency} from './language';

export enum HotelSortType {
  PRICE,
  STAR,
  REVIEW
}

export enum ChannelManager {
  RESELIVA = "RESELIVA"
}

export class Hotel extends BaseTranslatedModel {
  id: number;
  star: number;
  address: Address;
  evaluation: HotelEvaluation;
  rooms: Room[];
  photos: Photo[];
  primaryPhotoIndex: number;
  facilities: Facility[];
  maxChildAge: number;
  recommendedRooms: RecommendedRoomSet;
  currency: string;
  channelManager: ChannelManager;

  constructor(hotel?: Hotel) {
    super();
    Object.assign(this, hotel);
    if(!this.translations) {
      this.translations = [];
      this.translations.push(new Translation(this.defaultLanguageId));
    }
    this.address = new Address(this.address);
    if(this.evaluation) {
      this.evaluation = new HotelEvaluation(this.evaluation);
    }
    if(this.rooms) {
      this.rooms = this.rooms.map(room => new Room(room));
    }
    if(this.recommendedRooms) {
      this.recommendedRooms = new RecommendedRoomSet(this.recommendedRooms);
      this.loadAlternatives();
      this.recommendedRooms.mapAlternatives();
    }
    if(this.photos) {
      this.photos = this.photos.map(photo => Object.assign(new Photo(), photo));
    }
  }

  private loadAlternatives() {
    this.recommendedRooms.roomReservations.forEach(roomReservation => {
      roomReservation.alternative = this.getAlternativeById(roomReservation.alternativeId);
    });
  }

  findRoomById(roomId: number) {
    return this.rooms.find(room => room.id === roomId);
  }

  getAlternativeById(alternativeId: number) {
    for(let room of this.rooms) {
      for(let alternative of room.alternatives) {
        if(alternative.id === alternativeId) {
          return alternative;
        }
      }
    }
    return null;
  }

  isMatch(group: FilterGroup, filter: Filter) {
    if(group.type === FilterType.STAR) {
      return this.star === filter.value;
    }
    if(group.type === FilterType.REVIEW) {
      if(this.evaluation) {
        let score = Math.floor(this.evaluation.score);
        return filter.value && score > filter.value || !filter.value && score < 6;
      }
      return !filter.value;
    }
    if(group.type === FilterType.FACILITY) {
      return this.facilities.some(facility => facility.id === filter.name);
    }
    return false;
  }

  getPrimaryPhotoUrl() {
    return this.photos[this.primaryPhotoIndex].getFullUrl;
  }
}
