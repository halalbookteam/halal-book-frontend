export class CreditCard {
  id: number;
  sourceId: string;
  brand: string;
  last4: string;
  expirationMonth: number;
  expirationYear: number;
  default: boolean;

  constructor(creditCard?: CreditCard) {
    Object.assign(this, creditCard);
  }
}
