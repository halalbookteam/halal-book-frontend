export enum FilterType {
  REVIEW,
  STAR,
  FACILITY
}

export enum LogicType {
  OR,
  AND
}

export class FilterGroup {
  name: string | number;
  type: FilterType;
  filters: Filter[];

  constructor(name: string | number, type: FilterType) {
    this.name = name;
    this.type = type;
    this.filters = [];
  }

  get logicType(): LogicType {
    return this.type === FilterType.FACILITY ? LogicType.AND : LogicType.OR;
  }
}

export class Filter {
  name: string | number;
  count: number;
  value: any;
  checked: boolean;

  constructor(name: string | number, value: any) {
    this.name = name;
    this.value = value;
  }
}
