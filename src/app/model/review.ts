import {BaseTranslatedModel} from './translation';

export enum ReviewSortType {
  DATE,
  SCORE
}

export enum ReviewPurposeType {
  BUSINESS = 'BUSINESS',
  ENTERTAINMENT = 'ENTERTAINMENT',
  OTHER = 'OTHER'
}

export enum ReviewScore {
  POOR = 2.5,
  AVERAGE = 5,
  GOOD = 7.5,
  GREAT = 10
}

export enum ScoreSecondLevel {
  Superb = 'Superb',
  Good = 'Good',
  Okay = 'Okay',
  Poor = 'Poor',
  VeryPoor = 'VeryPoor',
}

export class ReviewCategory extends BaseTranslatedModel {
  id: number;

  constructor(reviewCategory?: ReviewCategory) {
    super();
    Object.assign(this, reviewCategory);
  }
}

export class ReviewCategoryScore {
  id: number;
  category: ReviewCategory;
  score: ReviewScore;
}

export enum ReviewStatus {
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
}

export enum TravellerType {
  Families = 'Families',
  Couples = 'Couples',
  Friends = 'Friends',
  Solo = 'Solo',
  Business = 'Business',
}

export class TravelWith {
  alone: boolean;
  friends: boolean;
  partner: boolean;
  family: boolean;
  colleague: boolean;
  pet: boolean;

  constructor(travelWith: TravelWith) {
    Object.assign(this, travelWith);
  }

  get suitableTravellerTypes() {
    return Object.keys(TravellerType).map(key => TravellerType[key]).filter(travellerType => this.isTravellerType(travellerType));
  }

  isTravellerType(travellerType: TravellerType) {
    switch (travellerType) {
      case TravellerType.Solo: {
        return this.alone;
      }
      case TravellerType.Business: {
        return this.colleague;
      }
      case TravellerType.Friends: {
        return this.friends;
      }
      case TravellerType.Couples: {
        return this.partner;
      }
      case TravellerType.Families: {
        return this.family;
      }
    }
    return undefined;
  }
}

export class Review {
  id: number;
  reservationId: number;
  purpose: ReviewPurposeType;
  categoryScores: ReviewCategoryScore[];
  likeComment: string;
  dislikeComment: string;
  status: ReviewStatus;
  title: string;
  travelWith;
  includeMyName: boolean;
  time: Date;
  reviewerName: string;
  reviewerCountry: string;
  score: number;
  languageId: number;

  constructor(review: Review) {
    Object.assign(this, review);
    this.score = this.calculateScore();
    this.travelWith = new TravelWith(this.travelWith);
    if(this.time) {
      this.time = new Date(this.time);
    }
    if(this.categoryScores) {
      this.categoryScores.forEach(score => score.category = new ReviewCategory(score.category));
    }
  }

  private calculateScore() {
    if (this.categoryScores) {
      return this.categoryScores.reduce((sum: number, score) => {
        return sum + score.score;
      }, 0) / this.categoryScores.length;
    }
    return null;
  }

  getScoreSecondLevel(score: number) {
    if (score >= 9) {
      return ScoreSecondLevel.Superb;
    }
    if (score >= 7) {
      return ScoreSecondLevel.Good;
    }
    if (score >= 5) {
      return ScoreSecondLevel.Okay;
    }
    if (score >= 3) {
      return ScoreSecondLevel.Poor;
    }
    return ScoreSecondLevel.VeryPoor;
  }
}
