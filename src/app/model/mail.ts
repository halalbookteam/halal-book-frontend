export enum RecipientType {
  NEWSLETTER_SUBSCRIBER = "NEWSLETTER_SUBSCRIBER",
  ADMIN = "ADMIN",
  HOTEL_OWNER = "HOTEL_OWNER",
}

export enum MailTemplateParameterType {
  TEXT = "TEXT",
  HTML = "HTML",
}

export class MailTemplateParameter {
  name: string;
  key: string;
  type: MailTemplateParameterType;
  value: string;
}

export class MailTemplate {
  name: string;
  parameters: MailTemplateParameter [];
}

export class MailData {
  recipientType: RecipientType = RecipientType.NEWSLETTER_SUBSCRIBER;
  recipientList: string[];
  subject: string;
  template: MailTemplate;
}
