export class Price {
  amount: number = 0;
  currency: string;

  constructor(price?: Price) {
    Object.assign(this, price);
  }

  multiply(value: number) {
    return Object.assign(new Price(), this, {
      amount: this.amount * value
    });
  }

  add(price: Price) {
    if (this.amount && this.currency !== price.currency) {
      throw new Error('Price Error. Currencies are different: ' + this.currency + ', ' + price.currency);
    }
    return Object.assign(new Price(), price, {
      amount: this.amount + price.amount
    });
  }
}
