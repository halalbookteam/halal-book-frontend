export class Translation {
  languageId: number;

  constructor(languageId: number) {
    this.languageId = languageId;
  }
}

export class BaseTranslatedModel {
  defaultLanguageId: number = 2;
  translations: Translation[] = [];

  get(field: string, languageId?: number) {
    if(!languageId || !this.translations[languageId]) {
      languageId = this.defaultLanguageId;
    }
    let translatedField = this.getForLanguage(languageId, field);
    return translatedField ? translatedField : this.getForLanguage(this.defaultLanguageId, field);
  }

  getTranslation(languageId?: number, create?: boolean): any {
    languageId = languageId || this.defaultLanguageId;
    let translation = this.translations.find(translation => translation.languageId === languageId);
    if(!translation && create) {
      translation = new Translation(languageId);
      this.translations.push(translation);
    }
    return translation;
  }

  private getForLanguage(languageId: number, field: string) {
    let translation = this.getTranslation(languageId);
    return translation ? translation[field] : null;
  }
}
