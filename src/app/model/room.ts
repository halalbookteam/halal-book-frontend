import {Price} from './price';
import {Facility} from './facility';
import {Photo} from './photo';
import {MealOption, PaymentOption} from './room-alternative-options';
import * as moment from 'moment';
import {min} from 'moment';
import {BaseTranslatedModel} from './translation';

export class RoomAlternative {
  id: number;
  price: Price;
  hotelId: number;
  roomId: number;
  maxAdult: number;
  maxChild: number;
  basePriceRatio: number;
  mealOption: MealOption;
  paymentOption: PaymentOption;

  constructor(alternative?: RoomAlternative) {
    Object.assign(this, alternative);
    this.mealOption = new MealOption(this.mealOption);
    this.paymentOption = new PaymentOption(this.paymentOption);
    if(this.price) {
      this.price = new Price(this.price);
    }
  }
}

export class RoomRate {
  id: number;
  date: Date;
  availableRoomCount: number;
  minNightCount: number;
  basePrice: Price;

  constructor(rate?: RoomRate) {
    Object.assign(this, rate);
    if(this.basePrice) {
      this.basePrice = new Price(this.basePrice);
    }
    this.date = new Date(this.date);
  }
}

export class Room extends BaseTranslatedModel {
  id: number;
  size: number;
  hotelId: number;
  primaryPhotoIndex: number;
  photos: Photo[];
  facilities: Facility[];
  alternatives: RoomAlternative[];
  rates: RoomRate[];
  availableCount: number;

  constructor(room?: Room) {
    super();
    Object.assign(this, room);
    if(this.photos) {
      this.photos = this.photos.map(photo => Object.assign(new Photo(), photo));
    }
    if(this.alternatives) {
      this.alternatives = this.alternatives.map(alternative => new RoomAlternative(alternative)).sort((a, b) => {
        let difference = a.maxAdult - b.maxAdult;
        return difference ? difference : a.basePriceRatio - b.basePriceRatio;
      });
    }
    if(this.rates) {
      this.rates = this.rates.map(rate => new RoomRate(rate))
    }
    if(this.facilities) {
      this.facilities = this.facilities.map(facility => new Facility(facility))
    }
  }

  findRate(date: Date) {
    if(!this.rates) {
      return null;
    }
    return this.rates.find(rate => rate.date.getTime() === date.getTime());
  }
}
