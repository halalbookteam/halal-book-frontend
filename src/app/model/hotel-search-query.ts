import {Place} from './place';
import {Reservation} from './reservation';

export class HotelSearchQuery {
  place: Place;
  roomCount: number = 1;
  childCount: number = 0;
  adultCount: number = 2;
  checkIn: Date;
  checkOut: Date;
  childAges = [];

  constructor(query?: HotelSearchQuery) {
    if(query) {
      Object.assign(this, query);
      if(this.checkIn) {
        this.checkIn = new Date(this.checkIn);
      }
      if(this.checkOut) {
        this.checkOut = new Date(this.checkOut);
      }
      if(this.place) {
        this.place = new Place(this.place);
      }
    }
  }

  get nightCount() {
    return this.checkIn ? Reservation.getNightCount(this.checkIn, this.checkOut) : null;
  }
}
