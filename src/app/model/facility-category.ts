import {Facility} from './facility';
import {BaseTranslatedModel} from './translation';

export enum CategoryType {
  HOTEL = "HOTEL",
  ROOM  = "ROOM"
}

export class FacilityCategory extends BaseTranslatedModel {
  id          : number;
  name : string;
  description : string;
  halal       : boolean;
  type        : CategoryType = CategoryType.HOTEL;
  filter      : boolean;
  facilities  : Facility[] = [];

  constructor(facilityCategory?: FacilityCategory) {
    super();
    Object.assign(this, facilityCategory);
    if(this.facilities) {
      this.facilities = this.facilities.map(facility => new Facility(facility));
    }
  }
}
