export class CurrencyExchange {
  id: string;
  ratio: number;
  time: number;

  constructor(id?: string, ratio?: number) {
    this.id = id;
    this.ratio = ratio;
    this.time = Date.now();
  }

  isExpired() {
    return Date.now() - this.time > 1000 * 60 * 5;
  }
}
