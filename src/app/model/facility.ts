import {BaseTranslatedModel} from './translation';

export class Facility extends BaseTranslatedModel {
  id: number;
  name: string;
  description: string;
  iconUrl: string;
  chargeable: boolean;
  categoryId: number;
  check: boolean;

  constructor(facility?: Facility) {
    super();
    Object.assign(this, facility);
  }
}
