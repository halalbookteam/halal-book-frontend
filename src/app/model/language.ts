export class Language {
  id: number;
  code: string;
  name: string;
  currency: Currency;
}

export class Currency {
  id: number;
  code: string;
  symbol: string;
  name: string;
  shortName: string;
  fractionDigits: number;
}
