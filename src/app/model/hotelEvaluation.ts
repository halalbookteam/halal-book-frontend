import {Review, ReviewCategory} from './review';

export class EvaluationCategoryScore {
  category: ReviewCategory;
  score: number;

  constructor(score: EvaluationCategoryScore) {
    Object.assign(this, score);
    this.category = new ReviewCategory(this.category);
  }
}

export class HotelEvaluation {
  reviewCount: number;
  score: number;
  evaluationCategoryScores: EvaluationCategoryScore[];
  reviews: Review[];

  constructor(evaluation: HotelEvaluation) {
    Object.assign(this, evaluation);
    if(this.evaluationCategoryScores) {
      this.evaluationCategoryScores = this.evaluationCategoryScores.map(score => new EvaluationCategoryScore(score));
    }
    if(this.reviews) {
      this.reviews = this.reviews.map(review => new Review(review));
    }
  }
}
