export class Person {
  id: number;
  name: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  country: string;

  constructor(person?: Person) {
    Object.assign(this, person);
  }

  toString() {
    return this.name + " " + this.lastName;
  }
}
