import { environment } from '../../environments/environment';

export class Photo {
  id: number;
  url: string;

  constructor(url?: string) {
    this.url = url;
  }

  get getFullUrl(): string {
    if(!this.url) {
      return null;
    }
    return this.url.startsWith("http") ? this.url : (environment.apiURL + this.url);
  }
}
