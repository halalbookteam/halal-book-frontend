import {BaseTranslatedModel} from './translation';

export class Address extends BaseTranslatedModel {
  id: number;
  postalCode: string;
  lon: number;
  lat: number;

  constructor(address?: Address) {
    super();
    Object.assign(this, address);
  }
}
