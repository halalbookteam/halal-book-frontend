import {Photo} from './photo';
import {Currency, Language} from './language';
import {CreditCard} from './credit-card';

export enum Permission {
  ALL_HOTELS_CRUD = "ALL_HOTELS_CRUD",
  OWNER_HOTELS_CRUD = "OWNER_HOTELS_CRUD",
  FACILITY_CRUD = "FACILITY_CRUD",
  USER_CRUD = "USER_CRUD",
  HOTEL_RESERVATIONS_CRUD = "HOTEL_RESERVATIONS_CRUD",
  ALL_RESERVATIONS_CRUD = "ALL_RESERVATIONS_CRUD",
  SEND_MAIL = "SEND_MAIL",
  REVIEW_REVIEW = "REVIEW_REVIEW",
}

export class Role {
  id: number;
  name: string;
}

export class UserProfile {
  id: number;
  displayName: string;
  newsletter: boolean;
  avatar: Photo;
  defaultCurrency: Currency;
  defaultLanguage: Language;

  constructor(profile: UserProfile) {
    Object.assign(this, profile);
    if(this.avatar) {
      this.avatar = new Photo(this.avatar.url);
    }
  }
}

export class User {
  id: number;
  email: string;
  name: string;
  surname: string;
  active: boolean = true;
  createTime: Date;
  roles: Role[] = [];
  permissions: Permission[];
  profile: UserProfile;
  cards: CreditCard[];

  constructor(user?: User) {
    Object.assign(this, user);
    if(this.createTime) {
      this.createTime = new Date(this.createTime);
    }
    if(this.profile) {
      this.profile = new UserProfile(this.profile);
    }
    if(this.cards) {
      this.cards.map(card => new CreditCard(card));
    }
  }

  toString() {
    return this.profile ? this.profile.displayName : this.name + " " + this.surname;
  }

  hasPermission(permission: Permission) {
    return this.permissions.some(p => p === permission);
  }

  removeRole(id: number) {
    const index = this.roles.findIndex(role => role.id === id);

    if (index >= 0) {
      this.roles.splice(index, 1);
      return true;
    }
    return false;
  }

  addRole(role: Role) {
    this.roles.push(role);
  }

  hasRole(role: Role) {
    return this.roles && this.roles.some(r => r.id === role.id);
  }

  isAgency() {
    //TODO implement
    return false;
  }
}
