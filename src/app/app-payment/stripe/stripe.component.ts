import { Component, OnInit } from '@angular/core';
import {ElementOptions, ElementsOptions, StripeService} from '@nomadreservations/ngx-stripe';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss']
})
export class StripeComponent implements OnInit {
  name: string;
  error: any;
  complete = false;
  element: any;
  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#276fd3',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };

  elementsOptions: ElementsOptions = {
    locale: 'auto'
  };

  constructor(
    private _stripe: StripeService,
    private languageService: LanguageService
  ) {
    //TODO ngx-stripe should fix the language problem
    this.elementsOptions.locale = this.languageService.getLanguageCode().toLowerCase();
  }

  ngOnInit() {
  }

  cardUpdated(result) {
    this.element = result.element;
    this.complete = result.card.complete;
    this.error = undefined;
  }

  getCardToken() {
    return this._stripe.createToken(this.element, {
      name: this.name,
    });
  }

  get valid(): boolean {
    return this.complete && !!this.name;
  }

}
