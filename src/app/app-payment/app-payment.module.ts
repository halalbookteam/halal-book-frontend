import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxStripeModule} from '@nomadreservations/ngx-stripe';
import {environment} from '../../environments/environment';
import {StripeComponent} from './stripe/stripe.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppCommonModule} from '../app-common/app-common.module';
import {FormsModule} from '@angular/forms';
import {PaymentDetailComponent} from './payment-detail/payment-detail.component';
import {CreditCardComponent} from './credit-card/credit-card.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppCommonModule,
    FormsModule,
    NgxStripeModule.forRoot(environment.stripe.apiKey),
  ],
  declarations: [
    StripeComponent,
    PaymentDetailComponent,
    CreditCardComponent,
  ],
  exports: [
    StripeComponent,
    PaymentDetailComponent,
    CreditCardComponent,
  ]
})
export class AppPaymentModule { }
