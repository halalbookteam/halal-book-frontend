import {Component, OnInit, ViewChild} from '@angular/core';
import {StripeComponent} from '../stripe/stripe.component';
import {CreditCard} from '../../model/credit-card';
import {of} from 'rxjs';
import {AuthenticationService} from '../../service/authentication.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.scss']
})
export class PaymentDetailComponent implements OnInit {
  @ViewChild("stripe")
  private stripe: StripeComponent;

  selectedCardId: number;

  existingCards: CreditCard[];

  newCard: boolean;

  constructor(private authenticationService: AuthenticationService) {
    let currentUser = this.authenticationService.getCurrentUser();
    if(currentUser) {
      this.existingCards = currentUser.cards;
    }
    this.newCard = !this.existingCards;
    if(this.existingCards) {
      this.selectedCardId = this.existingCards.find(card => card.default).id;
    }
  }

  ngOnInit() {
  }

  get valid(): boolean {
    return !this.newCard || this.stripe && this.stripe.valid;
  }

  get card() {
    if(this.newCard) {
      return this.stripe.getCardToken().pipe(map(result => {
        let creditCard = new CreditCard();
        creditCard.sourceId = result.token.id;
        return creditCard;
      }));
    }
    return of(this.existingCards.find(card => card.id === this.selectedCardId));
  }
}
