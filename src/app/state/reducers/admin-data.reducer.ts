import {AdminActionType, ReduxAction, SystemActionType, UserActionType} from '../app.action';
import {AppState} from './app.reducer';
import * as _ from 'lodash';
import {Facility} from '../../model/facility';
import {CategoryType, FacilityCategory} from '../../model/facility-category';
import {Role, User} from '../../model/user';
import {MailTemplate} from '../../model/mail';

export interface AdminDataState {
  categories: { [id: string]: FacilityCategory };
  roles: Role[];
  mailTemplates: MailTemplate[];
}

const initialState: AdminDataState = {
  categories: null,
  roles: null,
  mailTemplates: null,
};

export const AdminDataReducer =
  function (state: AdminDataState = initialState, action: ReduxAction): AdminDataState {
    const clonedState = _.cloneDeep(state);

    switch (action.type) {
      case UserActionType.LOGIN:
      case UserActionType.LOGOUT: {
        clonedState.roles = initialState.roles;
        return clonedState;
      }
      case SystemActionType.INITIALIZE: {
        let payload: AppState = action.payload;
        clonedState.categories = payload.adminData.categories;
        if(clonedState.categories) {
          Object.keys(clonedState.categories).forEach(id => {
            clonedState.categories[id] = new FacilityCategory(clonedState.categories[id]);
          });
        }

        return clonedState;
      }
      case AdminActionType.FETCH_CATEGORIES: {
        clonedState.categories = action.payload.reduce((categories: { [id: string]: FacilityCategory }, category) => {
          categories[category.id] = category;
          return categories;
        }, {});(action.payload);
        return clonedState;
      }
      case AdminActionType.CREATE_FACILITY: {
        let facility: Facility = action.payload;
        clonedState.categories[facility.categoryId].facilities.push(facility);
        return clonedState;
      }
      case AdminActionType.UPDATE_FACILITY: {
        let updatedFacility: Facility = action.payload;
        let facility = clonedState.categories[updatedFacility.categoryId].facilities.find(fac => fac.id === updatedFacility.id);
        Object.assign(facility, updatedFacility);
        return clonedState;
      }
      case AdminActionType.DELETE_FACILITY: {
        let deletedFacility: Facility = action.payload;
        let facilities = clonedState.categories[deletedFacility.categoryId].facilities;
        let index = facilities.findIndex(fac => fac.id === deletedFacility.id);
        facilities.splice(index, 1);
        return clonedState;
      }
      case AdminActionType.CREATE_CATEGORY: {
        const category: FacilityCategory = action.payload;
        clonedState.categories[category.id] = category;
        return clonedState;
      }
      case AdminActionType.UPDATE_CATEGORY: {
        const updatedCategory: FacilityCategory = action.payload;
        const category = clonedState.categories[updatedCategory.id];
        Object.assign(category, updatedCategory);
        return clonedState;
      }
      case AdminActionType.DELETE_CATEGORY: {
        delete clonedState.categories[action.payload];
        return clonedState;
      }
      case AdminActionType.FETCH_TEMPLATES: {
        clonedState.mailTemplates = action.payload;
        return clonedState;
      }
      case UserActionType.FETCH_USERS: {
        let users: User[] = action.payload;
        let roles: {[id: number]: Role} = {};
        users.forEach(user => user.roles.forEach(role => roles[role.id] = role));
        clonedState.roles = Object.keys(roles).map(id => roles[id]);
        return clonedState;
      }
      default:
        return state;
    }
  };

const getAdminDataState = (state: AppState): AdminDataState => _.cloneDeep(state.adminData);

export const getCategories = (state, categoryType?: CategoryType) => {
  let categories = getAdminDataState(state).categories;
  if(!categories) {
    return null;
  }
  let categoryList = Object.keys(categories).map((id) => categories[id]);
  if(!categoryType) {
    return categoryList;
  }
  return categoryList.filter(category => category.type === categoryType);
};

export const getCategory = (state, id) => {
  if(state.adminData.categories) {
    return getAdminDataState(state).categories[id];
  }
};

export const getAllRoles = (state) => {
  return getAdminDataState(state).roles;
};

export const getMailTemplates = (state) => {
  return getAdminDataState(state).mailTemplates;
};
