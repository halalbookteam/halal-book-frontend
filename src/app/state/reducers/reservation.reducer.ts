import {ReduxAction, ReservationActionType, SystemActionType, UserActionType} from '../app.action';
import {AppState} from './app.reducer';
import * as _ from 'lodash';
import {Reservation} from '../../model/reservation';
import {Review} from '../../model/review';

export interface ReservationState {
  activeReservation: Reservation;
  reservations: Reservation[];
  myReservations: Reservation[];
  reviews: Review[];
}

const initialState: ReservationState = {
  activeReservation: null,
  reservations: [],
  myReservations: [],
  reviews: [],
};

export const ReservationReducer =
  function (state: ReservationState = initialState, action: ReduxAction): ReservationState {
    const clonedState = _.cloneDeep(state);
    switch (action.type) {
      case UserActionType.LOGIN:
      case UserActionType.LOGOUT: {
        return initialState;
      }
      case SystemActionType.INITIALIZE: {
        let payload: AppState = action.payload;
        if (payload.reservation.activeReservation) {
          clonedState.activeReservation = new Reservation(payload.reservation.activeReservation);
        }
        if (payload.reservation.reservations) {
          clonedState.reservations = payload.reservation.reservations.map(reservation => new Reservation(reservation));
        }
        return clonedState;
      }
      case ReservationActionType.BOOK: {
        clonedState.activeReservation = action.payload;
        return clonedState;
      }
      case ReservationActionType.CREATE_RESERVATION: {
        clonedState.activeReservation = action.payload;
        clonedState.reservations.push(action.payload);
        return clonedState;
      }
      case ReservationActionType.UPDATE_RESERVATION: {
        let updatedRes: Reservation = action.payload;
        let reservation = clonedState.reservations.find(res => res.id === updatedRes.id);
        if(reservation) {
          Object.assign(reservation, updatedRes);
        }
        reservation = clonedState.myReservations.find(res => res.id === updatedRes.id);
        if(reservation) {
          Object.assign(reservation, updatedRes);
        }
        if(clonedState.activeReservation && clonedState.activeReservation.id === updatedRes.id) {
          Object.assign(clonedState.activeReservation, updatedRes);
        }
        return clonedState;
      }
      case ReservationActionType.SAVE_PAYMENT_DETAIL: {
        clonedState.activeReservation = null;
        return clonedState;
      }
      case ReservationActionType.FETCH_RESERVATIONS: {
        clonedState.reservations = action.payload;
        return clonedState;
      }
      case ReservationActionType.FETCH_MY_RESERVATIONS: {
        clonedState.myReservations = action.payload;
        return clonedState;
      }
      case ReservationActionType.CHANGE_RESERVATION: {
        clonedState.activeReservation = action.payload;
        return clonedState;
      }
      case ReservationActionType.SAVE_REVIEW: {
        let review: Review = action.payload;
        let reservation = clonedState.myReservations.find(res => res.id === review.reservationId);
        reservation.review = review;
        return clonedState;
      }
      case ReservationActionType.FETCH_REVIEWS: {
        clonedState.reviews = action.payload;
        return clonedState;
      }
      case ReservationActionType.REVIEW_REVIEW: {
        let review = action.payload;
        let existing = clonedState.reviews.find(rev => rev.id === review.id);
        Object.assign(existing, review);
        return clonedState;
      }
      default:
        return state;
    }
  };

const getReservationState = (state: AppState): ReservationState => _.cloneDeep(state.reservation);

export const getActiveReservation = (state: AppState): Reservation => {
  return getReservationState(state).activeReservation;
};

export const getReservations = (state: AppState): Reservation[] => {
  return getReservationState(state).reservations;
};

export const getMyReservations = (state: AppState): Reservation[] => {
  return getReservationState(state).myReservations;
};

export const findReservation = (state: AppState, reservationId): Reservation => {
  return getReservationState(state).reservations.find(res => res.id === reservationId);
};

export const findMyReservation = (state: AppState, reservationId): Reservation => {
  return getReservationState(state).myReservations.find(res => res.id === reservationId);
};

export const getReviews = (state: AppState): Review[] => {
  return getReservationState(state).reviews;
};
