import {LanguageActionType, ReduxAction, SystemActionType} from '../app.action';
import {AppState} from './app.reducer';
import * as _ from 'lodash';
import {Currency, Language} from '../../model/language';
import {CurrencyExchange} from '../../model/currencyExchange';

export interface LanguageState {
  languageCode : string;
  currencyCode : string;
  languageMap: { [id: string]: Language };
  currencyMap: { [id: string]: Currency };
  exchangeMap: { [id: string]: CurrencyExchange };
}

const initialState: LanguageState = {
  languageCode: null,
  currencyCode: null,
  languageMap: null,
  currencyMap: null,
  exchangeMap: { }
};

export const LanguageReducer =
  function (state: LanguageState = initialState, action: ReduxAction): LanguageState {
    let clonedState = _.cloneDeep(state);
    switch (action.type) {
      case SystemActionType.INITIALIZE: {
        let payload: AppState = action.payload;
        clonedState.languageCode = payload.language.languageCode;
        clonedState.currencyCode = payload.language.currencyCode;
        clonedState.exchangeMap = payload.language.exchangeMap;
        if(clonedState.exchangeMap) {
          Object.keys(clonedState.exchangeMap).forEach(id => clonedState.exchangeMap[id] = Object.assign(new CurrencyExchange(), clonedState.exchangeMap[id]))
        }
        return clonedState;
      }
      case LanguageActionType.SET_LANGUAGE: {
        clonedState.languageCode = action.payload;
        return clonedState;
      }
      case LanguageActionType.SET_CURRENCY: {
        clonedState.currencyCode = action.payload;
        return clonedState;
      }
      case LanguageActionType.SET_EXCHANGE: {
        let exchange: CurrencyExchange = action.payload;
        clonedState.exchangeMap[exchange.id] = exchange;
        return clonedState;
      }
      case LanguageActionType.FETCH_CURRENCIES_AND_LANGUAGES: {
        clonedState.languageMap = {};
        clonedState.currencyMap = {};
        action.payload.languages.forEach(lang => clonedState.languageMap[lang.code] = lang);
        action.payload.currencies.forEach(cur => clonedState.currencyMap[cur.code] = cur);
        return clonedState;
      }
      default:
        return state;
    }
  };

const getLanguageState = (state: AppState): LanguageState => _.cloneDeep(state.language);

export const getLanguage = (state: AppState): Language => {
  return state.language.languageCode && state.language.currencyMap ? getLanguageState(state).languageMap[state.language.languageCode] : null;
};

export const getLanguageCode = (state: AppState): string => {
  return state.language.languageCode;
};

export const getCurrency = (state: AppState): Currency => {
  return getLanguageState(state).currencyMap[state.language.currencyCode];
};

export const getCurrencyCode = (state: AppState): string => {
  return state.language.currencyCode;
};

export const getLanguageByCode = (state: AppState, langCode: string = "en"): Language => {
  let language = state.language.languageMap[langCode.toLowerCase()];
  return language ? language : state.language.languageMap["en"];
};

export const getCurrencyByCode = (state: AppState, curCode: string = "USD"): Currency => {
  let currency = state.language.currencyMap[curCode.toUpperCase()];
  return currency ? currency : state.language.currencyMap["USD"];
};

export const getLanguages = (state: AppState): Language[] => {
  let languages = getLanguageState(state).languageMap;
  return languages ? Object.keys(languages).map(lang => languages[lang]) : null;
};

export const getCurrencies = (state: AppState): Currency[] => {
  let currencies = getLanguageState(state).currencyMap;
  return currencies ? Object.keys(currencies).map(cur => currencies[cur]) : null;
};

export const getExchange = (state: AppState, exchangeId): CurrencyExchange => {
  return state.language.exchangeMap[exchangeId];
};


