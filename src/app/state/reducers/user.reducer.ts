import {ReduxAction, ReservationActionType, SystemActionType, UserActionType} from '../app.action';
import {AppState} from './app.reducer';
import * as _ from 'lodash';
import {User} from '../../model/user';

export interface UserState {
  currentUser: User;
  users: User[];
}

const initialState: UserState = {
  currentUser: null,
  users: null,
};

export const UserReducer =
  function (state: UserState = initialState, action: ReduxAction): UserState {
    let clonedState = _.cloneDeep(state);
    switch (action.type) {
      case SystemActionType.INITIALIZE: {
        let payload: AppState = action.payload;
        if(payload.user.currentUser) {
          clonedState.currentUser = new User(payload.user.currentUser);
        }
        return clonedState;
      }
      case UserActionType.FETCH_USERS: {
        clonedState.users = action.payload;
        return clonedState;
      }
      case UserActionType.CREATE_USER: {
        clonedState.users.push(action.payload);
        return clonedState;
      }
      case UserActionType.UPDATE_USER: {
        let updatedUser: User = action.payload;
        let existingUser = clonedState.users.find(u => u.id === updatedUser.id);
        if(existingUser) {
          Object.assign(existingUser, updatedUser);
        }
        return clonedState;
      }
      case UserActionType.UPDATE_PROFILE: {
        Object.assign(clonedState.currentUser.profile, action.payload);
        return clonedState;
      }
      case UserActionType.DELETE_USER: {
        let index = clonedState.users.findIndex(u => u.id === action.payload);
        clonedState.users.splice(index, 1);
        return clonedState;
      }
      case UserActionType.LOGIN: {
        clonedState = _.cloneDeep(initialState);
        clonedState.currentUser = action.payload;
        return clonedState;
      }
      case UserActionType.LOGOUT: {
        return initialState;
      }
      case ReservationActionType.SAVE_PAYMENT_DETAIL: {
        if(action.payload) {
          clonedState.currentUser.cards.push(action.payload);
        }
        return clonedState;
      }
      default:
        return state;
    }
  };

const getUserState = (state: AppState): UserState => _.cloneDeep(state.user);

export const getCurrentUser = (state: AppState): User => {
  return getUserState(state).currentUser;
};

export const getUsers = (state: AppState): User[] => {
  return getUserState(state).users;
};

export const findUser = (state: AppState, id: number): User => {
  return getUserState(state).users.find(user => user.id === id);
};


