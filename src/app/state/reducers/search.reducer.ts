import {HotelsActionType, ReduxAction, SearchActionType, SystemActionType} from '../app.action';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import {AppState} from './app.reducer';
import * as _ from 'lodash';
import {Reservation} from '../../model/reservation';
import {FilterGroup} from '../../model/filter';
import {Hotel} from '../../model/hotel';
import {Place} from '../../model/place';
import {Room} from '../../model/room';

export interface SearchState {
  query: HotelSearchQuery;
  hotelMap: { [id: string]: Hotel };
  filterGroups: FilterGroup[];
  places: Place[];
  searched: boolean;
}

const initialState: SearchState = {
  query: new HotelSearchQuery(),
  hotelMap: {},
  filterGroups: [],
  places: null,
  searched: false,
};

export const SearchReducer =
  function (state: SearchState = initialState, action: ReduxAction): SearchState {
    let clonedState = _.cloneDeep(state);
    switch (action.type) {
      case SystemActionType.INITIALIZE: {
        let payload: AppState = action.payload;
        clonedState = payload.search;
        if (clonedState.query) {
          clonedState.query = new HotelSearchQuery(clonedState.query);
        }
        if (clonedState.hotelMap) {
          Object.keys(clonedState.hotelMap).forEach(id => {
            clonedState.hotelMap[id] = new Hotel(clonedState.hotelMap[id]);
          });
        }
        if (clonedState.places) {
          Object.keys(clonedState.places).forEach(id => {
            clonedState.places[id] = new Place(clonedState.places[id]);
          });
        }
        if(clonedState.filterGroups) {
          clonedState.filterGroups = clonedState.filterGroups.map(gr => Object.assign(new FilterGroup(null, null), gr));
        }
        return clonedState;
      }
      case SearchActionType.SEARCH: {
        clonedState.query = action.payload.query;
        let hotels: Hotel[] = action.payload.hotels;
        clonedState.hotelMap = {};
        hotels.forEach(hotel => {
          clonedState.hotelMap[hotel.id] = hotel;
        });
        clonedState.searched = true;
        return clonedState;
      }
      case SearchActionType.LOAD_FILTER_GROUPS: {
        clonedState.filterGroups = action.payload;
        return clonedState;
      }
      case SearchActionType.LOAD_PLACES: {
        clonedState.places = action.payload;
        return clonedState;
      }
      case SearchActionType.FILTER_CHANGED: {
        let changedFilterGroup = action.payload.group;
        let changedFilter = action.payload.filter;

        let stateFilterGroup = clonedState.filterGroups.find(group => group.name === changedFilterGroup.name);
        let stateFilter = stateFilterGroup.filters.find(filter => filter.name === changedFilter.name);
        stateFilter.checked = changedFilter.checked;

        return clonedState;
      }
      case HotelsActionType.UPDATE_HOTEL_SUCCESS: {
        const updatedHotel: Hotel = action.payload;
        if(clonedState.hotelMap[updatedHotel.id]) {
          Object.assign(clonedState.hotelMap[updatedHotel.id], updatedHotel);
        }
        return clonedState;
      }
      case HotelsActionType.UPDATE_ROOM_SUCCESS: {
        const updatedRoom: Room = action.payload;
        if(clonedState.hotelMap[updatedRoom.hotelId]) {
          let room = clonedState.hotelMap[updatedRoom.hotelId].rooms.find(r => r.id === updatedRoom.id);
          Object.assign(room, updatedRoom);
        }
        return clonedState;
      }
      case SearchActionType.FETCH_HOTEL_REVIEWS: {
        let hotel = clonedState.hotelMap[action.payload.hotelId];
        if(hotel) {
          hotel.evaluation.reviews = action.payload.reviews;
        }
        return clonedState;
      }
      default:
        return state;
    }
  };

const getSearchState = (state: AppState): SearchState => _.cloneDeep(state.search);

export const getSearchQuery = (state: AppState): HotelSearchQuery => {
  return getSearchState(state).query;
};

export const getNightCount = (state: AppState): number => {
  let query = state.search.query;
  if (!query || !query.checkIn || !query.checkOut) {
    return 0;
  }

  return Reservation.getNightCount(query.checkIn, query.checkOut);
};

export const getFilterGroups = (state: AppState): FilterGroup[] => {
  return getSearchState(state).filterGroups;
};

const getHotelMap = (state) => getSearchState(state).hotelMap;

export const getAllHotels = (state) => {
  let hotelMap = getHotelMap(state);
  return Object.keys(hotelMap).map((id) => hotelMap[id]);
};

export const getHotel = (state: AppState, hotelId): Hotel => {
  return getHotelMap(state)[hotelId];
};

export const getPlaces = (state: AppState): Place[] => {
  return getSearchState(state).places;
};
export const isSearched = (state: AppState): boolean => {
  return state.search.searched;
};

