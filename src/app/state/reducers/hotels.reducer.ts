import {HotelsActionType, ReduxAction, SystemActionType, UserActionType} from '../app.action';
import {AppState} from './app.reducer';
import * as _ from 'lodash';
import {Hotel} from '../../model/hotel';
import {Room, RoomAlternative, RoomRate} from '../../model/room';

export interface HotelsState {
  hotels: Hotel[];
  progressing: boolean;
}

const initialState: HotelsState = {
  hotels: null,
  progressing: false,
};

export const HotelsReducer =
  function (state: HotelsState = initialState, action: ReduxAction): HotelsState {
    const clonedState = _.cloneDeep(state);
    switch (action.type) {
      case UserActionType.LOGIN:
      case UserActionType.LOGOUT: {
        return initialState;
      }
      case SystemActionType.INITIALIZE: {
        let payload: AppState = action.payload;
        if(payload.hotels.hotels) {
          clonedState.hotels = payload.hotels.hotels.map(hotel => new Hotel(hotel));
        }
        return clonedState;
      }
      case HotelsActionType.CREATE_HOTEL:
      case HotelsActionType.UPDATE_HOTEL:
      case HotelsActionType.DELETE_HOTEL:
      case HotelsActionType.CREATE_ROOM:
      case HotelsActionType.UPDATE_ROOM:
      case HotelsActionType.DELETE_ROOM:
      case HotelsActionType.CREATE_ALTERNATIVE:
      case HotelsActionType.UPDATE_ALTERNATIVE:
      case HotelsActionType.DELETE_ALTERNATIVE: {
        clonedState.progressing = true;
        return clonedState;
      }
      case HotelsActionType.FETCH_HOTELS: {
        clonedState.hotels = action.payload;
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.CREATE_HOTEL_SUCCESS: {
        clonedState.hotels.push(action.payload);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.UPDATE_HOTEL_SUCCESS: {
        const updatedHotel: Hotel = action.payload;
        const hotel = clonedState.hotels.find(hotel => hotel.id === updatedHotel.id);
        Object.assign(hotel, updatedHotel);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.DELETE_HOTEL_SUCCESS: {
        const hotelId: number = action.payload;
        const index = clonedState.hotels.findIndex(hotel => hotel.id === hotelId);
        clonedState.hotels.splice(index, 1);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.CREATE_ROOM_SUCCESS: {
        let room: Room = action.payload;
        let hotel = clonedState.hotels.find(hotel => hotel.id === room.hotelId);
        hotel.rooms.push(room);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.UPDATE_ROOM_SUCCESS: {
        const updatedRoom: Room = action.payload;
        let hotel = clonedState.hotels.find(hotel => hotel.id === updatedRoom.hotelId);
        let room = hotel.rooms.find(r => r.id === updatedRoom.id);
        Object.assign(room, updatedRoom);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.DELETE_ROOM_SUCCESS: {
        const deletedRoom: Room = action.payload;
        let hotel = clonedState.hotels.find(hotel => hotel.id === deletedRoom.hotelId);
        let index = hotel.rooms.findIndex(room => room.id === deletedRoom.id);
        hotel.rooms.splice(index, 1);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.CREATE_ALTERNATIVE_SUCCESS: {
        let alternative: RoomAlternative = action.payload;
        let hotel = clonedState.hotels.find(hotel => hotel.id === alternative.hotelId);
        let room = hotel.rooms.find(room => room.id === alternative.roomId);
        room.alternatives.push(alternative);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.UPDATE_ALTERNATIVE_SUCCESS: {
        let updatedAlternative: RoomAlternative = action.payload;
        let hotel = clonedState.hotels.find(hotel => hotel.id === updatedAlternative.hotelId);
        let room = hotel.rooms.find(room => room.id === updatedAlternative.roomId);
        let alternative = room.alternatives.find(alterative => alterative.id === updatedAlternative.id);
        Object.assign(alternative, updatedAlternative);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.DELETE_ALTERNATIVE_SUCCESS: {
        const deletedRoomAlternative: RoomAlternative = action.payload;
        let hotel = clonedState.hotels.find(hotel => hotel.id === deletedRoomAlternative.hotelId);
        let room = hotel.rooms.find(room => room.id === deletedRoomAlternative.roomId);
        let index = room.alternatives.findIndex(alternative => alternative.id === deletedRoomAlternative.id);
        room.alternatives.splice(index, 1);
        clonedState.progressing = false;
        return clonedState;
      }
      case HotelsActionType.SAVE_ROOM_RATES: {
        let room : Room = action.payload.room;
        let rates : RoomRate[] = action.payload.rates;
        let hotel = clonedState.hotels.find(h => h.id === room.hotelId);
        let hotelRoom = hotel.rooms.find(r => r.id === room.id);
        rates.forEach(rate => {
          let roomRate = hotelRoom.findRate(rate.date);
          if(roomRate) {
            Object.assign(roomRate, rate);
          }
          else {
            hotelRoom.rates.push(rate);
          }
        });
        return clonedState;
      }
      default:
        return state;
    }
  };

const getHotelsState = (state: AppState): HotelsState => _.cloneDeep(state.hotels);

export const getHotels = (state: AppState): Hotel[] => {
  return getHotelsState(state).hotels;
};

export const getHotelById = (state: AppState, id: number): Hotel => {
  return getHotelsState(state).hotels.find(hotel => hotel.id === id);
};

export const isProgressing = (state: AppState): boolean => {
  return getHotelsState(state).progressing;
};
