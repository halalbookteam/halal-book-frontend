/* tslint:disable:typedef */

import {combineReducers, Reducer} from 'redux';
import {SearchReducer, SearchState} from './search.reducer';
import {UserReducer, UserState} from './user.reducer';
import {HotelsReducer, HotelsState} from './hotels.reducer';
import {AdminDataReducer, AdminDataState} from './admin-data.reducer';
import {ReservationReducer, ReservationState} from './reservation.reducer';
import {LanguageReducer, LanguageState} from './language.reducer';

export interface AppState {
  search       : SearchState;
  user         : UserState;
  hotels       : HotelsState;
  adminData    : AdminDataState;
  reservation  : ReservationState;
  language     : LanguageState;
}

const rootReducer: Reducer<AppState> = combineReducers<AppState>({
  search      : SearchReducer,
  user        : UserReducer,
  hotels      : HotelsReducer,
  adminData   : AdminDataReducer,
  reservation : ReservationReducer,
  language    : LanguageReducer,
});

export default rootReducer;
