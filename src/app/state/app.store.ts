///<reference path="../../../node_modules/redux/index.d.ts"/>
import { InjectionToken } from '@angular/core';
import {
  createStore,
  Store,
  compose,
  StoreEnhancer, Action
} from 'redux';

import {
  AppState,
  default as reducer
} from './reducers/app.reducer';
import {ReduxAction} from './app.action';

export const AppStore = new InjectionToken('App.store');

const devtools: StoreEnhancer<AppState> =
  window['devToolsExtension'] ?
  window['devToolsExtension']() : f => f;

export function createAppStore(): Store<AppState> {
  return createStore<AppState, ReduxAction, any, any>(
    reducer,
    compose(devtools)
  );
}

export const appStoreProviders = [
   { provide: AppStore, useFactory: createAppStore }
];
