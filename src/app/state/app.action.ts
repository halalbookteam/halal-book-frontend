import {Action} from 'redux';

export enum SearchActionType {
  SEARCH              = '[Search] Search',
  LOAD_FILTER_GROUPS  = '[Search] Load Filter Groups',
  FILTER_CHANGED      = '[Search] Filter Changed',
  LOAD_PLACES         = '[Search] Load Places',
  FETCH_HOTEL_REVIEWS = '[Search] Fetch Hotel Reviews',
}

export enum HotelsActionType {
  FETCH_HOTELS               = '[Hotels] Fetch Success',
  CREATE_HOTEL               = '[Hotels] Create Hotel',
  CREATE_HOTEL_SUCCESS       = '[Hotels] Create Hotel Success',
  UPDATE_HOTEL               = '[Hotels] Update Hotel',
  UPDATE_HOTEL_SUCCESS       = '[Hotels] Update Hotel Success',
  DELETE_HOTEL               = '[Hotels] Delete Hotel',
  DELETE_HOTEL_SUCCESS       = '[Hotels] Delete Hotel Success',
  CREATE_ROOM                = '[Hotels] Create Room',
  CREATE_ROOM_SUCCESS        = '[Hotels] Create Room Success',
  UPDATE_ROOM                = '[Hotels] Update Room',
  UPDATE_ROOM_SUCCESS        = '[Hotels] Update Room Success',
  DELETE_ROOM                = '[Hotels] Delete Room',
  DELETE_ROOM_SUCCESS        = '[Hotels] Delete Room Success',
  CREATE_ALTERNATIVE         = '[Hotels] Create Alternative',
  CREATE_ALTERNATIVE_SUCCESS = '[Hotels] Create Alternative Success',
  UPDATE_ALTERNATIVE         = '[Hotels] Update Alternative',
  UPDATE_ALTERNATIVE_SUCCESS = '[Hotels] Update Alternative Success',
  DELETE_ALTERNATIVE         = '[Hotels] Delete Alternative',
  DELETE_ALTERNATIVE_SUCCESS = '[Hotels] Delete Alternative Success',
  SAVE_ROOM_RATES            = '[Hotels] Save Room Rates',
}

export enum UserActionType {
  LOGIN               = '[User] Login',
  LOGOUT              = '[User] Logout',
  FETCH_USERS         = '[User] Fetch Users',
  CREATE_USER         = '[User] Create User',
  UPDATE_USER         = '[User] Update User',
  DELETE_USER         = '[User] Delete User',
  UPDATE_PROFILE      = '[User] Update Profile',
}

export enum LanguageActionType {
  SET_LANGUAGE                    = '[Language] Set Locale',
  SET_CURRENCY                    = '[Language] Set Currency',
  FETCH_CURRENCIES_AND_LANGUAGES  = '[Language] Fetch Currencies & Languages',
  SET_EXCHANGE                    = '[Language] Set Exchange',
}

export enum SystemActionType {
  INITIALIZE               = '[Common] Initialize',
}

export enum AdminActionType {
  FETCH_CATEGORIES       = '[AdminData] Fetch Facility Categories',
  CREATE_FACILITY        = '[AdminData] Create Facility',
  UPDATE_FACILITY        = '[AdminData] Update Facility',
  DELETE_FACILITY        = '[AdminData] Delete Facility',
  CREATE_CATEGORY        = '[AdminData] Create Category',
  UPDATE_CATEGORY        = '[AdminData] Update Category',
  DELETE_CATEGORY        = '[AdminData] Delete Category',
  FETCH_TEMPLATES        = '[AdminData] Fetch Mail Templates',
}

export enum ReservationActionType {
  BOOK                = '[Reservation] Book',
  CREATE_RESERVATION  = '[Reservation] Create',
  UPDATE_RESERVATION  = '[Reservation] Update Reservation',
  SAVE_PAYMENT_DETAIL  = '[Reservation] Save Payment Detail',
  FETCH_RESERVATIONS  = '[Reservation] Fetch Reservations',
  FETCH_MY_RESERVATIONS  = '[Reservation] Fetch My Reservations',
  CHANGE_RESERVATION  = '[Reservation] Change',
  SAVE_REVIEW  = '[Reservation] Save Review',
  REVIEW_REVIEW  = '[Reservation] Review Review',
  FETCH_REVIEWS  = '[Reservation] Fetch Reviews',
}

export type ActionType = SearchActionType | UserActionType | HotelsActionType | AdminActionType | ReservationActionType | SystemActionType |
  LanguageActionType;

export interface ReduxAction<T = ActionType> extends Action {
  type: T;
  payload: any;
}

export const createAction = (type: ActionType, payload) => ({
  type: type,
  payload: payload
});
