import {Component, Inject, OnInit} from '@angular/core';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as userReducer from '../../state/reducers/user.reducer';
import * as adminDataReducer from '../../state/reducers/admin-data.reducer';
import {ActivatedRoute, Router} from '@angular/router';
import {Role, User} from '../../model/user';
import {UserService} from '../../service/user.service';
import {MessageService, MessageType} from '../../service/message.service';
import {FormControl} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatAutocompleteSelectedEvent, MatDialog} from '@angular/material';
import {map, startWith} from 'rxjs/operators';
import {createAction, UserActionType} from '../../state/app.action';
import {ConfirmationDialogComponent, DialogOption} from '../../app-common/confirmation-dialog/confirmation-dialog.component';
import {Unsubscribe} from 'redux';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: User;
  emailControl = new FormControl('');
  separatorKeysCodes: number[] = [ENTER, COMMA];
  roleCtrl = new FormControl();
  private allRoles: Role[];
  private unsubscribe: Unsubscribe;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private userService: UserService,
    private messageService: MessageService,
    private dialog: MatDialog,
  ) {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => this.updateState());
  }

  private updateState() {
    let state = this.store.getState();
    let userId = this.getId();
    if(userId) {
      this.user = userReducer.findUser(state, userId);
      if(!this.user) {
        this.messageService.showTransMessage("message.notFound", MessageType.ERROR);
        this.router.navigateByUrl("/users");
      }
      else {
        this.emailControl.setValue(this.user.email);
      }
    }
    else {
      this.user = new User();
    }
    this.allRoles = adminDataReducer.getAllRoles(state);
  }

  private getId() {
    return +this.route.snapshot.params['userId'];
  }

  ngOnInit() {
  }

  save(valid: boolean) {
    if(valid && this.emailControl.valid) {
      this.user.email = this.emailControl.value;
      this.userService.saveUser(this.user).subscribe((user: User) => {
        if (!this.getId()) {
          this.router.navigate(['/users/' + user.id]).then(() => {
            this.store.dispatch(createAction(UserActionType.CREATE_USER, user));
          });
        }
        this.messageService.showTransMessage('message.saved', MessageType.SUCCESS);
      });
    }
  }

  roleSelected(event: MatAutocompleteSelectedEvent): void {
    let roleName = event.option.viewValue;
    this.user.addRole(this.findRoleByName(roleName));
    this.roleCtrl.setValue(null);
  }

  private _filterRoles(value: string): Role[] {
    const filterValue = value.toLowerCase();
    return this.remainingRoles().filter(role =>
      role.name.toLowerCase().indexOf(filterValue) === 0 &&
      !this.user.roles.some(r => r.id === role.id)
    );
  }

  private findRoleByName(roleName: string) {
    return this.allRoles.find(role => role.name === roleName);
  }

  get filteredRoles() {
    return this.roleCtrl.valueChanges.pipe(
      startWith(null),
      map((roleName: string | null) => roleName ? this._filterRoles(roleName) : this.remainingRoles())
    );
  }

  private remainingRoles() {
    return this.allRoles.filter(role => !this.user.hasRole(role));
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent).afterClosed().subscribe((result: DialogOption) => {
      if(result === DialogOption.YES) {
        let email = this.user.email;
        this.userService.deleteUser(this.user).subscribe(() => {
          this.unsubscribe();
          this.store.dispatch(createAction(UserActionType.DELETE_USER, this.user.id));
          this.router.navigateByUrl("/users");
          this.messageService.showTransMessage("message.deleted", MessageType.SUCCESS);
        });
      }
    });
  }
}
