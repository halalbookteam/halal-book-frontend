import {AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {User} from '../../model/user';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as userReducer from '../../state/reducers/user.reducer';
import {Unsubscribe} from 'redux';

@Component({
  template: '<router-outlet></router-outlet>',
})
export class UserListParentComponent {
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['position', 'name', 'surname', 'email', 'roles'];
  dataSource: MatTableDataSource<User>;
  @ViewChild(MatSort) sort: MatSort;
  private unsubscribe: Unsubscribe;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => {
      this.updateState();
    });
  }

  private updateState() {
    let state = this.store.getState();
    this.dataSource = new MatTableDataSource<User>(userReducer.getUsers(state));
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
