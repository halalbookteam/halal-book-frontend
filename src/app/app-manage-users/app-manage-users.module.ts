import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserListComponent, UserListParentComponent} from './user-list/user-list.component';
import {UserComponent} from './user/user.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppCommonModule} from '../app-common/app-common.module';
import {NgxMaskModule} from 'ngx-mask';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModule.forRoot(),
    RouterModule,

    AppCommonModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [
    UserListParentComponent,
    UserListComponent,
    UserComponent,
  ]
})
export class AppManageUsersModule {
}
