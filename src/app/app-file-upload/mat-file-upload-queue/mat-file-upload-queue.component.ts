import {Component, EventEmitter, Input, Output} from '@angular/core';
import {UploadService} from '../../service/upload.service';


export class UploadFile {
  file: any;
  uuid: string;
}

/**
 * A material design file upload queue component.
 */
@Component({
  selector: 'mat-file-upload-queue',
  templateUrl: './mat-file-upload-queue.component.html',
  styleUrls: ['./mat-file-upload-queue.component.scss'],
})
export class MatFileUploadQueue {
  constructor(private uploadService: UploadService) {

  }

  files: UploadFile[] = [];

  /* Http request input bindings */

  @Input()
  fileAlias: string = 'file';

  @Input()
  maxFileCount: number;

  @Input()
  fileTypes: string[] = [];

  private _id: string = this.uploadService.newGuid();

  @Output() onUpload = new EventEmitter();

  get id(): string {
    return this._id;
  }

  add(file: any) {
    if (this.maxFileCount && Object.keys(this.files).filter(value => value != null).length === this.maxFileCount) {
      console.error('Max File Count: ' + this.maxFileCount);
      return;
    }
    let fileExtension = this.getExtension(file.name);
    if (!this.isSuitableExtension(fileExtension)) {
      console.error('File extension (' + fileExtension + ') is not one of them: ' + this.fileTypes);
      return;
    }
    this.files.push({
      file: file,
      uuid: this.uploadService.newGuid()
    });
  }

  private isSuitableExtension(fileExtension: any) {
    return !this.fileTypes || this.fileTypes.length === 0 || this.fileTypes.some(type => type === fileExtension);
  }

  private getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
  }

  fileUploaded(uuid: string) {
    this.onUpload.emit(uuid);
  }

  fileRemoved(uploadFile: UploadFile) {
    var index = this.files.findIndex(file => file.uuid === uploadFile.uuid);
    this.files.splice(index, 1);
  }

  isFull() {
    return this.files.length >= this.maxFileCount;
  }
}
