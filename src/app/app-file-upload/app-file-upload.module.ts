import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FileUploadInputForDirective} from './directive/file-upload-input-for.directive';
import {MatFileUploadQueue} from './mat-file-upload-queue/mat-file-upload-queue.component';
import {MatFileUpload} from './mat-file-upload/mat-file-upload.component';
import {MatButtonModule, MatCardModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule} from '@angular/material';
import {FormatBytesPipe} from './pipe/format-bytes.pipe';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    MatButtonModule,
    MatProgressBarModule,
    MatIconModule,
    MatCardModule,
    HttpClientModule,
    CommonModule,
    MatProgressSpinnerModule,
    TranslateModule,
  ],
  declarations: [
    MatFileUpload,
    MatFileUploadQueue,
    FileUploadInputForDirective,
    FormatBytesPipe,
  ],
  exports: [
    MatFileUpload,
    MatFileUploadQueue,
    FileUploadInputForDirective,
    FormatBytesPipe,
  ]
})
export class AppFileUploadModule { }
