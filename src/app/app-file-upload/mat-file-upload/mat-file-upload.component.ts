import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HttpClient, HttpEventType} from '@angular/common/http';
import {UploadFile} from '../mat-file-upload-queue/mat-file-upload-queue.component';
import {AppErrorHandler} from '../../handler/app-error-handler';
import {UploadService} from '../../service/upload.service';


/**
 * A material design file upload component.
 */
@Component({
  selector: 'mat-file-upload',
  templateUrl: './mat-file-upload.component.html',
  styleUrls: ['./mat-file-upload.component.scss'],
})
export class MatFileUpload implements OnInit {

  isUploading: boolean = false;

  /* Http request input bindings */
  @Input()
  fileAlias: string = 'file';

  @Input()
  get file(): any {
    return this._file;
  }

  set file(file: any) {
    this._file = file;
    this.total = this._file.file.size;
  }

  /** Output  */
  @Output() removeEvent = new EventEmitter<UploadFile>();
  @Output() onUpload = new EventEmitter();

  progressPercentage: number = 0;
  public loaded: number = 0;
  total: number = 0;
  private _file: UploadFile;
  private fileUploadSubscription: any;

  constructor(
    private http: HttpClient,
    private errorHandler: AppErrorHandler,
    private uploadService: UploadService,
  ) {
  }

  ngOnInit(): void {
    if (this._file) {
      this.upload();
    }
  }

  public upload(): void {
    this.isUploading = true;
    // How to set the alias?
    let formData = new FormData();
    formData.set(this.fileAlias, this._file.file, this._file.uuid);
    this.fileUploadSubscription = this.uploadService.upload(formData).pipe(this.errorHandler.catchException()).subscribe((event: any) => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progressPercentage = Math.floor(event.loaded * 100 / event.total);
        this.loaded = event.loaded;
        this.total = event.total;
      }
      else if (event.type === HttpEventType.DownloadProgress) {
        this.isUploading = false;
        this.onUpload.emit(this._file.uuid);
        this.remove();
      }
    }, (error: any) => {
      if (this.fileUploadSubscription) {
        this.fileUploadSubscription.unsubscribe();
      }
      this.isUploading = false;
    });
  }

  public remove(): void {
    if (this.fileUploadSubscription) {
      this.fileUploadSubscription.unsubscribe();
    }
    this.removeEvent.emit(this._file);
  }

  getImageUrl() {
    return UploadService.UPLOAD_SERVICE_URL + this._file.uuid;
  }
}
