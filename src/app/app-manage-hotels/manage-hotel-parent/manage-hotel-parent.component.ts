import { Component, OnInit } from '@angular/core';
import {Language} from '../../model/language';
import {LanguageService} from '../../service/language.service';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {MessageService, MessageType} from '../../service/message.service';
import {Hotel} from '../../model/hotel';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-manage-hotel-parent',
  templateUrl: './manage-hotel-parent.component.html',
  styleUrls: ['./manage-hotel-parent.component.css']
})
export class ManageHotelParentComponent implements OnInit {

  languages: Language[];

  constructor(
    private dataService: ManageHotelsDataService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
  ) {
    this.languages = this.languageService.getLanguages();
  }

  ngOnInit() {
    const hotelId = this.getId();
    if(!this.dataService.loadHotelById(hotelId)) {
      this.messageService.showTransMessage("message.notFound", MessageType.ERROR);
      this.router.navigateByUrl("/manage/hotels");
    }
    this.dataService.languageId = this.hotel.defaultLanguageId;
  }

  private getId(): number {
    return +this.route.snapshot.paramMap.get('hotelId');
  }

  get hotel(): Hotel {
    return this.dataService.hotel;
  }

  get languageId() {
    return this.dataService.languageId;
  }

  set languageId(id) {
    this.dataService.languageId = id;
  }
}
