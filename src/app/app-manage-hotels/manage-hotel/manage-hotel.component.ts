import {Component, OnInit, ViewChild} from '@angular/core';
import {ChannelManager, Hotel} from '../../model/hotel';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {MatFileUploadQueue} from '../../app-file-upload/mat-file-upload-queue/mat-file-upload-queue.component';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {FacilityCategory} from '../../model/facility-category';
import {Photo} from '../../model/photo';
import {MatDialog, MatStepper} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {MessageService, MessageType} from '../../service/message.service';
import {ConfirmationDialogComponent, DialogOption} from '../../app-common/confirmation-dialog/confirmation-dialog.component';
import {LanguageService} from '../../service/language.service';
import {Currency, Language} from '../../model/language';

@Component({
  selector: 'app-manage-hotel',
  templateUrl: './manage-hotel.component.html',
  styleUrls: ['./manage-hotel.component.scss'],
  providers: [NgbRatingConfig] // add NgbRatingConfig to the component providers

})
export class ManageHotelComponent implements OnInit {
  @ViewChild('stepper') private stepper: MatStepper;
  @ViewChild('fileUploadQueue')
  private queue: MatFileUploadQueue;

  latControl = new FormControl('', [Validators.min(-90), Validators.max(90)]);
  lonControl = new FormControl('', [Validators.min(-180), Validators.max(180)]);

  languages: Language[];
  currencies: Currency[];

  constructor(
    private dataService: ManageHotelsDataService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private dialog: MatDialog,
    private languageService: LanguageService,
  ) {
    this.languages = this.languageService.getLanguages();
    this.currencies = this.languageService.getCurrencies();
  }

  get hotel(): Hotel {
    return this.dataService.hotel;
  }

  get categories(): FacilityCategory[] {
    return this.dataService.hotelCategories;
  }

  get languageId() {
    return this.dataService.languageId;
  }

  get hotelTranslation(): any {
    return this.dataService.hotel.getTranslation(this.dataService.languageId, true);
  }

  get addressTranslation(): any {
    return this.dataService.hotel.address.getTranslation(this.dataService.languageId, true);
  }

  get channelManagers() {
    return Object.keys(ChannelManager)
  }

  ngOnInit() {
    this.latControl.setValue(this.hotel.address.lat);
    this.lonControl.setValue(this.hotel.address.lon);
  }

  private getId(): number {
    return +this.route.snapshot.paramMap.get('hotelId');
  }

  saveHotel() {
    this.dataService.saveHotel().subscribe((hotel: Hotel) => {
      this.stepper.selectedIndex++;
      if (!this.getId()) {
        this.router.navigate(['/manage/hotels/' + hotel.id]);
      }
      this.messageService.showTransMessage('message.saved', MessageType.SUCCESS);
    });
  }

  public fileUploaded(fileName: string) {
    this.hotel.photos.push(new Photo("api/store/" + fileName));
  }

  removePhoto(index: number) {
    this.hotel.photos.splice(index, 1);
  }

  saveBasic() {
    this.stepper.selected.completed = this.isBasicFormValid();
    if (this.stepper.selected.completed) {
      this.saveHotel();
    }
  }

  isBasicFormValid() {
    return this.hotelTranslation && !!this.hotelTranslation.name && !!this.hotel.currency;
  }

  saveAddress() {
    this.stepper.selected.completed = this.isAddressFormValid();
    if (this.stepper.selected.completed) {
      this.hotel.address.lat = this.latControl.value;
      this.hotel.address.lon = this.lonControl.value;
      this.saveHotel();
      this.stepper.linear = false;
    }
  }

  isAddressFormValid() {
    return this.latControl.valid && this.lonControl.valid && !!this.addressTranslation.city && !!this.addressTranslation.country;
  }

  selectPrimaryPhoto(index: number) {
    this.hotel.primaryPhotoIndex = index;
    this.messageService.showTransMessage("message.primaryPhotoSelected", MessageType.SUCCESS);
  }

  deleteHotel() {
    this.dialog.open(ConfirmationDialogComponent).afterClosed().subscribe((result: DialogOption) => {
      if(result === DialogOption.YES) {
        this.dataService.deleteHotel(this.hotel).subscribe(() => {
          this.messageService.showTransMessage("message.deleted", MessageType.SUCCESS);
          this.router.navigateByUrl("/manage/hotels");
        });
      }
    });
  }
}
