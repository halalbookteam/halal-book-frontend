import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppCommonModule} from '../app-common/app-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ManageHotelsComponent, ManageHotelsParentComponent} from './manage-hotels/manage-hotels.component';
import {AppHotelListModule} from '../app-hotel-list/app-hotel-list.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {ManageRoomComponent} from './manage-room/manage-room.component';
import {RoomAlternativeComponent} from './room-alternative/room-alternative.component';
import {ManageRoomsComponent} from './manage-rooms/manage-rooms.component';
import {ManageHotelComponent} from './manage-hotel/manage-hotel.component';
import {RoomAlternativesComponent} from './room-alternatives/room-alternatives.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxMaskModule} from 'ngx-mask';
import {ManageHotelParentComponent} from './manage-hotel-parent/manage-hotel-parent.component';
import { RoomRatesComponent } from './room-rates/room-rates.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModule.forRoot(),
    NgxMaskModule.forRoot(),

    AppCommonModule,
    AppHotelListModule,
    AppRoutingModule,
  ],
  declarations: [
    ManageHotelsParentComponent,
    ManageHotelParentComponent,
    ManageHotelComponent,
    ManageHotelsComponent,
    ManageRoomComponent,
    RoomAlternativeComponent,
    ManageRoomsComponent,
    RoomAlternativesComponent,
    RoomRatesComponent,
  ]
})
export class AppManageHotelsModule {
}
