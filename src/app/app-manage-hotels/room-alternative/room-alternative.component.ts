import {Component, OnInit, ViewChild} from '@angular/core';
import {Room, RoomAlternative} from '../../model/room';
import {Hotel} from '../../model/hotel';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MealOptionType, MealType} from '../../model/room-alternative-options';
import {MessageService, MessageType} from '../../service/message.service';
import {MatDialog, MatStepper} from '@angular/material';
import {ConfirmationDialogComponent, DialogOption} from '../../app-common/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-room-alternative',
  templateUrl: './room-alternative.component.html',
  styleUrls: [
    '../manage-hotel/manage-hotel.component.scss',
    './room-alternative.component.scss',
  ]
})
export class RoomAlternativeComponent implements OnInit {
  @ViewChild('stepper') private stepper: MatStepper;
  MealOptionType = MealOptionType;
  MealType = MealType;
  optionalMealIncluded: boolean = true;

  freeCancellation: boolean;
  penaltyCancellation: boolean;
  meals: { [id: string]: boolean } = {};

  constructor(
    private dataService: ManageHotelsDataService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private dialog: MatDialog,
  ) {
  }

  get alternative(): RoomAlternative {
    return this.dataService.alternative;
  }

  get room(): Room {
    return this.dataService.room;
  }

  get hotel(): Hotel {
    return this.dataService.hotel;
  }

  ngOnInit() {
    if (!this.hotel) {
      this.dataService.loadHotelById(this.getHotelId());
    }
    if (!this.room) {
      this.dataService.loadRoomById(this.getRoomId());
    }
    this.dataService.loadAlternativeById(this.getAlternativeId());
    this.alternative.mealOption.meals.forEach(meal => {
      this.meals[meal] = true;
    });
    this.freeCancellation = !!this.alternative.paymentOption.freeCancellationDays;
    this.penaltyCancellation = !!this.alternative.paymentOption.penaltyCancellationDays;
    if(this.alternative.mealOption.price && this.alternative.mealOption.price.amount > 0) {
      this.optionalMealIncluded = false;
    }
  }

  private getAlternativeId(): number {
    return +this.route.snapshot.paramMap.get('alternativeId');
  }

  private getRoomId(): number {
    return +this.route.snapshot.paramMap.get('roomId');
  }

  private getHotelId(): number {
    return +this.route.snapshot.paramMap.get('hotelId');
  }

  isBasicFormValid(): boolean {
    return !!+this.alternative.maxAdult && +this.alternative.maxChild >= 0 && !!+this.alternative.basePriceRatio;
  }

  saveBasic() {
    this.stepper.selected.completed = this.isBasicFormValid();
    if(this.stepper.selected.completed) {
      this.saveAlternative();
    }
  }

  saveAlternative() {
    this.dataService.saveAlternative().subscribe(() => {
      this.router.navigate(['/manage/hotels/' + this.hotel.id + '/' + this.room.id + "/" + this.alternative.id]);
      this.messageService.showTransMessage('message.saved', MessageType.SUCCESS);
      this.stepper.next();
    });
  }

  freeCancellationChanged(checked: boolean) {
    this.alternative.paymentOption.freeCancellationDays = checked ? 1 : 0;
  }

  penaltyCancellationChanged(checked: boolean) {
    this.alternative.paymentOption.penaltyCancellationDays = checked ? 1 : 0;
    this.alternative.paymentOption.penaltyPercentage = checked ? 10 : 0;
  }

  mealsChanged() {
    this.alternative.mealOption.meals = Object.keys(this.meals)
      .filter(mealStr => this.meals[mealStr])
      .map(mealStr => MealType[mealStr]);

    if(this.optionalMealIncluded) {
      this.alternative.mealOption.price.amount = 0;
    }
  }

  deleteAlternative() {
    this.dialog.open(ConfirmationDialogComponent).afterClosed().subscribe((result: DialogOption) => {
      if(result === DialogOption.YES) {
        this.dataService.deleteAlternative(this.alternative).subscribe(() => {
          this.messageService.showTransMessage('message.deleted', MessageType.SUCCESS);
          this.router.navigateByUrl("/manage/hotels/" + this.hotel.id + "/" + this.room.id);
        });
      }
    });
  }

  priceChanged(event) {
    console.log(event);
  }
}
