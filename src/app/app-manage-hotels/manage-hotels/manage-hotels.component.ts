import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Hotel} from '../../model/hotel';
import {MatSort, MatTableDataSource} from '@angular/material';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {FacilityCategory} from '../../model/facility-category';
import {MessageService} from '../../service/message.service';
import {AuthenticationService} from '../../service/authentication.service';
import {Permission, User} from '../../model/user';

@Component({
  template: '<router-outlet></router-outlet>',
})
export class ManageHotelsParentComponent {
}

@Component({
  selector: 'app-manage-hotels',
  templateUrl: './manage-hotels.component.html',
})
export class ManageHotelsComponent implements OnInit, AfterViewInit {
  displayedColumns = ['position', 'name', 'star'];
  dataSource: MatTableDataSource<Hotel>;
  @ViewChild(MatSort) sort: MatSort;
  private user: User;

  constructor(
    private dataService: ManageHotelsDataService,
    private messageService: MessageService,
    private authenticationService: AuthenticationService,
    ) {
    this.user = this.authenticationService.getCurrentUser();
  }

  hasCreateHotelPermission() {
    return this.user.hasPermission(Permission.ALL_HOTELS_CRUD);
  }

  get hotels(): Hotel[] {
    return this.dataService.hotels;
  }

  get categories(): FacilityCategory[] {
    return this.dataService.hotelCategories;
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Hotel>(this.hotels);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
