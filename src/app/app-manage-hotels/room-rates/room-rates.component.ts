import {Component, OnInit} from '@angular/core';
import {DateRange} from '../../app-common/datepicker-popup/datepicker-popup.component';
import * as moment from 'moment';
import {MatTableDataSource} from '@angular/material';
import {Room, RoomRate} from '../../model/room';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {SelectionModel} from '@angular/cdk/collections';
import {Price} from '../../model/price';
import {MessageService, MessageType} from '../../service/message.service';
import {Hotel} from '../../model/hotel';

@Component({
  selector: 'app-room-rates',
  templateUrl: './room-rates.component.html',
  styleUrls: ['./room-rates.component.css']
})
export class RoomRatesComponent implements OnInit {
  displayedColumns: string[] = ['select', 'date', 'availableRoomCount', 'minNightCount', 'price'];
  dataSource: MatTableDataSource<RoomRate>;
  selection = new SelectionModel<RoomRate>(true, []);
  fromDate: Date;
  toDate: Date;
  updateRate;
  private rates: RoomRate[];

  constructor(
    private dataService: ManageHotelsDataService,
    private messageService: MessageService,
  ) {
    this.clearForm();
  }

  ngOnInit() {
    this.fromDate = moment().startOf('day').toDate();
    this.toDate = moment(this.fromDate).add(10, 'days').toDate();
    this.loadTable();
  }

  get room(): Room {
    return this.dataService.room;
  }

  get hotel(): Hotel {
    return this.dataService.hotel;
  }

  onDateSelected(dateRange: DateRange) {
    this.fromDate = dateRange.fromDate;
    this.toDate = dateRange.toDate;
  }

  show() {
    let currentDate = moment(this.fromDate);
    let endDate = moment(this.toDate);
    if(endDate.diff(currentDate, 'days') > 50) {
      //TODO multi-language
      alert("max 50 days");
    }
    this.loadTable();
  }

  private loadTable() {
    this.selection.clear();
    let currentDate = moment(this.fromDate);
    let endDate = moment(this.toDate);

    this.rates = [];
    while(currentDate.isSameOrBefore(endDate)) {
      let date = currentDate.toDate();
      let rate = this.room.findRate(date);
      if(!rate) {
        rate = new RoomRate();
        rate.date = date;
      }
      this.rates.push(rate);
      currentDate = currentDate.add(1, 'day');
    }

    this.dataSource = new MatTableDataSource<RoomRate>(this.rates);
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  update() {
    const numSelected = this.selection.selected.length;
    if(numSelected === 0) {
      //TODO multi-language
      alert("Nothing selected");
      return;
    }
    if(!this.updateRate.basePrice.amount && !(this.updateRate.availableRoomCount >= 0) && !(this.updateRate.minNightCount >= 0)) {
      //TODO multi-language
      alert("Change something");
      return;
    }
    let newPrice;
    if(this.updateRate.basePrice > 0) {
      newPrice = Object.assign(new Price(), this.updateRate.basePrice);
    }
    this.selection.selected.forEach(rate => {
      if(this.updateRate.basePrice > 0) {
        rate.basePrice = newPrice;
      }
      if(this.updateRate.availableRoomCount >= 0) {
        rate.availableRoomCount = this.updateRate.availableRoomCount;
      }
      if(this.updateRate.minNightCount >= 0) {
        rate.minNightCount = this.updateRate.minNightCount;
      }
    });
    this.dataService.saveRoomRates(this.selection.selected).subscribe(() => {
      this.loadTable();
      this.messageService.showTransMessage("message.saved", MessageType.SUCCESS);
    });
  }

  clearForm() {
    this.updateRate = new RoomRate();
    this.updateRate.basePrice = new Price();
    this.updateRate.basePrice.amount = undefined;
  }

}
