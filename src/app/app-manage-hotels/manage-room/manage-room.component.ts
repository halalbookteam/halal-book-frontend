import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Room} from '../../model/room';
import {FacilityCategory} from '../../model/facility-category';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {Hotel} from '../../model/hotel';
import {Photo} from '../../model/photo';
import {MatDialog, MatStepper} from '@angular/material';
import {MessageService, MessageType} from '../../service/message.service';
import {ConfirmationDialogComponent, DialogOption} from '../../app-common/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-manage-room',
  templateUrl: './manage-room.component.html',
  styleUrls: [
    '../manage-hotel/manage-hotel.component.scss',
  ]
})
export class ManageRoomComponent implements OnInit {
  @ViewChild('stepper') private stepper: MatStepper;

  constructor(
    private dataService: ManageHotelsDataService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private dialog: MatDialog,
  ) {
  }

  get room(): Room {
    return this.dataService.room;
  }

  get roomTranslation(): any {
    return this.room.getTranslation(this.dataService.languageId, true);
  }

  get hotel(): Hotel {
    return this.dataService.hotel;
  }

  get categories(): FacilityCategory[] {
    return this.dataService.roomCategories;
  }

  get languageId() {
    return this.dataService.languageId;
  }

  ngOnInit() {
    if (!this.hotel) {
      this.dataService.loadHotelById(this.getHotelId());
    }
    this.dataService.loadRoomById(this.getRoomId());
  }

  private getRoomId(): number {
    return +this.route.snapshot.paramMap.get('roomId');
  }

  private getHotelId(): number {
    return +this.route.snapshot.paramMap.get('hotelId');
  }

  saveRoom() {
    this.dataService.saveRoom().subscribe((room: Room) => {
      if(!this.room.id) {
        this.router.navigate(['/manage/hotels/' + this.hotel.id + '/' + this.room.id]);
      }
      this.messageService.showTransMessage('message.saved', MessageType.SUCCESS);
      this.stepper.selectedIndex++;
    });
  }

  public photoUploaded(fileName: string) {
    this.room.photos.push(new Photo("api/store/" + fileName));
  }

  removePhoto(index: number) {
    this.room.photos.splice(index, 1);
  }

  saveBasic() {
    this.stepper.selected.completed = this.isBasicFormValid();
    if(this.stepper.selected.completed) {
      this.saveRoom();
    }
  }

  isBasicFormValid(): boolean {
    return !!this.roomTranslation.name && !!this.room.size;
  }


  selectPrimaryPhoto(index: number) {
    this.room.primaryPhotoIndex = index;
    this.messageService.showTransMessage("message.primaryPhotoSelected", MessageType.SUCCESS);
  }

  deleteRoom() {
    this.dialog.open(ConfirmationDialogComponent).afterClosed().subscribe((result: DialogOption) => {
      if(result === DialogOption.YES) {
        this.dataService.deleteRoom(this.room).subscribe(() => {
          this.messageService.showTransMessage("message.deleted", MessageType.SUCCESS);
          this.router.navigateByUrl("/manage/hotels/" + this.hotel.id);
        });
      }
    });
  }
}
