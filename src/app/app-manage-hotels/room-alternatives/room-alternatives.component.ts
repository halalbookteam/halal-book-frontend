import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Room, RoomAlternative} from '../../model/room';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-room-alternatives',
  templateUrl: './room-alternatives.component.html',
})
export class RoomAlternativesComponent implements OnInit {
  displayedColumns: string[] = ['position', 'meal', 'payment', 'personCount', 'basePriceRatio'];
  dataSource: MatTableDataSource<RoomAlternative>;

  constructor(
    private route: ActivatedRoute,
    private dataService: ManageHotelsDataService,
  ) {
  }

  get room(): Room {
    return this.dataService.room;
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<RoomAlternative>(this.room.alternatives);
  }

  hasAlternatives() {
    return this.room.alternatives && this.room.alternatives.length > 0;
  }
}
