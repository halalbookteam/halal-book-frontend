import {Injectable} from '@angular/core';
import {HotelService} from '../../service/hotel.service';
import {Hotel} from '../../model/hotel';
import {FacilityCategory} from '../../model/facility-category';
import {tap} from 'rxjs/operators';
import {Address} from '../../model/address';
import {Facility} from '../../model/facility';
import {Room, RoomAlternative, RoomRate} from '../../model/room';
import {MealOption, MealOptionType, MealType, PaymentOption} from '../../model/room-alternative-options';
import {Price} from '../../model/price';

@Injectable({
  providedIn: 'root'
})
export class ManageHotelsDataService {
  private _hotels: Hotel[];
  private _hotelCategories: FacilityCategory[];
  private _roomCategories: FacilityCategory[];
  private _hotel: Hotel;
  private _room: Room;
  private _alternative: RoomAlternative;
  private _languageId: number;

  constructor(private hotelService: HotelService) { }

  get hotels(): Hotel[] {
    return this._hotels;
  }

  set hotels(value: Hotel[]) {
    this._hotels = value;
  }

  get hotelCategories(): FacilityCategory[] {
    return this._hotelCategories;
  }

  set hotelCategories(value: FacilityCategory[]) {
    this._hotelCategories = value;
  }

  get roomCategories(): FacilityCategory[] {
    return this._roomCategories;
  }

  set roomCategories(value: FacilityCategory[]) {
    this._roomCategories = value;
  }

  get hotel(): Hotel {
    return this._hotel;
  }

  set hotel(value: Hotel) {
    this._hotel = value;
  }

  get room(): Room {
    return this._room;
  }

  set room(value: Room) {
    this._room = value;
  }

  get alternative(): RoomAlternative {
    return this._alternative;
  }

  set alternative(value: RoomAlternative) {
    this._alternative = value;
  }

  get languageId(): number {
    return this._languageId;
  }

  set languageId(value: number) {
    this._languageId = value;
  }

  deleteHotel(hotel: Hotel) {
    return this.hotelService.deleteHotel(hotel).pipe(tap(() => {
      let index = this._hotels.findIndex(h => h.id === hotel.id);
      this._hotels.splice(index, 1);
    }));
  }

  findHotelById(id: number) {
    return this._hotels.find(hotel => hotel.id === id);
  }

  loadHotelById(hotelId: number) {
    if(!hotelId) {
      this._hotel = new Hotel();
    }
    else {
      this._hotel = this.findHotelById(hotelId);
    }
    if(this._hotel) {
      if(!this._hotel.address) {
        this._hotel.address = new Address();
      }
      if(!this._hotel.rooms) {
        this._hotel.rooms = [];
      }
      if(!this._hotel.photos) {
        this._hotel.photos = [];
      }
      this._hotelCategories.forEach(category => category.facilities.forEach(facility => {
        facility.check = this._hotel.facilities && this._hotel.facilities.some(fac => fac.id === facility.id);
      }));
    }
    return this._hotel;
  }

  saveHotel() {
    this._hotel.facilities = this.hotelCategories.reduce((facilities: Facility[], category) => {
      facilities = facilities.concat(category.facilities.filter(facility => facility.check));
      return facilities;
    }, []);
    return this.hotelService.saveHotel(this._hotel).pipe(tap((hotel: Hotel) => {
      if (!this._hotel.id) {
        this._hotels.push(this._hotel);
      }
      Object.assign(this._hotel, hotel);
      this.loadHotelById(hotel.id);
    }));
  }

  deleteRoom(room: Room) {
    return this.hotelService.deleteRoom(room).pipe(tap(() => {
      let index = this._hotel.rooms.findIndex(r => r.id === room.id);
      this._hotel.rooms.splice(index, 1);
    }));
  }

  loadRoomById(roomId: number) {
    if(!roomId) {
      this._room = new Room(undefined);
      this._room.hotelId = this._hotel.id;
    }
    else {
      this._room = this.findRoomById(roomId);
    }
    if(!this._room.photos) {
      this._room.photos = [];
    }
    this._roomCategories.forEach(category => category.facilities.forEach(facility => {
      facility.check = this._room.facilities && this._room.facilities.some(fac => fac.id === facility.id);
    }));
    if(!this._room.alternatives) {
      this._room.alternatives = [];
    }
    if(!this._room.rates) {
      this._room.rates = [];
    }
  }

  private findRoomById(roomId: number) {
    return this._hotel.rooms.find(room => room.id === roomId);
  }

  saveRoom() {
    this._room.facilities = this._roomCategories.reduce((facilities: Facility[], category) => {
      facilities = facilities.concat(category.facilities.filter(facility => facility.check));
      return facilities;
    }, []);
    return this.hotelService.saveRoom(this.room).pipe(tap((room: Room) => {
      if (!this.room.id) {
        this._hotel.rooms.push(room);
      }
      Object.assign(this.room, room);
      this.loadRoomById(room.id);
    }));
  }

  deleteAlternative(alternative: RoomAlternative) {
    return this.hotelService.deleteAlternative(alternative).pipe(tap(() => {
      let index = this._room.alternatives.findIndex(a => a.id === alternative.id);
      this._room.alternatives.splice(index, 1);
    }));
  }

  loadAlternativeById(alternativeId: number) {
    if(!alternativeId) {
      this._alternative = new RoomAlternative();
      this._alternative.hotelId = this._hotel.id;
      this._alternative.roomId = this._room.id;
    }
    else {
      this._alternative = this.findAlternativeById(alternativeId);
    }
    if(!this._alternative.mealOption) {
      this._alternative.mealOption = new MealOption();
      this._alternative.mealOption.type = MealOptionType.NOTHING_INCLUDED;
    }
    if(!this._alternative.mealOption.price) {
      this._alternative.mealOption.price = new Price();
    }
    if(!this._alternative.mealOption.meals) {
      this._alternative.mealOption.meals = [MealType.BREAKFAST];
    }

    if(!this._alternative.paymentOption) {
      this._alternative.paymentOption = new PaymentOption();
      this._alternative.paymentOption.noPrepayment = false;
    }
    if(!this._alternative.paymentOption.penaltyPercentage) {
      this._alternative.paymentOption.penaltyPercentage = 0;
    }
  }

  private findAlternativeById(alternativeId: number) {
    return this._room.alternatives.find(a => a.id === alternativeId);
  }

  saveAlternative() {
    return this.hotelService.saveAlternative(this.alternative).pipe(tap((alternative: RoomAlternative) => {
      if (!this.alternative.id) {
        this._room.alternatives.push(alternative);
      }
      Object.assign(this.alternative, alternative);
      this.loadAlternativeById(alternative.id);
    }));
  }

  saveRoomRates(rates: RoomRate[]) {
    return this.hotelService.saveRoomRates(this._room, rates).pipe(tap((savedRates: RoomRate[]) => {
      savedRates.forEach(rate => {
        let roomRate = this._room.findRate(rate.date);
        if(roomRate) {
          Object.assign(roomRate, rate);
        }
        else {
          this._room.rates.push(rate);
        }
      });
    }));
  }
}
