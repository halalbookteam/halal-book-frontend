import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Room} from '../../model/room';
import {MatSort, MatTableDataSource} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {Hotel} from '../../model/hotel';
import {ManageHotelsDataService} from '../service/manage-hotels-data.service';

@Component({
  selector: 'app-manage-rooms',
  templateUrl: './manage-rooms.component.html',
})
export class ManageRoomsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['position', 'name', 'size'];
  dataSource: MatTableDataSource<Room>;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private dataService: ManageHotelsDataService,
  ) {
  }

  get hotel(): Hotel {
    return this.dataService.hotel;
  }

  get languageId() {
    return this.dataService.languageId;
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Room>(this.hotel.rooms);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  hasRooms() {
    return this.hotel.rooms && this.hotel.rooms.length > 0;
  }
}
