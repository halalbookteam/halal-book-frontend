import {AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Reservation, ReservationStatus} from '../../model/reservation';
import * as reservationReducer from '../../state/reducers/reservation.reducer';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {MatSort, MatTableDataSource} from '@angular/material';
import {ReservationService} from '../../service/reservation.service';
import {AuthenticationService} from '../../service/authentication.service';
import {Permission} from '../../model/user';
import {PricePipe} from '../../app-common/pipe/price.pipe';
import {LanguageService} from '../../service/language.service';

interface ReservationTableData {
  id;
  reservationNo;
  hotelName;
  hotelId;
  creatorType;
  creatorName;
  personName;
  checkIn;
  checkOut;
  nights;
  rooms;
  price;
  user;
  status;
}

@Component({
  template: '<router-outlet></router-outlet>',
})
export class ReservationsParentComponent {
}

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns = ['reservationNo', 'hotelName', 'personName', 'checkIn', 'checkOut', 'nights', 'rooms', 'price', 'status'];
  dataSource: MatTableDataSource<ReservationTableData>;
  @ViewChild(MatSort) sort: MatSort;
  ReservationStatus = ReservationStatus;
  statusFilter = ReservationStatus.CONFIRMED;
  private readonly EMPTY_FILTER = "EMPTY_FILTER";


  private reservations: Reservation[];
  private unsubscribe: Unsubscribe;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private reservationService: ReservationService,
    private authenticationService: AuthenticationService,
    private languageService: LanguageService,
    private pricePipe: PricePipe,
  ) {
  }

  ngOnInit() {
    this.unsubscribe = this.store.subscribe(() => this.updateState());
    this.updateState();
    if (!this.isCreator()) {
      this.displayedColumns.splice(2, 0, 'creatorName');
      this.displayedColumns.splice(2, 0, 'creatorType');
    }
  }

  private updateState() {
    let state = this.store.getState();
    this.reservations = reservationReducer.getReservations(state);
    this.dataSource = new MatTableDataSource<ReservationTableData>(this.reservations.map(res => {
      return {
        id: res.id,
        reservationNo: res.reservationNo,
        hotelId: res.hotel.id,
        hotelName: res.hotel.get('name', this.languageService.getLanguageId()),
        creatorType: res.creatorType,
        creatorName: res.creator,
        personName: res.person.toString(),
        checkIn: res.checkIn,
        checkOut: res.checkOut,
        nights: res.nightCount,
        rooms: res.roomReservations.length,
        price: this.pricePipe.transform(res.price, false),
        user: res.user,
        status: res.status,
      };
    }));
    this.dataSource.filterPredicate = (rowData, filterStr) => {
      if(this.statusFilter && rowData.status !== this.statusFilter) {
        return false;
      }
      return filterStr === this.EMPTY_FILTER || JSON.stringify(rowData).toLowerCase().includes(filterStr);
    };
    this.dataSource.filter = this.EMPTY_FILTER;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  isCreator() {
    let currentUser = this.authenticationService.getCurrentUser();
    return !currentUser.hasPermission(Permission.ALL_HOTELS_CRUD) && !currentUser.hasPermission(Permission.OWNER_HOTELS_CRUD);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue === "" ? this.EMPTY_FILTER : filterValue.toLowerCase();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  statusFilterChanged() {
    this.applyFilter(this.dataSource.filter === this.EMPTY_FILTER ? "" : this.dataSource.filter);
  }
}
