import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReservationsComponent, ReservationsParentComponent} from './reservations/reservations.component';
import {AppCommonModule} from '../app-common/app-common.module';
import {RouterModule} from '@angular/router';
import { ReservationComponent } from './reservation/reservation.component';
import {AppHotelListModule} from '../app-hotel-list/app-hotel-list.module';
import {AppHotelModule} from '../app-hotel/app-hotel.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppHotelListModule,
    AppHotelModule,
  ],
  declarations: [
    ReservationsComponent,
    ReservationComponent,
    ReservationsParentComponent,
  ]
})
export class AppManageReservationsModule { }
