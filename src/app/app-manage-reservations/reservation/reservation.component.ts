import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Reservation, ReservationStatus} from '../../model/reservation';
import * as reservationReducer from '../../state/reducers/reservation.reducer';
import {ActivatedRoute, Router} from '@angular/router';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {MessageService, MessageType} from '../../service/message.service';
import {ReservationService} from '../../service/reservation.service';
import {Permission, User} from '../../model/user';
import {AuthenticationService} from '../../service/authentication.service';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent, DialogOption} from '../../app-common/confirmation-dialog/confirmation-dialog.component';
import {createAction, ReservationActionType} from '../../state/app.action';
import {SearchService} from '../../service/search.service';
import {Hotel} from '../../model/hotel';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit, OnDestroy {
  reservation: Reservation;
  ReservationStatus = ReservationStatus;
  canCancel  : boolean;
  private currentUser: User;
  private unsubscribe: Unsubscribe;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private reservationService: ReservationService,
    private messageService: MessageService,
    private authenticationService: AuthenticationService,
    private dialog: MatDialog,
    private searchService: SearchService,
  ) {
    this.currentUser = this.authenticationService.getCurrentUser();
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => this.updateState());
  }

  private updateState() {
    let state = this.store.getState();
    let reservationId = this.getId();
    if(reservationId) {
      this.reservation = reservationReducer.findReservation(state, reservationId) || reservationReducer.findMyReservation(state, reservationId);
      this.canCancel = this.reservation.canCancelBy(this.currentUser);
      if(!this.reservation) {
        this.messageService.showTransMessage("message.notFound", MessageType.ERROR);
        this.router.navigateByUrl("/reservations");
      }
    }
  }

  private getId() {
    return +this.route.snapshot.params['reservationId'];
  }

  ngOnInit() {
  }

  isCreator() {
    return this.reservation.isCreator(this.currentUser);
  }

  cancel() {
    this.dialog.open(ConfirmationDialogComponent).afterClosed().subscribe((result: DialogOption) => {
      if(result === DialogOption.YES) {
        this.reservationService.cancel(this.reservation).subscribe(() => {
          this.messageService.showTransMessage("message.reservationCancelled", MessageType.SUCCESS);
        });
      }
    });
  }

  // change() {
  //   if(!this.reservation.hotel) {
  //     this.searchService.fetchHotel(this.reservation.hotelId).subscribe((hotel: Hotel) => {
  //       this.reservation.hotel = hotel;
  //       this.changeReservation();
  //     });
  //   }
  //   else {
  //     this.changeReservation();
  //   }
  // }
  //
  ngOnDestroy(): void {
    this.unsubscribe();
  }

  // private changeReservation() {
  //   this.reservation.load();
  //   this.store.dispatch(createAction(ReservationActionType.CHANGE_RESERVATION, this.reservation));
  //   this.router.navigateByUrl("/reservation");
  // }
}
