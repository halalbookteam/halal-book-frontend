import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppCommonModule} from './app-common/app-common.module';
import {AppHomeModule} from './app-home/app-home.module';
import {AppHotelModule} from './app-hotel/app-hotel.module';
import {AppHotelListModule} from './app-hotel-list/app-hotel-list.module';
import {appStoreProviders} from './state/app.store';
import {AppBookModule} from './app-book/app-book.module';
import {AppManageHotelsModule} from './app-manage-hotels/app-manage-hotels.module';
import {LocalStorageService, Ng2Webstorage, SessionStorageService} from 'ngx-webstorage';
import {AuthInterceptor} from './interceptor/auth.interceptor';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthServiceConfig, GoogleLoginProvider, SocialLoginModule} from 'angularx-social-login';
import {AppFileUploadModule} from './app-file-upload/app-file-upload.module';
import {AppFacilitiesModule} from './app-facilities/app-facilities.module';
import {HotelResolver} from './resolver/hotel.resolver';
import {AppProfileModule} from './app-profile/app-profile.module';
import {CategoriesResolver} from './resolver/categories.resolver';
import {HotelsResolver} from './resolver/hotels.resolver';
import {ReservationsResolver} from './resolver/reservations.resolver';
import {FilterGroupResolver} from './resolver/filter-group.resolver';
import {LoaderService} from './service/loader.service';
import {ToastaModule} from 'ngx-toasta';
import {AppErrorHandler} from './handler/app-error-handler';
import {UsersResolver} from './resolver/users.resolver';
import {AppManageReservationsModule} from './app-manage-reservations/app-manage-reservations.module';
import {AppManageUsersModule} from './app-manage-users/app-manage-users.module';
import {AppMailModule} from './app-mail/app-mail.module';
import {MailTemplatesResolver} from './resolver/mail-templates.resolver';
import {MyReservationsResolver} from './resolver/myReservations.resolver';
import {ReviewResolver} from './resolver/review-resolver.service';
import {AppManageReviewsModule} from './app-manage-reviews/app-manage-reviews.module';
import {ReviewsResolver} from './resolver/reviews-resolver.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {registerLocaleData} from '@angular/common';
import localeTr from '@angular/common/locales/tr';
import localeFr from '@angular/common/locales/fr';
import localeGb from '@angular/common/locales/en-GB';
import {LanguageService} from './service/language.service';
import {AppPaymentModule} from './app-payment/app-payment.module';

registerLocaleData(localeTr, 'tr');
registerLocaleData(localeFr, 'fr');
registerLocaleData(localeGb, 'en-gb');

export function provideConfig() {
  return new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(
      '491246943594-20egv3m6fuamkur8dg38i2blgciu7ikb.apps.googleusercontent.com')
  },
  // {
  //   id: FacebookLoginProvider.PROVIDER_ID,
  //   provider: new FacebookLoginProvider('243583799682449')
  // }
]);
}

export function appInit(loaderService: LoaderService) {
  return () => {
    return new Promise((resolve, reject) => {
      loaderService.initialize().subscribe(() => {
        resolve();
      });
    });
  };
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    AppCommonModule,
    AppHomeModule,
    AppHotelModule,
    AppHotelListModule,
    AppBookModule,
    AppManageHotelsModule,
    Ng2Webstorage.forRoot(),
    BrowserAnimationsModule,
    AppFileUploadModule,
    SocialLoginModule,
    AppFacilitiesModule,
    AppProfileModule,
    AppManageReservationsModule,
    AppManageUsersModule,
    AppMailModule,
    AppManageReviewsModule,
    ToastaModule.forRoot(),
    TranslateModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (httpClient) => new TranslateHttpLoader(httpClient),
        deps: [HttpClient]
      }
    }),
    AppPaymentModule
  ],
  providers: [appStoreProviders,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
      deps: [LocalStorageService, LanguageService]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [LoaderService]
    },
    {
      provide: ErrorHandler,
      useClass: AppErrorHandler,
    },

    HotelsResolver,
    CategoriesResolver,
    HotelResolver,
    ReservationsResolver,
    MyReservationsResolver,
    FilterGroupResolver,
    UsersResolver,
    MailTemplatesResolver,
    ReviewResolver,
    ReviewsResolver,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
