import {Inject, Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Hotel} from '../model/hotel';
import {HotelService} from '../service/hotel.service';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import * as hotelsReducer from '../state/reducers/hotels.reducer';
import {AppState} from '../state/reducers/app.reducer';
import {ManageHotelsDataService} from '../app-manage-hotels/service/manage-hotels-data.service';
import {tap} from 'rxjs/operators';

@Injectable()
export class HotelsResolver implements Resolve<Observable<Hotel[]>> {
  constructor(
    private hotelService: HotelService,
    private dataService: ManageHotelsDataService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve(): Observable<any> {
    let state = this.store.getState();
    let hotels = hotelsReducer.getHotels(state);
    if (!hotels) {
      return this.hotelService.fetchHotels().pipe(tap(list => this.dataService.hotels = list));
    }
    this.dataService.hotels = hotels;
    return of(hotels);
  }
}

