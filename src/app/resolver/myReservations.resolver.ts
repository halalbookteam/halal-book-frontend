import {Inject, Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import * as reservationReducer from '../state/reducers/reservation.reducer';
import {Reservation} from '../model/reservation';
import {ReservationService} from '../service/reservation.service';

@Injectable()
export class MyReservationsResolver implements Resolve<Observable<Reservation[]>> {
  constructor(
    private reservationService: ReservationService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve(): Observable<any> {
    let state = this.store.getState();
    let reservations = reservationReducer.getMyReservations(state);
    if(!reservations || !reservations.length) {
      return this.reservationService.fetchMyReservations();
    }
    return of(reservations);
  }
}
