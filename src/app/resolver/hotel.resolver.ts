import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {Hotel} from '../model/hotel';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {SearchService} from '../service/search.service';

@Injectable()
export class HotelResolver implements Resolve<Observable<Hotel>> {
  constructor(
    private searchService: SearchService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const hotelId = +route.paramMap.get('hotelId');
    return this.searchService.fetchHotel(hotelId);
  }
}

