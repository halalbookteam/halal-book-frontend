import {Inject, Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import * as adminDataReducer from '../state/reducers/admin-data.reducer';
import {MailTemplate} from '../model/mail';
import {MailService} from '../service/mail.service';

@Injectable()
export class MailTemplatesResolver implements Resolve<Observable<MailTemplate[]>> {
  constructor(
    private mailService: MailService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve(): Observable<any> {
    let state = this.store.getState();
    let templates = adminDataReducer.getMailTemplates(state);
    if(!templates || !templates.length) {
      return this.mailService.fetchTemplates();
    }
    return of(templates);
  }
}
