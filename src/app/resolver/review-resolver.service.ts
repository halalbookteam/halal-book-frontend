import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {ReservationService} from '../service/reservation.service';
import * as reservationReducer from '../state/reducers/reservation.reducer';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {Review} from '../model/review';

@Injectable()
export class ReviewResolver implements Resolve<Observable<Review>> {
  constructor(
    private reservationService: ReservationService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const reservationId = +route.paramMap.get('reservationId');
    let reservation = reservationReducer.findMyReservation(this.store.getState(), reservationId);
    if(reservation.review) {
      return of(reservation.review);
    }
    return this.reservationService.fetchReview(reservationId);
  }
}
