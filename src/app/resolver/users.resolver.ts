import {Inject, Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import * as userReducer from '../state/reducers/user.reducer';
import {User} from '../model/user';
import {UserService} from '../service/user.service';

@Injectable()
export class UsersResolver implements Resolve<Observable<User[]>> {
  constructor(
    private userService: UserService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve(): Observable<any> {
    let state = this.store.getState();
    let users = userReducer.getUsers(state);
    if(!users || !users.length) {
      return this.userService.fetchUsers();
    }
    return of(users);
  }
}
