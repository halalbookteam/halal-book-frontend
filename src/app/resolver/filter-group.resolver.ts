import {Inject, Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import * as searchReducer from '../state/reducers/search.reducer';
import {FilterGroup} from '../model/filter';
import {CategoriesResolver} from './categories.resolver';
import {map} from 'rxjs/operators';
import {SearchService} from '../service/search.service';

@Injectable()
export class FilterGroupResolver implements Resolve<Observable<FilterGroup[]>> {
  constructor(
    private searchService: SearchService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private categoryResolver: CategoriesResolver,
  ) {
  }

  resolve() {
    let state = this.store.getState();
    let filterGroups = searchReducer.getFilterGroups(state);
    if(filterGroups && filterGroups.length) {
      return of(filterGroups);
    }

    return this.categoryResolver.resolve().pipe(map(categories => {
      return this.searchService.loadFilterGroups(categories);
    }))
  }
}
