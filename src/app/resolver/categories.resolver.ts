import {Inject, Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {CategoryType, FacilityCategory} from '../model/facility-category';
import {HotelService} from '../service/hotel.service';
import {ManageHotelsDataService} from '../app-manage-hotels/service/manage-hotels-data.service';
import {FacilityService} from '../service/facility.service';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import * as adminDataReducer from '../state/reducers/admin-data.reducer';
import {tap} from 'rxjs/operators';

@Injectable()
export class CategoriesResolver implements Resolve<Observable<FacilityCategory[]>> {
  constructor(
    private hotelService: HotelService,
    private dataService: ManageHotelsDataService,
    private facilityService: FacilityService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  resolve() {
    let state = this.store.getState();
    let categories = adminDataReducer.getCategories(state);
    if (!categories) {
      return this.facilityService.fetchCategories().pipe(tap((list: FacilityCategory[]) => this.loadCategories(list)));
    }
    this.loadCategories(categories);
    return of(categories);
  }

  private loadCategories(categories: FacilityCategory[]) {
    this.dataService.hotelCategories = categories.filter(category => category.type === CategoryType.HOTEL);
    this.dataService.roomCategories = categories.filter(category => category.type === CategoryType.ROOM);
  }
}
