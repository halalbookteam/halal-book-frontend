import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {ReservationService} from '../service/reservation.service';
import {Review} from '../model/review';

@Injectable()
export class ReviewsResolver implements Resolve<Observable<Review[]>> {
  constructor(
    private reservationService: ReservationService,
  ) {
  }

  resolve(): Observable<any> {
    return this.reservationService.getPendingReviews();
  }
}
