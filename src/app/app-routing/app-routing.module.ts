import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HotelListComponent} from '../app-hotel-list/hotel-list/hotel-list.component';
import {HomeComponent} from '../app-home/home/home.component';
import {HotelComponent} from '../app-hotel/hotel/hotel.component';
import {BookComponent} from '../app-book/book/book.component';
import {ManageHotelsComponent, ManageHotelsParentComponent} from '../app-manage-hotels/manage-hotels/manage-hotels.component';
import {ManageHotelComponent} from '../app-manage-hotels/manage-hotel/manage-hotel.component';
import {ManageRoomComponent} from '../app-manage-hotels/manage-room/manage-room.component';
import {RoomAlternativeComponent} from '../app-manage-hotels/room-alternative/room-alternative.component';
import {HotelsResolver} from '../resolver/hotels.resolver';
import {CategoriesParentComponent, CategoryListComponent} from '../app-facilities/category-list/category-list.component';
import {HotelResolver} from '../resolver/hotel.resolver';
import {ProfileComponent} from '../app-profile/profile/profile.component';
import {CategoryComponent} from '../app-facilities/category/category.component';
import {ReservationsComponent, ReservationsParentComponent} from '../app-manage-reservations/reservations/reservations.component';
import {ReservationsResolver} from '../resolver/reservations.resolver';
import {CategoriesResolver} from '../resolver/categories.resolver';
import {FilterGroupResolver} from '../resolver/filter-group.resolver';
import {PageNotFoundComponent} from '../app-home/page-not-found/page-not-found.component';
import {UserListComponent, UserListParentComponent} from '../app-manage-users/user-list/user-list.component';
import {UsersResolver} from '../resolver/users.resolver';
import {UserComponent} from '../app-manage-users/user/user.component';
import {ReservationComponent} from '../app-manage-reservations/reservation/reservation.component';
import {ProfileParentComponent} from '../app-profile/profile-parent/profile-parent.component';
import {ChangePasswordComponent} from '../app-profile/change-password/change-password.component';
import {SendMailComponent} from '../app-mail/send-mail/send-mail.component';
import {MailTemplatesResolver} from '../resolver/mail-templates.resolver';
import {MyReservationsComponent} from '../app-profile/my-reservations/my-reservations.component';
import {MyReservationsResolver} from '../resolver/myReservations.resolver';
import {ReviewComponent} from '../app-profile/review/review.component';
import {ReviewResolver} from '../resolver/review-resolver.service';
import {ManageReviewsComponent} from '../app-manage-reviews/manage-reviews/manage-reviews.component';
import {ReviewsResolver} from '../resolver/reviews-resolver.service';
import {ManageHotelParentComponent} from '../app-manage-hotels/manage-hotel-parent/manage-hotel-parent.component';
import {ActivateAccountComponent} from '../app-common/activate-account/activate-account.component';
import {ForgotPasswordComponent} from '../app-profile/forgot-password/forgot-password.component';
import {RecoverPasswordComponent} from '../app-profile/recover-password/recover-password.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'hotels', component: HotelListComponent, resolve: {filterGroups: FilterGroupResolver}},
  {path: 'hotel/:hotelId', component: HotelComponent, resolve: {hotel: HotelResolver}},
  {path: 'reservation', component: BookComponent},
  {path: 'manage/hotels', component: ManageHotelsParentComponent, resolve: {hotels: HotelsResolver, categories: CategoriesResolver},
    children: [
      {path: '', component: ManageHotelsComponent},
      {path: ':hotelId', component: ManageHotelParentComponent, children: [
          {path: '', component: ManageHotelComponent},
          {path: ':roomId', component: ManageRoomComponent},
          {path: ':roomId/:alternativeId', component: RoomAlternativeComponent},
        ]},
    ]
  },
  {path: 'categories', component: CategoriesParentComponent, resolve: {categories: CategoriesResolver},
    children: [
      {path: '', component: CategoryListComponent, resolve: {categories: CategoriesResolver}},
      {path: ':categoryId', component: CategoryComponent},
    ]
  },
  {path: 'profile', component: ProfileParentComponent,
    children: [
      {path: '', redirectTo: '/profile/settings', pathMatch: 'full'},
      {path: 'settings', component: ProfileComponent},
      {path: 'changePassword', component: ChangePasswordComponent},
      {path: 'reservations', component: ReservationsParentComponent, resolve: {myReservations: MyReservationsResolver},
        children: [
          {path: '', component: MyReservationsComponent},
          {path: ':reservationId', component: ReservationComponent},
          {path: ':reservationId/review', component: ReviewComponent, resolve: {review: ReviewResolver}},
        ]},
    ]},
  {path: 'reservations', component: ReservationsParentComponent, resolve: {reservations: ReservationsResolver},
    children: [
      {path: '', component: ReservationsComponent},
      {path: ':reservationId', component: ReservationComponent},
    ]},
  {path: 'users', component: UserListParentComponent, resolve: {reservations: UsersResolver},
    children: [
      {path: '', component: UserListComponent},
      {path: ':userId', component: UserComponent},
    ]
  },
  {path: 'reviews', component: ManageReviewsComponent, resolve: {reviews: ReviewsResolver}},
  {path: 'mail', component: SendMailComponent, resolve: {templates: MailTemplatesResolver}},
  {path: 'activateAccount', component: ActivateAccountComponent},
  {path: 'forgotPassword', component: ForgotPasswordComponent},
  {path: 'recoverPassword', component: RecoverPasswordComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
