import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppErrorHandler} from '../handler/app-error-handler';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {tap} from 'rxjs/operators';
import {AdminActionType, createAction} from '../state/app.action';
import {environment} from '../../environments/environment';
import {MailData, MailTemplate} from '../model/mail';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  private readonly SERVICE_URL = environment.apiURL + 'api/mail/';

  constructor(
    private http: HttpClient,
    private errorHandler: AppErrorHandler,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) { }

  fetchTemplates() {
    return this.http.get<MailTemplate[]>(this.SERVICE_URL).pipe(
      tap((templates: MailTemplate[]) => {
        this.store.dispatch(createAction(AdminActionType.FETCH_TEMPLATES, templates));
      }),
      this.errorHandler.catchException()
    );
  }

  sendMail(mail: MailData) {
    return this.http.post(this.SERVICE_URL, mail).pipe(
      this.errorHandler.catchException()
    );
  }
}
