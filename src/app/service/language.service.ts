import {Inject, Injectable} from '@angular/core';
import {combineLatest, of} from 'rxjs';
import * as languageReducer from '../state/reducers/language.reducer';
import * as userReducer from '../state/reducers/user.reducer';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {createAction, LanguageActionType} from '../state/app.action';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {fromPromise} from 'rxjs/internal-compatibility';
import {environment} from '../../environments/environment';
import {Currency, Language} from '../model/language';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  private readonly SERVICE_URL = environment.apiURL + 'api/locale/';

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private http: HttpClient,
    private translateService: TranslateService,
  ) {
  }

  initialize() {
    return fromPromise(this.loadLanguages().toPromise().then(() => {
      let language = this.getLanguage();
      if (!language) {
        return this.determineLanguage();
      }
      else {
        this.changeLanguageAndCurrency(language.code);
        this.translateService.use(language.code);
      }
      return of(null);
    }));
  }

  getLanguage() {
    return languageReducer.getLanguage(this.store.getState());
  }

  getLanguageId() {
    let language = this.getLanguage();
    return language ? language.id : null;
  }

  getLanguageCode() {
    return languageReducer.getLanguageCode(this.store.getState());
  }

  getCurrency() {
    return languageReducer.getCurrency(this.store.getState());
  }

  private determineLanguage() {
    let currentUser = userReducer.getCurrentUser(this.store.getState());
    if(currentUser && currentUser.profile) {
      if(currentUser.profile.defaultCurrency) {
        this.changeCurrency(currentUser.profile.defaultCurrency.code);
      }
      if(currentUser.profile.defaultLanguage) {
        this.changeLanguage(currentUser.profile.defaultLanguage.code);
        return of(null).toPromise();
      }
    }
    return this.http.get('http://ip-api.com/json').pipe(tap((data: any) => {
      this.changeLanguageAndCurrency(data.countryCode);
    }, () => {
      this.changeLanguageAndCurrency(this.translateService.getBrowserLang());
    })).toPromise();
  }

  private changeLanguageAndCurrency(langCode: string) {
    let language = languageReducer.getLanguageByCode(this.store.getState(), langCode);
    this.changeLanguage(language.code);
    if (!this.getCurrency()) {
      this.changeCurrency(language.currency.code);
    }
  }

  private loadLanguages() {
    let languages = languageReducer.getLanguages(this.store.getState());
    if (languages) {
      this.translateService.addLangs(languages.map(lang => lang.code));
      this.translateService.setDefaultLang('en');
      return of(null);
    }
    return combineLatest(this.http.get(this.SERVICE_URL + "languages"),
      this.http.get(this.SERVICE_URL + "currencies")).pipe(
      tap(([languages, currencies]: [Language[], Currency[]]) => {
        this.translateService.addLangs(languages.map(lang => lang.code));
        this.translateService.setDefaultLang('en');
        this.store.dispatch(createAction(LanguageActionType.FETCH_CURRENCIES_AND_LANGUAGES, {
          languages: languages,
          currencies: currencies
        }));
      }));
  }

  getLanguages() {
    return languageReducer.getLanguages(this.store.getState());
  }

  getCurrencies() {
    return languageReducer.getCurrencies(this.store.getState());
  }

  changeLanguage(langCode: string) {
    this.translateService.use(langCode);
    this.store.dispatch(createAction(LanguageActionType.SET_LANGUAGE, langCode.toLowerCase()));
  }

  changeCurrency(curCode: string) {
    this.store.dispatch(createAction(LanguageActionType.SET_CURRENCY, curCode.toUpperCase()));
  }

  getCurrencyCode() {
    return languageReducer.getCurrencyCode(this.store.getState());
  }
}
