import {Inject, Injectable} from '@angular/core';
import {LanguageService} from './language.service';
import * as languageReducer from '../state/reducers/language.reducer';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {map, share, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Price} from '../model/price';
import {createAction, LanguageActionType} from '../state/app.action';
import {CurrencyExchange} from '../model/currencyExchange';
import {fromPromise} from 'rxjs/internal-compatibility';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {
  private observable: Observable<any>;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private languageService: LanguageService,
    private http: HttpClient,
  ) {
  }


  exchange(price: Price) {
    let toCurrency = this.languageService.getCurrencyCode();
    if (price.currency === toCurrency) {
      return of(price);
    }
    let exchangeId = price.currency + '_' + toCurrency;
    let currencyExchange = languageReducer.getExchange(this.store.getState(), exchangeId);
    let generatePrice = ratio => {
      let newPrice = price.multiply(ratio);
      newPrice.currency = toCurrency;
      return newPrice;
    };

    if (currencyExchange && !currencyExchange.isExpired()) {
      return of(currencyExchange.ratio).pipe(map(generatePrice));
    }
    else if(!this.observable) {
      this.observable = fromPromise(this.fetchExchange(exchangeId).pipe(map(generatePrice)).toPromise());
    }
    return this.observable;
  }

  private fetchExchange(exchangeId: string) {
    return this.http.get('https://free.currencyconverterapi.com/api/v6/convert?q=' + exchangeId + '&compact=ultra').pipe(
      map(data => data[exchangeId]),
      tap(ratio => {
        this.observable = null;
        this.store.dispatch(createAction(LanguageActionType.SET_EXCHANGE, new CurrencyExchange(exchangeId, ratio)));
      })
    );
  }
}
