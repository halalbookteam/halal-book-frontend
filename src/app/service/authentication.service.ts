import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LocalStorageService} from 'ngx-webstorage';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import * as moment from 'moment';
import {AppState} from '../state/reducers/app.reducer';
import {createAction, SystemActionType, UserActionType} from '../state/app.action';
import * as userReducer from '../state/reducers/user.reducer';
import {User} from '../model/user';
import {AppErrorHandler} from '../handler/app-error-handler';
import {of} from 'rxjs';
import {LanguageService} from './language.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly SERVICE_URL = environment.apiURL + 'api/authentication/';

  constructor(private http: HttpClient,
              private $localStorage: LocalStorageService,
              private errorHandler: AppErrorHandler,
              private languageService: LanguageService,
              @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  login(email, password) {
    return this.http.post(this.SERVICE_URL + 'authenticate', {
      email: email,
      password: password
    }, {observe: 'response'}).pipe(map(this.authenticateSuccess.bind(this)));
  }

  authenticateSuccess(resp) {
    const bearerToken = resp.headers.get('Authorization');
    if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
      const jwt = bearerToken.slice(7, bearerToken.length);
      this.storeAuthenticationToken(jwt);
      let user = new User(resp.body);
      this.store.dispatch(createAction(UserActionType.LOGIN, user));
      if(user.profile) {
        if(user.profile.defaultLanguage) {
          this.languageService.changeLanguage(user.profile.defaultLanguage.code);
        }
        if(user.profile.defaultCurrency) {
          this.languageService.changeCurrency(user.profile.defaultCurrency.code);
        }
      }
    }
  }

  storeAuthenticationToken(jwt) {
    this.$localStorage.store('authenticationToken', jwt);
  }

  register(email, password) {
    return this.http.post(this.SERVICE_URL + 'register', {
      email: email,
      password: password
    }).pipe(map((user: User) => new User(user)), this.errorHandler.catchException());
  }

  activateAccount(ticket: any) {
    return this.http.get(this.SERVICE_URL + 'activate?ticket=' + ticket, {observe: 'response'}).
      pipe(map(this.authenticateSuccess.bind(this)), this.errorHandler.catchException());
  }

  loginOrRegisterWithGoogle(token: string) {
    return this.http.post(this.SERVICE_URL + 'google', {token: token},
      {observe: 'response'}).pipe(map(this.authenticateSuccess.bind(this)), this.errorHandler.catchException());
  }

  loginOrRegisterWithFacebook(token: string) {
    return this.http.post(this.SERVICE_URL + 'facebook', {token: token},
      {observe: 'response'}).pipe(map(this.authenticateSuccess.bind(this)), this.errorHandler.catchException());
  }

  signOut(): void {
    this.$localStorage.clear('authenticationToken');
    this.store.dispatch(createAction(UserActionType.LOGOUT, null));
  }

  getCurrentUser() {
    return userReducer.getCurrentUser(this.store.getState());
  }

  //runs on app init
  initialize() {
    this.store.subscribe(() => {
      this.$localStorage.store('redux', {
        time: Date.now(),
        state: this.store.getState()
      });
    });

    let redux = this.$localStorage.retrieve('redux');
    if(redux && !this.isReduxExpired(redux.time)) {
      return this.fetchUser().pipe(tap((user: User) => {
        if (!!user) {
          redux.state.user.currentUser = new User(user);
        }
        else {
          this.$localStorage.clear('authenticationToken');
        }
        this.store.dispatch(createAction(SystemActionType.INITIALIZE, redux.state));
      }));
    }
    else {
      this.$localStorage.clear('authenticationToken');
      this.$localStorage.clear('redux');
    }
    return of(null);
  }

  private isReduxExpired(time: number) {
    return moment().diff(time, 'minutes') > 60;
  }


  private fetchUser() {
    return this.http.get(this.SERVICE_URL + 'user').pipe(this.errorHandler.catchException());
  }

  changePassword(currentPassword, newPassword) {
    return this.http.put(this.SERVICE_URL + "password/", {
      currentPassword: currentPassword,
      newPassword: newPassword
    }).pipe(
      this.errorHandler.catchException()
    );
  }

  forgotPassword(email: string) {
    return this.http.post(this.SERVICE_URL + "forgotPassword", email);
  }

  recoverPassword(data: any) {
    return this.http.post(this.SERVICE_URL + "recoverPassword", data);
  }
}
