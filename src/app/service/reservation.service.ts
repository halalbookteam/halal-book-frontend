import {Inject, Injectable} from '@angular/core';
import {Reservation} from '../model/reservation';
import {HttpClient} from '@angular/common/http';
import {createAction, ReservationActionType} from '../state/app.action';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {CreditCard} from '../model/credit-card';
import {AppErrorHandler} from '../handler/app-error-handler';
import {SearchService} from './search.service';
import {Review} from '../model/review';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private readonly SERVICE_URL = environment.apiURL + 'api/reservation/';

  constructor(
    private http: HttpClient,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private searchService: SearchService,
    private errorHandler: AppErrorHandler,
  ) {
  }

  saveReservation(reservation: Reservation) {
    if (!reservation.id) {
      return this.createReservation(reservation);
    }
    else {
      return this.updateReservation(reservation);
    }
  }

  private createReservation(reservation: Reservation) {
    return this.http.post(this.SERVICE_URL, reservation).pipe(
      map((res: Reservation) => {
        res.hotel = reservation.hotel;
        return new Reservation(res);
      }),
      tap((reservation: Reservation) => this.store.dispatch(createAction(ReservationActionType.CREATE_RESERVATION, reservation))),
      this.errorHandler.catchException()
    );
  }

  private updateReservation(reservation: Reservation) {
    return this.http.put(this.SERVICE_URL + reservation.id, reservation).pipe(
      map((res: Reservation) => {
        res.hotel = reservation.hotel;
        return new Reservation(res);
      }),
      tap((reservation: Reservation) => this.store.dispatch(createAction(ReservationActionType.UPDATE_RESERVATION, reservation))),
      this.errorHandler.catchException()
    );
  }

  saveSource(reservationId: number, paymentDetail: CreditCard) {
    return this.http.post(this.SERVICE_URL + reservationId + '/paymentDetail', paymentDetail).pipe(
      map((creditCard: CreditCard) => new CreditCard(creditCard)),
      tap((createdCreditCard: CreditCard) => {
        this.store.dispatch(createAction(ReservationActionType.SAVE_PAYMENT_DETAIL, paymentDetail.id ? null : createdCreditCard));
      }),
      this.errorHandler.catchException()
    );
  }

  fetchReservations() {
    return this.http.get(this.SERVICE_URL).pipe(
      map((reservations: Reservation[]) => reservations.map((reservation: Reservation) => new Reservation(reservation))),
      tap(reservations => this.store.dispatch(createAction(ReservationActionType.FETCH_RESERVATIONS, reservations))),
      this.errorHandler.catchException()
    );
  }

  fetchMyReservations() {
    return this.http.get(this.SERVICE_URL + "myReservations").pipe(
      map((reservations: Reservation[]) => reservations.map((reservation: Reservation) => new Reservation(reservation))),
      tap(reservations => this.store.dispatch(createAction(ReservationActionType.FETCH_MY_RESERVATIONS, reservations))),
      this.errorHandler.catchException()
    );
  }

  cancel(reservation: Reservation) {
    return this.http.put(this.SERVICE_URL + "cancel/" + reservation.id, null).pipe(
      tap((res: Reservation) => {
        this.store.dispatch(createAction(ReservationActionType.UPDATE_RESERVATION, new Reservation(res)));
      }),
      this.errorHandler.catchException()
    );
  }

  fetchReview(reservationId: number) {
    return this.http.get(this.SERVICE_URL + "review/" + reservationId).pipe(
      map((review: Review) => new Review(review)),
      tap((review: Review) => {
        this.store.dispatch(createAction(ReservationActionType.SAVE_REVIEW, review));
      }),
      this.errorHandler.catchException()
    );
  }

  saveReview(review: Review) {
    if(!review.id) {
      return this.createReview(review);
    }
    return this.updateReview(review);
  }

  private createReview(review: Review) {
    return this.http.post(this.SERVICE_URL + 'review', review).pipe(
      map((review: Review) => new Review(review)),
      tap((createdReview: Review) => {
        this.store.dispatch(createAction(ReservationActionType.SAVE_REVIEW, createdReview));
      }),
      this.errorHandler.catchException()
    );
  }

  private updateReview(review: Review) {
    return this.http.put(this.SERVICE_URL + 'review/' + review.id, review).pipe(
      map((review: Review) => new Review(review)),
      tap((updatedReview: Review) => {
        this.store.dispatch(createAction(ReservationActionType.SAVE_REVIEW, updatedReview));
      }),
      this.errorHandler.catchException()
    );
  }

  getPendingReviews() {
    return this.http.get(this.SERVICE_URL + "review/pending").pipe(
      map((reviews: Review[]) => reviews.map(review => new Review(review))),
      tap((reviews: Review[]) => {
        this.store.dispatch(createAction(ReservationActionType.FETCH_REVIEWS, reviews));
      }),
      this.errorHandler.catchException()
    );
  }

  acceptReview(review: Review) {
    return this.http.put(this.SERVICE_URL + 'review/accept/' + review.id, null).pipe(
      map((review: Review) => new Review(review)),
      tap((updatedReview: Review) => {
        this.store.dispatch(createAction(ReservationActionType.REVIEW_REVIEW, updatedReview));
      }),
      this.errorHandler.catchException()
    );
  }

  rejectReview(review: Review) {
    return this.http.put(this.SERVICE_URL + 'review/reject/' + review.id, null).pipe(
      map((review: Review) => new Review(review)),
      tap((updatedReview: Review) => {
        this.store.dispatch(createAction(ReservationActionType.REVIEW_REVIEW, updatedReview));
      }),
      this.errorHandler.catchException()
    );
  }
}
