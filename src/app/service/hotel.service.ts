import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Hotel} from '../model/hotel';
import {createAction, HotelsActionType, SearchActionType} from '../state/app.action';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {map, tap} from 'rxjs/operators';
import {Room, RoomAlternative, RoomRate} from '../model/room';
import {AppErrorHandler} from '../handler/app-error-handler';
import {Review} from '../model/review';

@Injectable({
  providedIn: 'root'
})

export class HotelService {
  private readonly SERVICE_URL = environment.apiURL + 'api/hotel/';

  constructor(
    private http: HttpClient,
    private errorHandler: AppErrorHandler,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  saveHotel(hotel: Hotel) {
    if (!hotel.id) {
      return this.createHotel(hotel);
    }
    else {
      return this.updateHotel(hotel);
    }
  }

  createHotel(hotel: Hotel) {
    this.store.dispatch(createAction(HotelsActionType.CREATE_HOTEL, null));
    return this.http.post(this.SERVICE_URL, hotel).pipe(
      map((hotel: Hotel) => new Hotel(hotel)),
      tap((createdHotel: Hotel) => {
        this.store.dispatch(createAction(HotelsActionType.CREATE_HOTEL_SUCCESS, createdHotel));
      }),
      this.errorHandler.catchException()
    );
  }

  updateHotel(hotel: Hotel) {
    this.store.dispatch(createAction(HotelsActionType.UPDATE_HOTEL, null));
    return this.http.put(this.SERVICE_URL + hotel.id, hotel).pipe(
      map((hotel: Hotel) => new Hotel(hotel)),
      tap((updatedHotel: Hotel) => {
        this.store.dispatch(createAction(HotelsActionType.UPDATE_HOTEL_SUCCESS, updatedHotel));
      }),
      this.errorHandler.catchException()
    );
  }

  deleteHotel(hotel: Hotel) {
    this.store.dispatch(createAction(HotelsActionType.DELETE_HOTEL, null));
    return this.http.delete(this.SERVICE_URL + hotel.id).pipe(
      tap(() => {
        this.store.dispatch(createAction(HotelsActionType.DELETE_HOTEL_SUCCESS, hotel.id));
      }),
      this.errorHandler.catchException()
    );
  }

  fetchHotels() {
    return this.http.get<Hotel[]>(this.SERVICE_URL).pipe(
      map(hotels => hotels.map(hotel => new Hotel(hotel))),
      tap((hotels: Hotel[]) => {
        if (!hotels) {
          hotels = [];
        }
        this.store.dispatch(createAction(HotelsActionType.FETCH_HOTELS, hotels));
      }),
      this.errorHandler.catchException()
    );
  }

  saveRoom(room: Room) {
    if (!room.id) {
      return this.createRoom(room);
    }
    else {
      return this.updateRoom(room);
    }
  }

  createRoom(room: Room) {
    this.store.dispatch(createAction(HotelsActionType.CREATE_ROOM, null));
    return this.http.post(this.SERVICE_URL + 'room', room).pipe(
      map((room: Room) => new Room(room)),
      tap((createdRoom: Room) => {
        this.store.dispatch(createAction(HotelsActionType.CREATE_ROOM_SUCCESS, createdRoom));
      }),
      this.errorHandler.catchException()
    );
  }

  updateRoom(room: Room) {
    this.store.dispatch(createAction(HotelsActionType.UPDATE_ROOM, null));
    return this.http.put(this.SERVICE_URL + 'room/' + room.id, room).pipe(
      map((room: Room) => new Room(room)),
      tap((updatedRoom: Room) => {
        this.store.dispatch(createAction(HotelsActionType.UPDATE_ROOM_SUCCESS, updatedRoom));
      }),
      this.errorHandler.catchException()
    );
  }

  deleteRoom(room: Room) {
    this.store.dispatch(createAction(HotelsActionType.DELETE_ROOM, null));
    return this.http.delete(this.SERVICE_URL + 'room/' + room.id).pipe(
      tap(() => {
        this.store.dispatch(createAction(HotelsActionType.DELETE_ROOM_SUCCESS, room));
      }),
      this.errorHandler.catchException()
    );
  }

  deleteAlternative(alternative: RoomAlternative) {
    this.store.dispatch(createAction(HotelsActionType.DELETE_ALTERNATIVE, null));
    return this.http.delete(this.SERVICE_URL + 'alternative/' + alternative.id).pipe(
      tap(() => {
        this.store.dispatch(createAction(HotelsActionType.DELETE_ALTERNATIVE_SUCCESS, alternative));
      }),
      this.errorHandler.catchException()
    );
  }

  saveAlternative(alternative: RoomAlternative) {
    if (!alternative.id) {
      return this.createAlternative(alternative);
    }
    else {
      return this.updateAlternative(alternative);
    }
  }

  createAlternative(alternative: RoomAlternative) {
    this.store.dispatch(createAction(HotelsActionType.CREATE_ALTERNATIVE, null));
    return this.http.post(this.SERVICE_URL + 'alternative', alternative).pipe(
      map((roomAlternative: RoomAlternative) => new RoomAlternative(roomAlternative)),
      tap((createdAlternative: RoomAlternative) => {
        this.store.dispatch(createAction(HotelsActionType.CREATE_ALTERNATIVE_SUCCESS, createdAlternative));
      }),
      this.errorHandler.catchException()
    );
  }

  updateAlternative(alternative: RoomAlternative) {
    this.store.dispatch(createAction(HotelsActionType.UPDATE_ALTERNATIVE, null));
    return this.http.put(this.SERVICE_URL + 'alternative/' + alternative.id, alternative).pipe(
      map((roomAlternative: RoomAlternative) => new RoomAlternative(roomAlternative)),
      tap((updatedAlternative: RoomAlternative) => {
        this.store.dispatch(createAction(HotelsActionType.UPDATE_ALTERNATIVE_SUCCESS, updatedAlternative));
      }),
      this.errorHandler.catchException()
    );
  }

  fetchReviews(hotel: Hotel) {
    return this.http.get(this.SERVICE_URL + "reviews/" + hotel.id).pipe(
      map((reviews: Review[]) => reviews.map(review => new Review(review))),
      tap((reviews: Review[]) => {
        this.store.dispatch(createAction(SearchActionType.FETCH_HOTEL_REVIEWS, {
          hotelId: hotel.id,
          reviews: reviews,
        }));
      }),
      this.errorHandler.catchException()
    );
  }

  saveRoomRates(room: Room, rates: RoomRate[]) {
    return this.http.post(this.SERVICE_URL + "rates/" + room.id, rates).pipe(
      map((roomRates: RoomRate[]) => roomRates.map(rate => new RoomRate(rate))),
      tap((roomRates: RoomRate[]) => {
        this.store.dispatch(createAction(HotelsActionType.SAVE_ROOM_RATES, {
          room: room,
          rates: roomRates
        }));
      }),
      this.errorHandler.catchException()
    );
  }
}
