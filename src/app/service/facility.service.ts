import {Inject, Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Facility} from '../model/facility';
import {HttpClient} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {AdminActionType, createAction} from '../state/app.action';
import {FacilityCategory} from '../model/facility-category';
import {AppErrorHandler} from '../handler/app-error-handler';
import * as adminDataReducer from '../state/reducers/admin-data.reducer';

@Injectable({
  providedIn: 'root'
})
export class FacilityService {
  private readonly SERVICE_URL = environment.apiURL + 'api/facility/';

  constructor(
    private http: HttpClient,
    private errorHandler: AppErrorHandler,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  fetchCategories() {
    return this.http.get<FacilityCategory[]>(this.SERVICE_URL + 'categories').pipe(
      map(categories => categories.map(category => new FacilityCategory(category))),
      tap(categories => {
        this.store.dispatch(createAction(AdminActionType.FETCH_CATEGORIES, categories));
      }),
      this.errorHandler.catchException()
    );
  }

  createFacility(facility: Facility): Observable<any> {
    return this.http.post(this.SERVICE_URL + 'facility', facility).pipe(
      map((fac: Facility) => new Facility(fac)),
      tap(fac => {
        this.store.dispatch(createAction(AdminActionType.CREATE_FACILITY, fac));
      }),
      this.errorHandler.catchException()
    );
  }

  updateFacility(facility: Facility) {
    return this.http.put(this.SERVICE_URL + 'facility/' + facility.id, facility).pipe(
      map((fac: Facility) => new Facility(fac)),
      tap(fac => {
        this.store.dispatch(createAction(AdminActionType.UPDATE_FACILITY, fac));
      }),
      this.errorHandler.catchException()
    );
  }

  deleteFacility(facility: Facility): Observable<any> {
    return this.http.delete(this.SERVICE_URL + 'facility/' + facility.id).pipe(
      tap(() => {
        this.store.dispatch(createAction(AdminActionType.DELETE_FACILITY, facility));
      }),
      this.errorHandler.catchException()
    );
  }

  createCategory(category: FacilityCategory): Observable<any> {
    return this.http.post(this.SERVICE_URL + 'category', category).pipe(
      map((category: FacilityCategory) => new FacilityCategory(category)),
      this.errorHandler.catchException()
    );
  }

  updateCategory(category: FacilityCategory) {
    return this.http.put(this.SERVICE_URL + 'category/' + category.id, category).pipe(
      map((category: FacilityCategory) => new FacilityCategory(category)),
      tap((cat: FacilityCategory) => {
        this.store.dispatch(createAction(AdminActionType.UPDATE_CATEGORY, cat));
      }),
      this.errorHandler.catchException()
    );
  }

  deleteCategory(category: FacilityCategory): Observable<any> {
    return this.http.delete(this.SERVICE_URL + 'category/' + category.id).pipe(
      tap(() => {
        this.store.dispatch(createAction(AdminActionType.DELETE_CATEGORY, category.id));
      }),
      this.errorHandler.catchException()
    );
  }

  saveCategory(category: FacilityCategory) {
    if (!category.id) {
      return this.createCategory(category);
    }
    return this.updateCategory(category);
  }

  saveFacility(facility: Facility) {
    if (!facility.id) {
      return this.createFacility(facility);
    }
    return this.updateFacility(facility);
  }

  getCategoryById(id: number) {
    return adminDataReducer.getCategory(this.store.getState(), id);
  }
}
