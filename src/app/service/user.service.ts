import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LocalStorageService} from 'ngx-webstorage';
import { environment } from '../../environments/environment';
import {map, tap} from 'rxjs/operators';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {createAction, UserActionType} from '../state/app.action';
import {User, UserProfile} from '../model/user';
import {AppErrorHandler} from '../handler/app-error-handler';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly SERVICE_URL = environment.apiURL + 'api/user/';

  constructor(private http: HttpClient,
              private $localStorage: LocalStorageService,
              private errorHandler: AppErrorHandler,
              @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
  }

  fetchUsers() {
    return this.http.get(this.SERVICE_URL).pipe(
      map((users: User[]) => users.map(user => new User(user))),
      tap(users => this.store.dispatch(createAction(UserActionType.FETCH_USERS, users))),
      this.errorHandler.catchException()
    );
  }

  saveUser(user: User) {
    if(!user.id) {
      return this.createUser(user);
    }
    return this.updateUser(user);
  }

  private createUser(user: User) {
    return this.http.post(this.SERVICE_URL, user).pipe(
      map((user: User) => new User(user)),
      this.errorHandler.catchException()
    );
  }

  private updateUser(user: User) {
    return this.http.put(this.SERVICE_URL + user.id, user).pipe(
      map((user: User) => new User(user)),
      tap((updatedUser: User) => {
        this.store.dispatch(createAction(UserActionType.UPDATE_USER, updatedUser));
      }),
      this.errorHandler.catchException()
    );
  }

  deleteUser(user: User) {
    return this.http.delete(this.SERVICE_URL + user.id).pipe(
      this.errorHandler.catchException()
    );
  }

  saveProfile(profile: UserProfile) {
    return this.http.put(this.SERVICE_URL + "profile", profile).pipe(
      tap(() => {
        this.store.dispatch(createAction(UserActionType.UPDATE_PROFILE, profile));
      }),
      this.errorHandler.catchException()
    );
  }
}
