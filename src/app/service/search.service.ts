import {Inject, Injectable} from '@angular/core';
import {FacilityCategory} from '../model/facility-category';
import {Filter, FilterGroup, FilterType} from '../model/filter';
import {createAction, SearchActionType} from '../state/app.action';
import {HttpClient} from '@angular/common/http';
import {AppStore} from '../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../state/reducers/app.reducer';
import {HotelSearchQuery} from '../model/hotel-search-query';
import {of} from 'rxjs';
import {Hotel} from '../model/hotel';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';
import {Place} from '../model/place';
import * as searchReducer from '../state/reducers/search.reducer';
import {AppErrorHandler} from '../handler/app-error-handler';
import {TranslateService} from '@ngx-translate/core';
import {ReviewScoreLevelPipe} from '../app-common/pipe/review-score-level.pipe';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private readonly SERVICE_URL = environment.apiURL + 'api/hotel/';

  constructor(
    private http: HttpClient,
    private errorHandler: AppErrorHandler,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private translateService: TranslateService,
    private reviewScoreLevelPipe: ReviewScoreLevelPipe,
  ) {
  }

  search(hotelSearchQuery: HotelSearchQuery) {
    return this.http.post<Hotel[]>(this.SERVICE_URL + 'search', hotelSearchQuery).pipe(
      map(hotels => {
        hotels = hotels.map(hotel => new Hotel(hotel));

        this.store.dispatch(createAction(SearchActionType.SEARCH, {
          query: hotelSearchQuery,
          hotels: hotels
        }));

        return hotels;
      }),
      this.errorHandler.catchException());
  }

  loadFilterGroups(categories: FacilityCategory[]) {
    let filterGroups: FilterGroup[] = [];
    let starGroup = new FilterGroup('enum.FilterType.STAR', FilterType.STAR);
    of(5, 4, 3, 2).subscribe(value => {
      starGroup.filters.push(new Filter(value, value));
    });
    starGroup.filters.push(new Filter('common.others', null));

    filterGroups.push(starGroup);

    let reviewGroup = new FilterGroup('enum.FilterType.REVIEW', FilterType.REVIEW);
    of(9, 8, 7, 6).pipe().subscribe(value => {
      this.reviewScoreLevelPipe.transform(value).subscribe(t => {
        reviewGroup.filters.push(new Filter(value, value));
      });
    });
    reviewGroup.filters.push(new Filter('common.others', null));

    filterGroups.push(reviewGroup);

    categories.forEach(category => {
      if (category.filter) {
        let group = new FilterGroup(category.id, FilterType.FACILITY);
        filterGroups.push(group);
        category.facilities.forEach(facility => {
          group.filters.push(new Filter(facility.id, true));
        });
      }
    });

    this.store.dispatch(createAction(SearchActionType.LOAD_FILTER_GROUPS, filterGroups));

    return filterGroups;
  }

  private fetchPlaces() {
    return this.http.get<Place[]>(this.SERVICE_URL + 'places').pipe(
      map(places => places.map(place => new Place(place))),
      tap(places => this.store.dispatch(createAction(SearchActionType.LOAD_PLACES, places)))
    );
  }

  getPlaces() {
    let places = searchReducer.getPlaces(this.store.getState());
    if (places) {
      return of(places);
    }
    return this.fetchPlaces();
  }

  fetchHotel(hotelId: number) {
    let hotel = this.getHotel(hotelId);
    if (hotel) {
      return of(hotel);
    }
    return this.http.get(environment.apiURL + 'api/hotel/' + hotelId).pipe(
      map((hotel: Hotel) => new Hotel(hotel)),
      this.errorHandler.catchException()
    );
  }

  getHotel(hotelId: number) {
    return searchReducer.getHotel(this.store.getState(), hotelId);
  }
}
