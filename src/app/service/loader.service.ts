import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {LanguageService} from './language.service';
import {fromPromise} from 'rxjs/internal-compatibility';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(
    private authenticationService: AuthenticationService,
    private languageService: LanguageService,
  ) { }

  initialize() {
    return fromPromise(this.authenticationService.initialize().toPromise()
      .then(() => this.languageService.initialize().toPromise()));
  }
}
