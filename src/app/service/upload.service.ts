import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  static readonly UPLOAD_SERVICE_URL = environment.apiURL + 'api/store/';

  constructor(
    private http: HttpClient,
  ) { }

  upload(formData: FormData) {
    return this.http.post(UploadService.UPLOAD_SERVICE_URL, formData, {
      observe: 'events',
      reportProgress: true,
      responseType: 'json'
    });
  }

  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
