import {ApplicationRef, Injectable, Injector} from '@angular/core';
import {ToastaConfig, ToastaService, ToastOptions} from 'ngx-toasta';
import {Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

export enum MessageType {
  DEFAULT = "DEFAULT",
  INFO = "INFO",
  SUCCESS = "SUCCESS",
  WARNING = "WARNING",
  WAIT = "WAIT",
  ERROR = "ERROR",
  SERVER_ERROR = "SERVER_ERROR",
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private errorSubject = new Subject<any>();

  constructor(
    private toastaService:ToastaService,
    private toastaConfig: ToastaConfig,
    private injector: Injector,
    private translationService: TranslateService,
    ) {
    this.toastaConfig.theme = 'default';
    this.toastaConfig.position='top-right';
  }

  showMessage(message: string, type: MessageType = MessageType.ERROR, title: string = undefined) {
    let options: ToastOptions = {
      msg: message,
      title: title,
    };
    if(!title) {
      this.translationService.get("enum.messageType." + type).subscribe(title => options.title = title);
    }
    switch (type) {
      case MessageType.DEFAULT:
        this.toastaService.default(options);
        return;
      case MessageType.ERROR:
        this.toastaService.error(options);
        return;
      case MessageType.INFO:
        this.toastaService.info(options);
        return;
      case MessageType.SUCCESS:
        this.toastaService.success(options);
        return;
      case MessageType.WARNING:
        this.toastaService.warning(options);
        return;
      case MessageType.WAIT:
        this.toastaService.wait(options);
        return;
      case MessageType.SERVER_ERROR:
        this.showServerErrorMessage(message, title);
        return;
    }
  }

  showServerErrorMessage(message: string, title?: string, removeCallback?) {
    let options = {
      msg: message,
      title: title,
      onRemove: removeCallback,
    };
    if(!title) {
      this.translationService.get("enum.messageType.ERROR").subscribe(title => options.title = title);
    }
    this.toastaService.error(options);
    this.errorSubject.next('');
    this.injector.get(ApplicationRef).tick();
  }

  showTransMessage(key: string, type: MessageType, title?: string) {
    this.translationService.get(key).subscribe(message => this.showMessage(message, type, title));
  }
}
