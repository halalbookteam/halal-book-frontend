import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfileComponent} from './profile/profile.component';
import {AppCommonModule} from '../app-common/app-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProfileParentComponent} from './profile-parent/profile-parent.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxMaskModule} from 'ngx-mask';
import {AppHotelListModule} from '../app-hotel-list/app-hotel-list.module';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {ReviewComponent} from './review/review.component';
import {MyReservationsComponent} from './my-reservations/my-reservations.component';
import {ReservationCardComponent} from './reservation-card/reservation-card.component';
import {ReviewScoresComponent} from './review-scores/review-scores.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {RouterModule} from '@angular/router';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModule.forRoot(),
    NgxMaskModule.forRoot(),

    AppCommonModule,
    AppHotelListModule,
    RouterModule,
  ],
  declarations: [
    ProfileComponent,
    ProfileParentComponent,
    ChangePasswordComponent,
    ReviewComponent,
    MyReservationsComponent,
    ReservationCardComponent,
    ReviewScoresComponent,
    ForgotPasswordComponent,
    RecoverPasswordComponent,
  ]
})
export class AppProfileModule { }
