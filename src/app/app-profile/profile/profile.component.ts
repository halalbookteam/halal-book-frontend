import {Component, OnInit, ViewChild} from '@angular/core';
import {UserProfile} from '../../model/user';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../service/authentication.service';
import {UserService} from '../../service/user.service';
import {MessageService, MessageType} from '../../service/message.service';
import {UploadService} from '../../service/upload.service';
import {HttpEventType} from '@angular/common/http';
import {AppErrorHandler} from '../../handler/app-error-handler';
import {Photo} from '../../model/photo';
import {Currency, Language} from '../../model/language';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  languages: Language[] = [];
  currencies: Currency[] = [];
  profile: UserProfile;
  @ViewChild('fileInput') fileInput;

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router,
    private messageService: MessageService,
    private errorHandler: AppErrorHandler,
    private uploadService: UploadService,
    private languageService: LanguageService,
  ) {
  }

  ngOnInit() {
    let user = this.authenticationService.getCurrentUser();
    this.languages = this.languageService.getLanguages();
    this.currencies = this.languageService.getCurrencies();
    if(!user) {
      this.router.navigateByUrl("");
    }
    else {
      this.profile = user.profile;
    }
  }

  save() {
    this.userService.saveProfile(this.profile).subscribe(() => {
      this.messageService.showTransMessage("message.saved", MessageType.SUCCESS);
    });
  }

  upload() {
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      let formData = new FormData();
      let uuid = this.uploadService.newGuid();
      formData.set("file", fileBrowser.files[0], uuid);
      let fileUploadSubscription = this.uploadService.upload(formData).subscribe((event: any) => {
        if (event.type === HttpEventType.DownloadProgress) {
          this.profile.avatar = new Photo("api/store/" + uuid);
        }
      }, (error: any) => {
        if (fileUploadSubscription) {
          fileUploadSubscription.unsubscribe();
        }
      });
    }
    this.fileInput.nativeElement.value = '';
  }

  getImageUrl() {
    if(this.profile.avatar) {
      return this.profile.avatar.getFullUrl;
    }
    return "https://s-ec.bstatic.com/static/img/profile/default_avatar_L/26e19e03bae083b98dcc8d134ec2b9be4107f442.png";
  }

}
