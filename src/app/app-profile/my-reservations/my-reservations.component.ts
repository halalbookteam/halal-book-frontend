import {Component, Inject, OnInit} from '@angular/core';
import {ReservationService} from '../../service/reservation.service';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as reservationReducer from '../../state/reducers/reservation.reducer';
import {Reservation} from '../../model/reservation';

@Component({
  selector: 'app-my-reservations',
  templateUrl: './my-reservations.component.html',
  styleUrls: ['./my-reservations.component.scss']
})
export class MyReservationsComponent implements OnInit {
  reservations: Reservation[];

  constructor(
    private reservationService: ReservationService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
    this.updateState();
  }

  ngOnInit() {
  }

  private updateState() {
    let state = this.store.getState();
    this.reservations = reservationReducer.getMyReservations(state);
  }
}
