import {Component, Inject, OnInit} from '@angular/core';
import {Review, ReviewPurposeType} from '../../model/review';
import {ActivatedRoute, Router} from '@angular/router';
import {Reservation} from '../../model/reservation';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as reservationReducer from '../../state/reducers/reservation.reducer';
import {MessageService, MessageType} from '../../service/message.service';
import {ReservationService} from '../../service/reservation.service';
import * as _ from 'lodash';
import {AuthenticationService} from '../../service/authentication.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  review: Review;
  reservation: Reservation;
  ReviewPurposeType = ReviewPurposeType;
  user: User;

  constructor(
    private route: ActivatedRoute,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private messageService: MessageService,
    private router: Router,
    private reservationService: ReservationService,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    const reservationId = this.getId();
    if(!reservationReducer.findMyReservation(this.store.getState(), reservationId)) {
      this.messageService.showTransMessage("message.notFound", MessageType.SUCCESS);
      this.router.navigateByUrl("/profile");
    }
    this.route.data
      .subscribe((data: { review: Review }) => {
        this.review = _.cloneDeep(data.review);
        this.reservation = reservationReducer.findMyReservation(this.store.getState(), this.review.reservationId);
      });
    this.user = this.authenticationService.getCurrentUser();
  }

  private getId(): number {
    return +this.route.snapshot.paramMap.get('reservationId');
  }

  save() {
    this.reservationService.saveReview(this.review).subscribe(review => {
      Object.assign(this.review, review);
      this.messageService.showTransMessage("message.saved", MessageType.SUCCESS);
    });
  }

  aloneSelected() {
    let travelWith = this.review.travelWith;
    if(travelWith.alone) {
      travelWith.friends = travelWith.partner = travelWith.family = travelWith.colleague = false;
    }
  }

  companySelected(checked: boolean) {
    if(checked) {
      this.review.travelWith.alone = false;
    }
  }
}
