import {Component, Input, OnInit} from '@angular/core';
import {Reservation, ReservationStatus} from '../../model/reservation';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reservation-card',
  templateUrl: './reservation-card.component.html',
  styleUrls: ['./reservation-card.component.scss']
})
export class ReservationCardComponent implements OnInit {
  @Input() reservation: Reservation;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

  mainAction() {
    if(this.reservation.canChange()) {
      this.goReservation();
    }
    else if(this.reservation.canReview()) {
      this.goReview();
    }
    else {
      this.goHotel();
    }
  }

  private goHotel() {
    this.router.navigateByUrl('/hotel/' + this.reservation.hotel.id);
  }

  goReview() {
    this.router.navigateByUrl('/profile/reservations/' + this.reservation.id + '/review');
  }

  goReservation() {
    this.router.navigateByUrl('/profile/reservations/' + this.reservation.id);
  }
}
