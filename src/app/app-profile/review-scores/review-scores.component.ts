import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ReviewScore} from '../../model/review';

@Component({
  selector: 'app-review-scores',
  templateUrl: './review-scores.component.html',
  styleUrls: ['./review-scores.component.scss']
})
export class ReviewScoresComponent implements OnInit {
  @Input() title: string;
  @Input() score: ReviewScore;
  @Output() scoreChange = new EventEmitter<ReviewScore>();

  ReviewScore = ReviewScore;

  constructor() { }

  ngOnInit() {
  }

  setScore(score) {
    this.score = score;
    this.scoreChange.emit(score);
  }
}
