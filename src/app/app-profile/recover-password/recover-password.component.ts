import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MessageService, MessageType} from '../../service/message.service';
import {AuthenticationService} from '../../service/authentication.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent implements OnInit {
  formMessage: string;
  newPasswordControl = new FormControl('');
  repeatNewPasswordControl = new FormControl('');

  constructor(
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private translateService: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
  }

  save(form) {
    this.formMessage = undefined;
    if (!this.newPasswordControl.valid || !this.repeatNewPasswordControl.valid) {
      return;
    }

    if (this.newPasswordControl.value === this.repeatNewPasswordControl.value) {
      this.route.queryParams.subscribe(params => {
        this.authenticationService.recoverPassword({
            password: this.newPasswordControl.value,
            token: params.ticket
          }).subscribe(() => {
          this.messageService.showTransMessage('message.saved', MessageType.SUCCESS);
          this.router.navigateByUrl('');
        });
      });
    }
    else {
      this.translateService.get('message.passwordsNotMatch').subscribe(message => this.formMessage = message);
    }
  }
}
