import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MessageService, MessageType} from '../../service/message.service';
import {AuthenticationService} from '../../service/authentication.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  formMessage: string;
  currentPasswordControl = new FormControl('');
  newPasswordControl = new FormControl('');
  repeatNewPasswordControl = new FormControl('');

  constructor(
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
  }

  save(form) {
    this.formMessage = undefined;
    if(!this.currentPasswordControl.valid || !this.newPasswordControl.valid || !this.repeatNewPasswordControl.valid) {
      return;
    }
    if(this.newPasswordControl.value === this.repeatNewPasswordControl.value) {
      this.authenticationService.changePassword(this.currentPasswordControl.value, this.newPasswordControl.value).subscribe(() => {
        this.messageService.showTransMessage("message.saved", MessageType.SUCCESS);
        form.reset();
      });
    }
    else {
      this.translateService.get("message.passwordsNotMatch").subscribe(message => this.formMessage = message);
    }
  }
}
