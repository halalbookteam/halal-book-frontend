import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {AuthenticationService} from '../../service/authentication.service';
import {MessageService, MessageType} from '../../service/message.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recover-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  formMessage: string;
  emailControl = new FormControl('');

  constructor(
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  submit() {
    this.formMessage = undefined;
    if(this.emailControl.valid) {
      this.authenticationService.forgotPassword(this.emailControl.value).subscribe(() => {
        this.router.navigateByUrl("").then(() => {
          this.messageService.showTransMessage("message.forgotPassword", MessageType.SUCCESS);
        });
      },
        err => {
          this.formMessage = err.error;
        }
      );
    }
  }
}
