import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as reservationReducer from '../../state/reducers/reservation.reducer';
import {AppState} from '../../state/reducers/app.reducer';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {Reservation, ReservationStep} from '../../model/reservation';
import {ReservationService} from '../../service/reservation.service';
import {Hotel} from '../../model/hotel';
import {Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import * as searchReducer from '../../state/reducers/search.reducer';
import {PaymentDetailComponent} from '../../app-payment/payment-detail/payment-detail.component';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit, OnDestroy, AfterViewInit {
  reservationStep = ReservationStep;
  currentStep = ReservationStep.DETAILS;
  private unsubscribe: Unsubscribe;
  reservation: Reservation;
  hotel: Hotel;
  emailControl = new FormControl('');
  hotelSearchQuery: HotelSearchQuery;

  @ViewChild("paymentDetail")
  private paymentDetail: PaymentDetailComponent;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private reservationService: ReservationService,
    private cdr: ChangeDetectorRef,
    private router: Router,
  ) {
  }

  ngOnInit() {
    let state = this.store.getState();
    this.hotelSearchQuery = searchReducer.getSearchQuery(state);
    this.unsubscribe = this.store.subscribe(() => this.updateState());
    this.updateState();
    if(!this.reservation) {
      this.router.navigateByUrl("/");
    }
    else {
      this.hotel = new Hotel(this.reservation.hotel);
    }
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }

  updateState() {
    let state = this.store.getState();
    let res = reservationReducer.getActiveReservation(state);
    if(res) {
      this.reservation = res;
      this.emailControl.setValue(this.reservation.person.email);
    }
  }

  saveCreditCard() {
    this.paymentDetail.card.subscribe(card => {
      this.reservation.source = card;
      this.reservationService.saveSource(this.reservation.id, this.reservation.source).subscribe(() => {
        this.currentStep++;
      });
    });
  }

  savePerson() {
    if (this.isPersonFormValid()) {
      this.reservation.person.email = this.emailControl.value;
      this.reservation.price = this.reservation.calculateTotalPrice();
      this.reservationService.saveReservation(this.reservation).subscribe(() => {
        this.currentStep++;
      });
    }
  }

  prevStep() {
    this.currentStep--;
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  private isPersonFormValid() {
    return this.reservation.person.name &&
      this.reservation.person.lastName &&
      this.emailControl.valid &&
      (!this.reservation.person.phoneNumber || this.reservation.person.phoneNumber.length === 10);
  }
}
