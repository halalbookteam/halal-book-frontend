import {Component, Input, OnInit} from '@angular/core';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import {Reservation} from '../../model/reservation';

@Component({
  selector: 'app-book-summary',
  templateUrl: './book-summary.component.html',
  styleUrls: ['./book-summary.component.scss']
})
export class BookSummaryComponent implements OnInit {

  @Input() reservation: Reservation;
  @Input() hotelSearchQuery: HotelSearchQuery;

  ngOnInit() {
  }
}
