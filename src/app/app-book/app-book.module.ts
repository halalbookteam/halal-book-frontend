import {NgModule} from '@angular/core';
import {BookComponent} from './book/book.component';
import {BookSummaryComponent} from './book-summary/book-summary.component';
import {AppHotelListModule} from '../app-hotel-list/app-hotel-list.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppCommonModule} from '../app-common/app-common.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppHotelModule} from '../app-hotel/app-hotel.module';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {NgxMaskModule} from 'ngx-mask';
import {AppPaymentModule} from '../app-payment/app-payment.module';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModule.forRoot(),

    AppCommonModule,
    AppHotelListModule,
    AppHotelModule,
    NgxMaskModule.forRoot(),

    AppPaymentModule
  ],
  declarations: [
    BookComponent,
    BookSummaryComponent,
  ]
})
export class AppBookModule { }
