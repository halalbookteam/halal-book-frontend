import {AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {FacilityService} from '../../service/facility.service';
import {AppState} from '../../state/reducers/app.reducer';
import * as adminDataReducer from '../../state/reducers/admin-data.reducer';
import {FacilityCategory} from '../../model/facility-category';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Unsubscribe} from 'redux';

@Component({
  template: '<router-outlet></router-outlet>',
})
export class CategoriesParentComponent {
}

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
})
export class CategoryListComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['position', 'name', 'type', 'description', 'halal', 'filter'];
  dataSource: MatTableDataSource<FacilityCategory>;
  @ViewChild(MatSort) sort: MatSort;

  private categories: FacilityCategory[];
  private unsubscribe: Unsubscribe;

  constructor(private facilityService: FacilityService,
              @Inject(AppStore) private store: Redux.Store<AppState>) { }

  ngOnInit() {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => {
      this.updateState();
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  private updateState() {
    const state = this.store.getState();
    this.categories = adminDataReducer.getCategories(state);
    this.dataSource = new MatTableDataSource<FacilityCategory>(this.categories);
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
