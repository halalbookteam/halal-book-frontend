import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Facility} from '../../model/facility';
import * as _ from 'lodash';

@Component({
  selector: 'app-facility',
  templateUrl: './facility.component.html',
  styleUrls: ['./facility.component.css']
})
export class FacilityComponent implements OnInit {
  @Input() facility: Facility;
  @Input() languageId: number;
  @Input() newFacility: Facility = new Facility();
  @Output() save = new EventEmitter();
  @Output() delete = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.newFacility = _.cloneDeep(this.facility);
  }

  get translation() {
    return this.newFacility.getTranslation(this.languageId);
  }

  saveAction() {
    if(this.isFormValid()) {
      this.save.emit(this.newFacility);
    }
  }

  private isFormValid() {
    return this.translation.name;
  }
}
