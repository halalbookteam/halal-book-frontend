import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {CategoryType, FacilityCategory} from '../../model/facility-category';
import {FacilityService} from '../../service/facility.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as adminReducer from '../../state/reducers/admin-data.reducer';
import {AdminActionType, createAction} from '../../state/app.action';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent, DialogOption} from '../../app-common/confirmation-dialog/confirmation-dialog.component';
import {MessageService, MessageType} from '../../service/message.service';
import {Language} from '../../model/language';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit, OnDestroy {
  category: FacilityCategory;
  private unsubscribe: Unsubscribe;
  languageId: number;
  languages: Language[];

  constructor(
    private facilityService: FacilityService,
    private route: ActivatedRoute,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private router: Router,
    private dialog: MatDialog,
    private messageService: MessageService,
    private languageService: LanguageService,
  ) {
    this.languages = this.languageService.getLanguages();
    this.languageId = this.languageService.getLanguageId();
  }

  ngOnInit() {
    this.unsubscribe = this.store.subscribe(() => this.updateState());
    this.updateState();
  }

  get categoryTranslation() {
    return this.category.getTranslation(this.languageId, true);
  }

  get types() {
    return Object.keys(CategoryType);
  }

  private updateState() {
    let categoryId = +this.route.snapshot.params["categoryId"];
    this.category = adminReducer.getCategory(this.store.getState(), categoryId);
    if(!this.category) {
      this.category = new FacilityCategory();
    }
  }

  save() {
    if(this.isFormValid()) {
      this.facilityService.saveCategory(this.category).subscribe((savedCategory) => {
        this.messageService.showTransMessage("message.saved", MessageType.SUCCESS);
        if(!this.category.id) {
          this.router.navigateByUrl('/categories/' + savedCategory.id).then(() => {
            this.store.dispatch(createAction(AdminActionType.CREATE_CATEGORY, savedCategory));
          });
        }
      });
    }
  }

  private isFormValid() {
    return this.categoryTranslation.name;
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent).afterClosed().subscribe((result: DialogOption) => {
      if(result === DialogOption.YES) {
        this.facilityService.deleteCategory(this.category).subscribe(() => {
          this.router.navigateByUrl("/categories");
          this.messageService.showTransMessage("message.deleted", MessageType.SUCCESS);
        });
      }
    });

  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
