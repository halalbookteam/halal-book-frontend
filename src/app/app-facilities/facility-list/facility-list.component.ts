import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Facility} from '../../model/facility';
import {FacilityService} from '../../service/facility.service';
import {FacilityCategory} from '../../model/facility-category';
import {FacilityComponent} from '../facility/facility.component';
import {MessageService, MessageType} from '../../service/message.service';

@Component({
  selector: 'app-facility-list',
  templateUrl: './facility-list.component.html',
  styleUrls: ['./facility-list.component.css']
})
export class FacilityListComponent implements OnInit, AfterViewInit, OnChanges  {
  displayedColumns: string[] = ['position', 'name', 'url', 'description'];
  dataSource: MatTableDataSource<Facility>;
  @ViewChild(MatSort) sort: MatSort;
  @Input() category: FacilityCategory;
  @Input() languageId: number;

  constructor(
    private facilityService: FacilityService,
    private dialog: MatDialog,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  openModal(facility: Facility) {
    if (!facility) {
      facility = new Facility();
      facility.categoryId = this.category.id
    }
    let modal = this.dialog.open(FacilityComponent, {
      width: '500px',
      data: {interval: facility}
    });
    let component: FacilityComponent = modal.componentInstance;
    component.facility = facility;
    component.languageId = this.languageId;
    component.save.subscribe((changedFacility) => {
      this.facilityService.saveFacility(changedFacility).subscribe(() => {
        modal.close();
        this.messageService.showTransMessage("message.saved", MessageType.SUCCESS);
      });
    });
    component.delete.subscribe(facility => {
      modal.close();
      this.facilityService.deleteFacility(facility).subscribe(() => {
        this.messageService.showTransMessage("message.deleted", MessageType.SUCCESS);
      });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource = new MatTableDataSource<Facility>(this.category.facilities);
  }
}
