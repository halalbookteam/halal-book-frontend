import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CategoriesParentComponent, CategoryListComponent} from './category-list/category-list.component';
import {AppCommonModule} from '../app-common/app-common.module';
import {CategoryComponent} from './category/category.component';
import {FacilityListComponent} from './facility-list/facility-list.component';
import {FacilityComponent} from './facility/facility.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AppCommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CategoryListComponent,
    CategoryComponent,
    FacilityListComponent,
    CategoriesParentComponent,
    FacilityComponent,
  ],
  entryComponents: [FacilityComponent],

})
export class AppFacilitiesModule { }
