import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {MessageService, MessageType} from '../service/message.service';
import {BAD_REQUEST, FORBIDDEN, getStatusText, INTERNAL_SERVER_ERROR, UNAUTHORIZED} from 'http-status-codes';
import {NavigationService} from '../service/navigation.service';
import {HttpErrorResponse} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppErrorHandler extends ErrorHandler {

  constructor(
    private messageService: MessageService,
    private navigationService: NavigationService,
    private injector: Injector,
  ) {
    super();
  };

  public handleError(error: Error | HttpErrorResponse) {
    this.messageService = this.injector.get(MessageService);
    this.navigationService = this.injector.get(NavigationService);

    if (error instanceof HttpErrorResponse) {
      // Server or connection error happened
      if (!navigator.onLine) {
        this.messageService.showTransMessage("message.cannotAccessServer", MessageType.ERROR);
      } else {
        switch (error.status) {
          case UNAUTHORIZED:
          case FORBIDDEN:
            this.messageService.showTransMessage('message.forbidden', MessageType.ERROR, getStatusText(error.status));
            this.navigationService.navigate("");
            break;
          case BAD_REQUEST:
            this.messageService.showServerErrorMessage(this.getMessage(error));
            break;
          case 0:
          case INTERNAL_SERVER_ERROR:
            this.messageService.showTransMessage("message.oops", MessageType.ERROR);
            break;
          default:
            this.messageService.showServerErrorMessage(this.getMessage(error), getStatusText(error.status));
        }
      }
    } else {
      // this.messageService.showErrorMessage(error.message);
    }

    super.handleError(error);
  }

  getMessage(error) {
    return error.error ? error.error : error.message;
  }

  catchException() {
    return catchError(err => {
      this.handleError(err);
      return of();
    });
  }
}
