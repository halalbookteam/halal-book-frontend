import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MailData, MailTemplate, MailTemplateParameterType, RecipientType} from '../../model/mail';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as adminDataReducer from '../../state/reducers/admin-data.reducer';
import {MailService} from '../../service/mail.service';
import {MessageService, MessageType} from '../../service/message.service';

@Component({
  selector: 'app-send-mail',
  templateUrl: './send-mail.component.html',
  styleUrls: ['./send-mail.component.scss']
})
export class SendMailComponent implements OnInit{
  mail: MailData = new MailData();
  templates: MailTemplate [];
  MailTemplateParameterType = MailTemplateParameterType;
  @Output() valueChange = new EventEmitter();


  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private mailService: MailService,
    private messageService: MessageService,
  ) {
    this.updateState();
  }

  private updateState() {
    let state = this.store.getState();
    this.templates = adminDataReducer.getMailTemplates(state);
    this.mail.template = this.templates[0];
  }

  ngOnInit() {
  }

  templateSelected(index) {
    this.mail.template = this.templates[index];
  }

  sendMail() {
    this.mailService.sendMail(this.mail).subscribe(() => {
      this.messageService.showTransMessage("message.mailSent", MessageType.SUCCESS)
    });
  }

  recipientTypeChanged(type: any) {
    this.mail.recipientType = type;
  }

  get recipientTypes() {
    return Object.keys(RecipientType);
  }
}
