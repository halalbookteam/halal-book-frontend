import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SendMailComponent} from './send-mail/send-mail.component';
import {AppCommonModule} from '../app-common/app-common.module';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AppCommonModule,
    ReactiveFormsModule,
  ],
  declarations: [SendMailComponent]
})
export class AppMailModule { }
