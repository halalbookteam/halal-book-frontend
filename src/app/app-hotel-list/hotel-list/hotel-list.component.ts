import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {Hotel, HotelSortType} from '../../model/hotel';
import {HotelService} from '../../service/hotel.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {AppState} from '../../state/reducers/app.reducer';
import * as Redux from 'redux';
import {AppStore} from '../../state/app.store';
import * as searchReducer from '../../state/reducers/search.reducer';
import {FilterGroup} from '../../model/filter';
import {Router} from '@angular/router';
import {Unsubscribe} from 'redux';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.scss']
})
export class HotelListComponent implements OnInit, OnDestroy {
  @Input() hotels: Hotel[];
  filterGroups: FilterGroup[];
  selectedHotel: Hotel;
  sortTypes = HotelSortType;
  sortType: HotelSortType;
  asc: boolean;
  showPrice: number;
  private unsubscribe: Unsubscribe;

  constructor(
    private hotelService: HotelService,
    private modalService: NgbModal,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private router: Router,
    ) {
    if(!searchReducer.isSearched(this.store.getState())) {
      this.router.navigateByUrl("");
    }
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => {
      this.updateState();
    });
  }

  private updateState() {
    let state = this.store.getState();
    this.filterGroups = searchReducer.getFilterGroups(state);
    this.hotels = searchReducer.getAllHotels(state);
    if(!this.showPrice && searchReducer.getNightCount(state)) {
      this.showPrice = searchReducer.getNightCount(state);
      this.sortType = HotelSortType.PRICE;
      this.asc = true;
    }
  }

  ngOnInit() {
    this.sortType = this.showPrice ? HotelSortType.PRICE : HotelSortType.REVIEW;
    this.asc = !!this.showPrice;
  }

  onMapSelected(hotel: Hotel, content) {
    this.selectedHotel = hotel;
    this.modalService.open(content, {windowClass: 'dark-modal', size: 'lg'});
  }

  toggleSortType() {
    this.asc = !this.asc;
  }

  getDirectionSymbol(sortType: HotelSortType) {
    if (this.sortType === sortType) {
      return this.asc ? '▲' : '▼';
    }
    return '';
  }

  sortTypeChanged() {
    this.asc = this.sortType === HotelSortType.PRICE;
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
