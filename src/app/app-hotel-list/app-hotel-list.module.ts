import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HotelListComponent} from './hotel-list/hotel-list.component';
import {HotelListItemComponent} from './hotel-list-item/hotel-list-item.component';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {AppCommonModule} from '../app-common/app-common.module';
import {FilterGroupComponent} from './filter-group/filter-group.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    AppCommonModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  exports: [HotelListItemComponent],
  declarations: [
    HotelListComponent,
    HotelListItemComponent,
    FilterGroupComponent,
  ]
})
export class AppHotelListModule { }
