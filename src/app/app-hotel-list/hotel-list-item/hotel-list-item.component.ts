import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Hotel} from '../../model/hotel';
import {NgbCarousel} from '@ng-bootstrap/ng-bootstrap';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as searchReducer from '../../state/reducers/search.reducer';


@Component({
  selector: 'app-hotel-list-item',
  templateUrl: './hotel-list-item.component.html',
  styleUrls: ['./hotel-list-item.component.scss']
})
export class HotelListItemComponent implements OnInit, AfterViewInit {
  @Input() hotel: Hotel;
  @Input() readonly: boolean;
  @Input() showPrice: boolean;
  @Output() mapSelected = new EventEmitter<Hotel>();
  @ViewChild('carousel') carouselNgbCarousel: NgbCarousel;

  hotelSearchQuery: HotelSearchQuery;

  constructor(private cdr: ChangeDetectorRef,
              @Inject(AppStore) private store: Redux.Store<AppState>,
  ) {
    this.hotelSearchQuery = searchReducer.getSearchQuery(this.store.getState());
  }

  ngOnInit() {
  }

  selectMap() {
    this.mapSelected.emit(this.hotel);
  }

  ngAfterViewInit(): void {
    if(this.hotel.primaryPhotoIndex) {
      const slideId = this.carouselNgbCarousel.slides.toArray()[this.hotel.primaryPhotoIndex].id;
      this.carouselNgbCarousel.select(slideId);
      this.cdr.detectChanges();
    }
  }
}
