import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {Filter, FilterGroup, FilterType, LogicType} from '../../model/filter';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {createAction, SearchActionType} from '../../state/app.action';
import {FacilityService} from '../../service/facility.service';
import {LanguageService} from '../../service/language.service';
import {TranslateService} from '@ngx-translate/core';
import {ReviewScoreLevelPipe} from '../../app-common/pipe/review-score-level.pipe';

@Component({
  selector: 'app-filter-group',
  templateUrl: './filter-group.component.html',
  styleUrls: ['./filter-group.component.css']
})
export class FilterGroupComponent implements OnInit, OnDestroy {
  @Input() filterGroup: FilterGroup;
  filterGroupName: string;
  filterNames: string[];
  private unsubscribe: Unsubscribe;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private facilityService: FacilityService,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private reviewScoreLevelPipe: ReviewScoreLevelPipe,
  ) {
  }

  ngOnInit() {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => this.updateState());
  }

  private updateState() {
    this.filterNames = [];
    let languageId = this.languageService.getLanguageId();
    if(this.filterGroup.type === FilterType.FACILITY) {
      let category = this.facilityService.getCategoryById(+this.filterGroup.name);
      if(!category) {
        return;
      }
      this.filterGroupName = category.get('name', languageId);
      this.filterNames = this.filterGroup.filters.map(filter => {
        let facility = category.facilities.find(facility => facility.id === filter.name);
        return facility.get('name', languageId);
      });
    }
    else {
      this.translateService.get(this.filterGroup.name + "").subscribe(groupName => {
        this.filterGroupName = groupName;
        this.filterGroup.filters.forEach(filter => {
          if(filter.value === null) {
            this.translateService.get(filter.name + "").subscribe(filterName => this.filterNames.push(filterName));
          }
          else if(this.filterGroup.type ===  FilterType.STAR) {
            this.filterNames.push(filter.value + " " + groupName);
          }
          else {
            this.reviewScoreLevelPipe.transform(filter.value).subscribe(scoreLevel => {
              this.filterNames.push(scoreLevel + ': ' + filter.value + "+");
            })
          }
        });
      });
    }
  }

  isSuitable() {
    return this.filterGroup.filters.some(filter => filter.count > 0 || filter.checked);
  }

  isAddable() {
    return this.filterGroup.logicType === LogicType.OR && this.isGroupSelected();
  }

  filterChanged(filter: Filter) {
    this.store.dispatch(createAction(SearchActionType.FILTER_CHANGED, {
      group: this.filterGroup,
      filter: filter
    }));
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  get countToShow() {
    return this.filterGroup.logicType === LogicType.AND ? 1 : 0;
  }

  isShowFilter(filter: Filter) {
    return filter.checked || filter.count > 0;
  }

  private isGroupSelected() {
    return this.filterGroup.filters.some(filter => filter.checked)
  }
}
