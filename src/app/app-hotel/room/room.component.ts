import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Room} from '../../model/room';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  room: Room;
  @ViewChild('description') description: ElementRef;
  @Output() closeModal = new EventEmitter();

  constructor(
    private languageService: LanguageService,
  ) { }

  ngOnInit() {
    this.description.nativeElement.innerHTML = this.room.get('description', this.languageService.getLanguageId());
  }

  close() {
    this.closeModal.emit();
  }
}
