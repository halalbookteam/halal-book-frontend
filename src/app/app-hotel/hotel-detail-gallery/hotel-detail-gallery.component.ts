import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AfterViewInit, ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {CarouselWithThumbailsComponent} from '../../app-common/carousel-with-thumbails/carousel-with-thumbails.component';
import {Photo} from '../../model/photo';


@Component({
  selector: 'app-hotel-detail-gallery',
  templateUrl: './hotel-detail-gallery.component.html',
  styleUrls: ['./hotel-detail-gallery.component.css']
})
export class HotelDetailGalleryComponent implements AfterViewInit {

  @ViewChild('parentCarousel')
  public carouselNgbCarousel: CarouselWithThumbailsComponent;

  private modalCarouselNgbCarousel: CarouselWithThumbailsComponent;

  @Input() photos: Array<Photo>;
  @Input() selectedIndex;

  constructor(
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef) {
  }

  openModal() {
    const modalRef = this.modalService.open(CarouselWithThumbailsComponent,
      {windowClass: 'dark-modal hotel-gallery-modal gallery-modal', size: 'lg'});
    this.modalCarouselNgbCarousel = modalRef.componentInstance;
    this.modalCarouselNgbCarousel.photos = this.photos;
    setTimeout(() => {
      this.modalCarouselNgbCarousel.select(this.carouselNgbCarousel.getSelectedIndex());
    }, 10);
    modalRef.result.then(() => {
      this.setCarouselIndex(this.modalCarouselNgbCarousel.getSelectedIndex());
    }, () => {
      this.setCarouselIndex(this.modalCarouselNgbCarousel.getSelectedIndex());
    });
    this.modalCarouselNgbCarousel.closeModal.subscribe(() => {
      modalRef.close();
    })
  }

  private setCarouselIndex(index: number) {
    this.carouselNgbCarousel.select(index);
  }

  ngAfterViewInit(): void {
    if(!this.selectedIndex) {
      this.selectedIndex = 0;
    }
    this.carouselNgbCarousel.select(this.selectedIndex);
    this.cdr.detectChanges();
  }


}
