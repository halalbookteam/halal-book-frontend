import {Component, Input, OnInit} from '@angular/core';
import {Review} from '../../model/review';

@Component({
  selector: 'app-hotel-review',
  templateUrl: './hotel-review.component.html',
  styleUrls: ['./hotel-review.component.scss']
})
export class HotelReviewComponent implements OnInit {
  @Input() review: Review;

  constructor() { }

  ngOnInit() {
  }

}
