import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HotelComponent} from './hotel/hotel.component';
import {NgxImageGalleryModule} from 'ngx-image-gallery';
import {AppHomeModule} from '../app-home/app-home.module';
import {AppCommonModule} from '../app-common/app-common.module';
import {HotelDetailGalleryComponent} from './hotel-detail-gallery/hotel-detail-gallery.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HotelRoomCardComponent} from './hotel-room-card/hotel-room-card.component';
import {RoomComponent} from './room/room.component';
import {HotelReviewsComponent} from './hotel-reviews/hotel-reviews.component';
import {HotelReviewComponent} from './hotel-review/hotel-review.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    NgxImageGalleryModule,
    AppHomeModule,
    AppCommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule,
  ],
  declarations: [
    HotelComponent,
    HotelDetailGalleryComponent,
    HotelRoomCardComponent,
    RoomComponent,
    HotelReviewsComponent,
    HotelReviewComponent
  ],
  exports: [
    HotelRoomCardComponent,
  ],
  entryComponents: [RoomComponent],
})
export class AppHotelModule {
}
