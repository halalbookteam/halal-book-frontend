import {ChangeDetectorRef, Component, Inject, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Room} from '../../model/room';
import {NgbCarousel, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RoomComponent} from '../room/room.component';
import * as Redux from 'redux';
import {AppStore} from '../../state/app.store';
import {AppState} from '../../state/reducers/app.reducer';
import {Reservation} from '../../model/reservation';
import {MealOptionType} from '../../model/room-alternative-options';
import {RoomReservation} from '../../model/recommended-room-set';

@Component({
  selector: 'app-hotel-room-card',
  templateUrl: './hotel-room-card.component.html',
  styleUrls: ['./hotel-room-card.component.scss']
})
export class HotelRoomCardComponent implements OnInit, OnChanges {
  @Input() room: Room;
  @Input() reservation: Reservation;
  @Input() roomReservation: RoomReservation;
  @Input() onlySelectedAlternatives: boolean;
  @Input() changeAlternativeCounts = true;
  @Input() summary;
  private roomDialog: RoomComponent;
  MealOptionType = MealOptionType;
  @ViewChild('carousel') carouselNgbCarousel: NgbCarousel;
  showPrice: boolean;

  constructor(private modalService: NgbModal,
              @Inject(AppStore) private store: Redux.Store<AppState>,
              private cdr: ChangeDetectorRef) {}

  ngOnInit() {
  }

  get alternatives() {
    if(this.roomReservation) {
      return [this.roomReservation.alternative];
    }
    if(this.onlySelectedAlternatives) {
      return this.reservation.getAlternativesByRoom(this.room);
    }
    return this.room.alternatives;
  }

  openModal() {
    let modalRef = this.modalService.open(RoomComponent, {windowClass: 'dark-modal room-gallery-modal gallery-modal', size: 'lg'});
    this.roomDialog = modalRef.componentInstance;
    this.roomDialog.closeModal.subscribe(() => {
      modalRef.close();
    });
    this.roomDialog.room = this.room;
  }

  removeRoomReservation(roomReservation) {
    let index = this.reservation.roomReservations.findIndex(rr => rr === roomReservation);
    this.reservation.roomReservations.splice(index, 1);
  }

  alternativeCountChanged(alternative, count: number) {
    let indexes = this.reservation.findIndexes(alternative.id);
    for(let i = 0; i < count - indexes.length; i++) {
      let roomReservation = new RoomReservation();
      roomReservation.alternative = alternative;
      roomReservation.alternativeId = alternative.id;
      roomReservation.adultCount = alternative.maxAdult;
      roomReservation.childCount = 0;
      this.reservation.roomReservations.push(roomReservation)
    }
    for(let i = indexes.length - 1; i >= count; i--) {
      this.reservation.roomReservations.splice(indexes[i], 1);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.carouselNgbCarousel && this.room.primaryPhotoIndex) {
      const slideId = this.carouselNgbCarousel.slides.toArray()[this.room.primaryPhotoIndex].id;
      this.carouselNgbCarousel.select(slideId);
      this.cdr.detectChanges();
    }
  }
}
