import {Component, Inject, Input, OnInit} from '@angular/core';
import {HotelEvaluation} from '../../model/hotelEvaluation';
import {Hotel} from '../../model/hotel';
import {Review, ReviewSortType, ScoreSecondLevel, TravellerType} from '../../model/review';
import {HotelService} from '../../service/hotel.service';
import * as languageReducer from '../../state/reducers/language.reducer';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {Language} from '../../model/language';

@Component({
  selector: 'app-hotel-reviews',
  templateUrl: './hotel-reviews.component.html',
  styleUrls: ['./hotel-reviews.component.scss']
})
export class HotelReviewsComponent implements OnInit {
  @Input() hotel: Hotel;
  evaluation: HotelEvaluation;

  travellerTypeMap: { [id: string]: Review[] } = {};
  scoreLevelMap: { [id: string]: Review[] } = {};
  languageMap: { [id: number]: Review[] } = {};

  selectedTravellerType: TravellerType;
  selectedScoreSecondLevel: ScoreSecondLevel;
  selectedLanguage: number;

  filteredReviews: Review[] = [];

  sortType: ReviewSortType = ReviewSortType.SCORE;
  ReviewSortType = ReviewSortType;

  asc: boolean;
  private allLanguages: Language[];

  constructor(
    private hotelService: HotelService,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    ) {
    this.allLanguages = languageReducer.getLanguages(this.store.getState());
  }

  ngOnInit() {
    this.evaluation = this.hotel.evaluation;
    if(this.evaluation && !this.evaluation.reviews) {
      this.hotelService.fetchReviews(this.hotel).subscribe((reviews: Review[]) => {
        this.evaluation.reviews = reviews;
        this.classifyReviews();
      });
    }
    else if(this.evaluation && this.evaluation.reviews) {
      this.classifyReviews();
    }
  }

  filterChanged() {
    this.filteredReviews = this.evaluation.reviews;

    if(this.selectedTravellerType) {
      this.filteredReviews = this.filteredReviews.filter(review => review.travelWith.isTravellerType(this.selectedTravellerType));
    }
    if(this.selectedScoreSecondLevel) {
      this.filteredReviews = this.filteredReviews.filter(review => review.getScoreSecondLevel(review.score) === this.selectedScoreSecondLevel);
    }
    if(this.selectedLanguage) {
      this.filteredReviews = this.filteredReviews.filter(review => review.languageId === +this.selectedLanguage);
    }
  }

  private classifyReviews() {
    this.filteredReviews = this.evaluation.reviews;
    this.evaluation.reviews.forEach(review => {
      this.classifyTravellerTypes(review);
      this.classifyScoreLevels(review);
      this.classifyLanguages(review);
    });
    this.filterChanged();
  }

  private classifyTravellerTypes(review: Review) {
    review.travelWith.suitableTravellerTypes.forEach(travellerType => {
      if(!this.travellerTypeMap[travellerType]) {
        this.travellerTypeMap[travellerType] = [];
      }
      this.travellerTypeMap[travellerType].push(review);
    });
  }

  private classifyScoreLevels(review: Review) {
    let scoreLevel = review.getScoreSecondLevel(review.score);
    if(!this.scoreLevelMap[scoreLevel]) {
      this.scoreLevelMap[scoreLevel] = [];
    }
    this.scoreLevelMap[scoreLevel].push(review);
  }

  private classifyLanguages(review: Review) {
    if(!this.languageMap[review.languageId]) {
      this.languageMap[review.languageId] = [];
    }
    this.languageMap[review.languageId].push(review);
  }

  get travellerTypes() {
    return Object.keys(this.travellerTypeMap);
  }

  get scoreLevels() {
    return Object.keys(this.scoreLevelMap);
  }

  get languages() {
    return Object.keys(this.languageMap);
  }

  toggleSortType() {
    this.asc = !this.asc;
  }

  getDirectionSymbol(sortType: ReviewSortType) {
    if (this.sortType === sortType) {
      return this.asc ? '▲' : '▼';
    }
    return '';
  }

  getLanguageName(languageId: number) {
    return this.allLanguages.find(language => language.id === +languageId).name;
  }
}
