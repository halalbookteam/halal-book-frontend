import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HotelService} from '../../service/hotel.service';
import {Hotel} from '../../model/hotel';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppState} from '../../state/reducers/app.reducer';
import * as searchReducer from '../../state/reducers/search.reducer';
import * as Redux from 'redux';
import {AppStore} from '../../state/app.store';
import {createAction, ReservationActionType} from '../../state/app.action';
import {HotelSearchQuery} from '../../model/hotel-search-query';
import {Reservation} from '../../model/reservation';
import {LanguageService} from '../../service/language.service';
import {Unsubscribe} from 'redux';
import * as _ from 'lodash';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit, OnDestroy {
  hotel: Hotel;
  hotelSearchQuery: HotelSearchQuery;
  searchPanelSelected: boolean;
  animated: boolean;
  reservation = new Reservation();

  @ViewChild('description') description: ElementRef;
  private unsubscribe: Unsubscribe;

  constructor(
    private route: ActivatedRoute,
    private hotelService: HotelService,
    private modalService: NgbModal,
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private languageService: LanguageService,
    private router: Router
  ) {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => this.updateState());
  }

  ngOnInit(): void {
    this.hotel = this.route.snapshot.data['hotel'];
    this.reservation.hotel = this.hotel;
    this.reservation.hotelId = this.hotel.id;
    if(this.hotel.recommendedRooms) {
      this.reservation.roomReservations = _.cloneDeep(this.hotel.recommendedRooms.roomReservations);
    }
    if(this.description) {
      this.description.nativeElement.innerHTML = this.hotel.get('description', this.languageService.getLanguageId());
    }
  }

  private updateState() {
    let state = this.store.getState();
    this.hotelSearchQuery = searchReducer.getSearchQuery(state);
    this.reservation.checkIn = this.hotelSearchQuery.checkIn;
    this.reservation.checkOut = this.hotelSearchQuery.checkOut;
    if(this.description) {
      this.description.nativeElement.innerHTML = this.hotel.get('description', this.languageService.getLanguageId());
    }
  }

  mapResize($event, content) {
    this.modalService.open(content, {windowClass: 'dark-modal', size: 'lg'});
  }

  scroll(target) {
    target.scrollIntoView();
  }

  selectSearchPanel(target) {
    this.scroll(target);
    this.animated = true;
    this.searchPanelSelected = !this.searchPanelSelected;
  }

  book(popover) {
    if (!this.reservation.hasReservedRoom()) {
      popover.open();
      setTimeout(() => {
        popover.close();
      }, 3000);
      return;
    }
    this.store.dispatch(createAction(ReservationActionType.BOOK, this.reservation));
    this.router.navigateByUrl('/reservation');
  }

  showSearchResults() {
    return this.router.url !== '/hotels' && searchReducer.isSearched(this.store.getState());
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }
}
