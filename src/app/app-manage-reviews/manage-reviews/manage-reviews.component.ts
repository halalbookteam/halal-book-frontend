import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import {ReservationService} from '../../service/reservation.service';
import * as reservationReducer from '../../state/reducers/reservation.reducer';
import {Review, ReviewStatus} from '../../model/review';
import {MatTableDataSource} from '@angular/material';
import {MessageService, MessageType} from '../../service/message.service';

@Component({
  selector: 'app-manage-reviews',
  templateUrl: './manage-reviews.component.html',
  styleUrls: ['./manage-reviews.component.scss']
})
export class ManageReviewsComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['like', 'dislike', 'actions'];
  dataSource: MatTableDataSource<Review>;
  private unsubscribe: Unsubscribe;

  reviews: Review[];

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private reservationService: ReservationService,
    private messageService: MessageService,
  ) {
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => {
      this.updateState();
    });
  }

  ngOnInit() {
  }

  private updateState() {
    let state = this.store.getState();
    this.dataSource = new MatTableDataSource<Review>(reservationReducer.getReviews(state).filter(review => review.status === ReviewStatus.PENDING));
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  accept(review) {
    this.reservationService.acceptReview(review).subscribe(() => {
      this.messageService.showTransMessage("message.reviewAccepted", MessageType.SUCCESS);
    });
  }

  reject(review) {
    this.reservationService.rejectReview(review).subscribe(() => {
      this.messageService.showTransMessage("message.reviewRejected", MessageType.SUCCESS);
    });
  }
}
