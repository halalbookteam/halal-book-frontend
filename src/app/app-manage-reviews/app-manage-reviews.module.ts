import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageReviewsComponent} from './manage-reviews/manage-reviews.component';
import {AppCommonModule} from '../app-common/app-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    ManageReviewsComponent,
  ]
})
export class AppManageReviewsModule { }
