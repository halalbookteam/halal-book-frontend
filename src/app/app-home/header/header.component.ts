import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AppStore} from '../../state/app.store';
import * as Redux from 'redux';
import {Unsubscribe} from 'redux';
import {AppState} from '../../state/reducers/app.reducer';
import * as userReducer from '../../state/reducers/user.reducer';
import {Permission, User} from '../../model/user';
import {LoginModalComponent} from '../../app-common/login-modal/login-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {LoginFormType} from '../../app-common/login-form/login-form.component';
import {AuthenticationService} from '../../service/authentication.service';
import {Currency, Language} from '../../model/language';
import {LanguageService} from '../../service/language.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  user: User;
  Permission = Permission;
  private readonly unsubscribe: Unsubscribe;
  FormType = LoginFormType;
  languages: Language[];
  currencies: Currency[];
  currentLang: string;
  currentCurrency: string;

  constructor(
    @Inject(AppStore) private store: Redux.Store<AppState>,
    private modalService: NgbModal,
    private authenticationService: AuthenticationService,
    private router: Router,
    private languageService: LanguageService,
  ) {
    this.languages = this.languageService.getLanguages();
    this.currencies = this.languageService.getCurrencies();
    this.updateState();
    this.unsubscribe = this.store.subscribe(() => {
      this.updateState();
    });
  }

  ngOnInit() {
  }

  private updateState() {
    const state = this.store.getState();
    this.user = userReducer.getCurrentUser(state);
    this.currentLang = this.languageService.getLanguageCode();
    this.currentCurrency = this.languageService.getCurrency().code;
  }

  login(formType: LoginFormType) {
    let modal = this.modalService.open(LoginModalComponent,
      {windowClass: 'dark-modal login-modal', size: 'lg'});
    let loginModal: LoginModalComponent = modal.componentInstance;
    loginModal.formType = formType;
  }

  signOut(): void {
    this.router.navigateByUrl("").then(value => {
      this.authenticationService.signOut();
    })
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  languageChanged(langCode: string) {
    this.languageService.changeLanguage(langCode);
  }

  currencyChanged(curCode: string) {
    this.languageService.changeCurrency(curCode);
  }
}
