import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';
import {AppCommonModule} from '../app-common/app-common.module';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing/app-routing.module';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AppMaterialModule} from '../app-material/app-material.module';

@NgModule({
  imports: [
    CommonModule,
    AppCommonModule,
    FormsModule,
    AppRoutingModule,
    AppMaterialModule,
  ],
  exports: [
    HeaderComponent,
  ],
  declarations: [
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
  ]
})
export class AppHomeModule { }
