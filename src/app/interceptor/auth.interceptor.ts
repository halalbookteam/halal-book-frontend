import {Observable} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {environment} from '../../environments/environment';
import {LocalStorageService} from 'ngx-webstorage';
import {LanguageService} from '../service/language.service';

export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private localStorage: LocalStorageService,
    private languageService: LanguageService,
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request || !request.url || (/^http/.test(request.url) && !(environment.apiURL && request.url.startsWith(environment.apiURL)))) {
      return next.handle(request);
    }

    const token = this.localStorage.retrieve('authenticationToken');
    let languageCode = this.languageService.getLanguageCode();
    let headers = {};
    if(!!languageCode) {
      headers['appLanguageCode'] = languageCode;
    }
    if (!!token) {
      headers['Authorization'] = 'Bearer ' + token;
    }
    if(Object.keys(headers).length > 0) {
      request = request.clone({
        setHeaders: headers
      });
    }
    return next.handle(request);
  }
}


